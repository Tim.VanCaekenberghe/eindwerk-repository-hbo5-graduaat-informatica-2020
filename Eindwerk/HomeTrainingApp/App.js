import React,{useState} from 'react'
import * as Font from 'expo-font';
import {AppLoading} from 'expo';

// make sure this matches the file name of navigator config
import MainStackNavigator from './src/navigation/AppNavigator'

const fetchFonts = () => {
  return Font.loadAsync({
    'Roboto_medium': require('./assets/fonts/Roboto-Medium.ttf')
  });
};

export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  if (!fontLoaded) {
    return (
        <AppLoading
            startAsync={fetchFonts}
            onFinish={() => setFontLoaded(true)}
            onError={(err) => console.log(err)}
        />
    );
  }
  return (
      <MainStackNavigator />
  )
}
