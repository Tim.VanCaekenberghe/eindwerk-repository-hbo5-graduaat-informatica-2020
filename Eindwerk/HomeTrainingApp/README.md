# 2 Navigators with React Navigation 5

- Working with 2 navigators in 1 project:
    - stack
    - bottomtab
    
- Normally you do not need to install anything, except expo,  

- The project itself already has the following:

`npm install @react-navigation/native`

`npm install @react-navigation/stack`

`npm install @react-navigation/bottom-tabs`

`expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view`



- If you receive a message that expo is not installed, run the following:

`yarn add expo`

