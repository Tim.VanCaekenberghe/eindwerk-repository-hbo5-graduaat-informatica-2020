import React from 'react';
import {StyleSheet, Text, Button, View} from 'react-native';
import Colors from "../constants/Colors";
import {Left, Header, Body, Title, Right} from 'native-base';
import MyTitle from "./MyTitle";
import { Ionicons } from '@expo/vector-icons';

const MyHeader = props =>{
    return(
        <View style={styles.header}>
            <Header>
                <Left>
                    <Button transparent>
                        <Ionicons
                            name='md-menu'
                            size={50}
                            onPress={ () =>
                                navigation.openDrawer()
                            }
                        />
                    </Button>
                </Left>
                <Body>
                    <Title>Header</Title>
                </Body>
                <Right />
            </Header>
        </View>
    )
}
const styles = StyleSheet.create({
    header:{
        width: '100%',
        height: 90,
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent:'center',
        paddingTop: 30,
    },
    headerTitle:{
        fontSize: 30,
        color: Colors.normalText,

    }
})
export default MyHeader;