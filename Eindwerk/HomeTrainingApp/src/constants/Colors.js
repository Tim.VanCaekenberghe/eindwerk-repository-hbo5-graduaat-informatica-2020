export default {
    primary: "#0066cc",
    tint: "#99ccff",
    secondary: "#4da6ff",
    accent: "#d9d9d9",
    normalText: "black",

};