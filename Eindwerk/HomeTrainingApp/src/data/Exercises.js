import Difficulty from '../model/Difficulty';
import MuscleGroup from '../model/MuscleGroup';
import Exercise from "../model/Exercise";

export const EXERCISE = [
    new Exercise('e1', 'Triceps met stoel','http://yoffielife.com/wp-content/uploads/2015/11/Tricep-Dip-Straight-Legs_Chair_A1.jpg', 'https://tse2.mm.bing.net/th?id=OIP.Am43yq-1rV4aZXrkh7GVpwHaLJ&pid=Api&P=0&w=300&h=300', '8 X 3','Zit op een stoel en zet je voeten op de grond. Houd je vast aan de rand van de stoel en verplaats je heupen van de stoel. {\'\\n\'} Buig je armen en beweeg je lichaam langzaam op en neer naar de grond en terug.'),
    new Exercise('e2', 'Armraises lateral', 'http://4.bp.blogspot.com/--V0h1jB54_U/T834EzWm2VI/AAAAAAAAAcc/c7VlmKzq-sc/s1600/Lateral-shoulder-raises.jpg', 'https://tse1.mm.bing.net/th?id=OIP.QPjVc8OtFkqd7OA0LLOrOAHaLJ&pid=Api&P=0&w=300&h=300', '00:30', 'Ga op de vloer staan en strek je armen zijwaarts tot op schouderhoogte.{"\\n"}Keer terug naar de beginpositie en herhaal.'),
    new Exercise('e3', 'Jumping jacks', 'https://media1.popsugar-assets.com/files/thumbor/BLn2-1T1Yp-cgpoOU76QVkuhlpc/fit-in/2048xorig/filters%3Aformat_auto-%21%21-%3Astrip_icc-%21%21-/2015/05/01/974/n/1922729/8a48e47672d474dc_c9d6640d1d97a449_jumping-jacks.xxxlarge/i/Jumping-Jacks.jpg', 'http://www.sparkpeople.com/assets/exercises/Jumping-Jacks.gif', '00:30', 'Start met je voeten bij elkaar en je armen langs je lichaam. Spring dan omhoog met je voeten gespreid en je armen boven je hoofd.'),
    new Exercise('e4', 'Diamond push up', 'https://tse1.mm.bing.net/th?id=OIP.MgREfKB8TAqyy25MnaxjiAHaEm&pid=Api&P=0&w=249&h=156', 'https://www.lifetime60day.com/wp-content/uploads/2017/11/Diamond-Push-Up.gif', '3X4', 'Start in een push-up positie. Maak met je handen een ruitvorm onder je borstkas door je wijsvingers en duimen tegen elkaar te houden. Druk vervolgens je lichaam omhoog en omlaag. Vergeet niet om je bovenlijf recht te houden.' )
];
export const DIFFICULTY = [
    new Difficulty('d1', 'Beginner'),
    new Difficulty('d2', 'Gemiddeld'),
    new Difficulty('d3', 'Gevorderd')
];

export const MUSCLEGROUP = [
    new MuscleGroup('m1', 'Chest'),
    new MuscleGroup('m2', 'Back'),
    new MuscleGroup('m3', 'Shoulder'),
    new MuscleGroup('m4', 'Chest'),
    new MuscleGroup('m5', 'Chest')

];

export const EXERCISE = [
    new Exercise('e1', 'crumble', 'https://qph.fs.quoracdn.net/main-qimg-181cd1a94e3526525f3dae6530c2f5ed')
];


