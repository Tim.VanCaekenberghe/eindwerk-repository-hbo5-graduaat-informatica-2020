class Exercise{
    constructor(id, exerciseName, image, gif, reps, description) {
        this.id = id;
        this.exerciseName = exerciseName;
        this.image = image;
        this.gif = gif;
        this.reps = reps;
        this.description = description;

    }

}

export default Exercise;