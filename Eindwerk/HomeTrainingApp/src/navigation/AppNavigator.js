import * as React from 'react';
import {View, Image, Dimensions,StyleSheet} from 'react-native';
import {Header, Body, Container, Content} from 'native-base'
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator, DrawerItem } from '@react-navigation/drawer';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import BeginnerScreen from "../screens/BeginnerScreen";
import MediumScreen from "../screens/MediumScreen";
import AdvancedScreen from "../screens/AdvancedScreen";
import Colors from '../constants/Colors'

import Home from '../screens/Home'
import Detail from '../screens/Detail'
import Settings from '../screens/Settings'
import Profile from "../screens/Profile";

const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const customDrawerContentComponent = (props) => (
    <Container>
        <Header>
            <Body>
            <Image
                source={{uri: 'https://tse2.mm.bing.net/th?id=OIP.2Wr9dHn_XTnJw5w2Bd3AMwHaEo&pid=Api&P=0&w=257&h=161'}}
                style={styles.drawerImg}
            />
            </Body>
        </Header>
    </Container>
)


function MainTabNavigator() {

    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: Colors.accent,
                inactiveTintColor: Colors.normalText,
                style: {
                    backgroundColor: Colors.primary,
                    height: 35,
                    opacity: 1
                },
                labelStyle:{
                    fontSize: 15,
                    padding: 5,
                    opacity: 1

                }
            }}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ color, size }) => {
                    let iconName
                    if (route.name == 'Home') {
                        iconName = 'ios-home'
                    } else if (route.name == 'Profile') {
                        iconName = 'ios-person'
                    }
                    return <Ionicons name={iconName} color={color} size={size} />
                }
            })}>
            <Tab.Screen name='Beginner' component={BeginnerScreen} />
            <Tab.Screen name='Gemiddeld' component={MediumScreen} />
            <Tab.Screen name='Gevorderd' component={AdvancedScreen} />
        </Tab.Navigator>

    )
}

function AppNavigator() {
  return (
    <NavigationContainer>
        <Drawer.Navigator drawerType='slide'
                          initialRouteName="Home"
                          contentComponent= {customDrawerContentComponent}
        >
            <Drawer.Screen
                name='Home'
                component={Home}
                options={{ title: 'Home'}}
            />
        <Drawer.Screen
          name='Exercises'
          component={MainTabNavigator}
          options={{ title: 'Oefeningen'}}
          />
        <Drawer.Screen
          name='Detail'
          component={Detail}
          options={{ title: 'Planner'}}
        />
        <Drawer.Screen
          name='Settings'
          component={Settings}
          options={{ title: "Voedingschema's" }}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
        drawerImg: {
            width: 150,
            height: 150,
            borderRadius: 75
        }
    }
)
export default AppNavigator;
