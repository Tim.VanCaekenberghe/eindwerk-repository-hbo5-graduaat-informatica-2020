import React, {useState} from 'react';
import { StyleSheet, View, ScrollView, Image, Modal,ImageBackground,Text, TouchableOpacity } from 'react-native';
import {Header, Title, Content, Left, Button, Right, Card, CardItem, Body} from 'native-base';
import MyTitle from '../components/MyTitle'
import {Ionicons, Entypo} from "@expo/vector-icons";
import Colors from "../constants/Colors";
import {set} from "react-native-reanimated";

const AdvancedScreen = props => {
    const [ex1Open, setEx1Open] = useState(false);
    const [ex2Open, setEx2Open] = useState(false);
    const [ex3Open, setEx3Open] = useState(false);
    const [ex4Open, setEx4Open] = useState(false);
    const [ex5Open, setEx5Open] = useState(false);
    const [ex6Open, setEx6Open] = useState(false);
    const [ex7Open, setEx7Open] = useState(false);
    const [ex8Open, setEx8Open] = useState(false);
    const [ex9Open, setEx9Open] = useState(false);
    const [ex10Open, setEx10Open] = useState(false);
    const [ex11Open, setEx11Open] = useState(false);
    const [ex12Open, setEx12Open] = useState(false);
    const [ex13Open, setEx13Open] = useState(false);
    const [ex14Open, setEx14Open] = useState(false);
    const [ex15Open, setEx15Open] = useState(false);
    const [ex16Open, setEx16Open] = useState(false);
    const [ex17Open, setEx17Open] = useState(false);
    const [ex18Open, setEx18Open] = useState(false);
    const [ex19Open, setEx19Open] = useState(false);
    const [ex20Open, setEx20Open] = useState(false);
    const [ex21Open, setEx21Open] = useState(false);

    const { navigation } = props;
    return (
        <View style={styles.viewContainer}>
            <Header style={styles.header}>
                <Left>
                    <Button transparent>
                        <Ionicons
                            name='md-menu'
                            size={50}
                            color= {Colors.normalText}
                            onPress={ () =>
                                navigation.openDrawer()
                            }
                        />
                    </Button>
                </Left>
                <Body>
                    <Title style={styles.title}>Oefeningen gevorderd</Title>
                </Body>
            </Header>
            <ScrollView contentContainerStyle={styles.container}>
                <Card style={styles.card}>
                    <Image
                        source={{uri: 'https://qph.fs.quoracdn.net/main-qimg-181cd1a94e3526525f3dae6530c2f5ed'}}
                        style={styles.headImg}
                    />
                    <Title style={styles.subTitle}>Arm</Title>
                    <Modal visible={ex1Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Triceps met stoel
                            </Text>
                            <Image source={{uri: 'https://tse2.mm.bing.net/th?id=OIP.Am43yq-1rV4aZXrkh7GVpwHaLJ&pid=Api&P=0&w=300&h=300'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8 {'\n'}{'\n'} Zit op een stoel en zet je voeten op de grond. Houd je vast aan de rand van de stoel en verplaats je heupen van de stoel. {'\n'} Buig je armen en beweeg je lichaam langzaam op en neer naar de grond en terug.  </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx1Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx1Open(true)}
                        />
                        <Image source={{uri: 'http://yoffielife.com/wp-content/uploads/2015/11/Tricep-Dip-Straight-Legs_Chair_A1.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Triceps met stoel  {"\n"}{"\n"} 3 X 8
                        </Text>
                    </Card>
                    <Modal visible={ex2Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Armraises lateral
                            </Text>
                            <Image source={{uri: 'https://tse1.mm.bing.net/th?id=OIP.QPjVc8OtFkqd7OA0LLOrOAHaLJ&pid=Api&P=0&w=300&h=300'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Tijd: 00:30{"\n"}{"\n"}Ga op de vloer staan en strek je armen zijwaarts tot op schouderhoogte.{"\n"}Keer terug naar de beginpositie en herhaal. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx2Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>

                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx2Open(true)}
                        />

                        <Image source={{uri: 'http://4.bp.blogspot.com/--V0h1jB54_U/T834EzWm2VI/AAAAAAAAAcc/c7VlmKzq-sc/s1600/Lateral-shoulder-raises.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Armraises lateral{"\n"}{"\n"} 00:30
                        </Text>
                    </Card>
                    <Modal visible={ex3Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Jumping jacks
                            </Text>
                            <Image source={{uri: 'http://www.sparkpeople.com/assets/exercises/Jumping-Jacks.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Tijd: 00:30{"\n"}{"\n"}Start met je voeten bij elkaar en je armen langs je lichaam. Spring dan omhoog met je voeten gespreid en je armen boven je hoofd.</Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx3Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>(setEx3Open(true))}
                        />
                        <Image source={{uri: 'https://media1.popsugar-assets.com/files/thumbor/BLn2-1T1Yp-cgpoOU76QVkuhlpc/fit-in/2048xorig/filters%3Aformat_auto-%21%21-%3Astrip_icc-%21%21-/2015/05/01/974/n/1922729/8a48e47672d474dc_c9d6640d1d97a449_jumping-jacks.xxxlarge/i/Jumping-Jacks.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Jumping Jacks  {"\n"}{"\n"} 00:30
                        </Text>
                    </Card>
                    <Modal visible={ex4Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Diamond push-up
                            </Text>
                            <Image source={{uri: 'https://www.lifetime60day.com/wp-content/uploads/2017/11/Diamond-Push-Up.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3x4{"\n"}{"\n"}Start in een push-up positie. Maak met je handen een ruitvorm onder je borstkas door je wijsvingers en duimen tegen elkaar te houden. Druk vervolgens je lichaam omhoog en omlaag. Vergeet niet om je bovenlijf recht te houden. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx4Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>setEx4Open(true)}
                        />
                        <Image source={{uri: 'https://tse1.mm.bing.net/th?id=OIP.MgREfKB8TAqyy25MnaxjiAHaEm&pid=Api&P=0&w=249&h=156'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Diamond push-ups  {"\n"}{"\n"} 3 X 4
                        </Text>
                    </Card>
                </Card>
                <Card style={styles.card}>
                    <Image
                        source={{uri: 'https://cdn-ami-drupal.heartyhosting.com/sites/muscleandfitness.com/files/styles/full_node_image_1090x614/public/media/cable-crossover-content-chest-pec.jpg?itok=Ztg98azg'}}
                        style={styles.headImg}
                    />
                    <Title style={styles.subTitle}>Borst</Title>
                    <Modal visible={ex5Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Hindu push ups
                            </Text>
                            <Image source={{uri: 'https://cmeimg-a.akamaihd.net/640/ppds/ab30b91c-c275-4f7c-85d1-832119c741c0.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8{"\n"}{"\n"}Begin met je handen en voeten op de vloer en je heupen in de lucht, net als een omgekeerde 'V'. Buig vervolgens je ellebogen zodat je lichaam richting de vloer kan zakken. Wanneer je lichaam bijna de vloer raakt til je je bovenlichaam zo ver mogelijk omhoog. Keer terug naar de beginpositie en herhaal. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx5Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx5Open(true)}
                        />
                        <Image source={{uri: 'http://www.simonecasagrande.com/wp-content/uploads/2012/10/P1050936-.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Hindu push-ups  {"\n"}{"\n"} 3 X 8
                        </Text>
                    </Card>
                    <Modal visible={ex6Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Cobra Stretch
                            </Text>
                            <Image source={{uri: 'https://thumbs.gfycat.com/RashVigorousArrowana-small.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Tijd: 00:20{"\n"}{"\n"}Ga op je buik liggen en buig je ellebogen met je handen onder je schouders. {'\n'}{'\n'} Duw dan je borst zo ver mogelijk van de grond af. Houd deze positie aan gedruende 20 seconden. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx6Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>

                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx6Open(true)}
                        />

                        <Image source={{uri: 'https://tse1.mm.bing.net/th?id=OIP.v8ivz1K51dVaBBmffuH0hwHaEK&pid=Api&P=0&w=267&h=151'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Cobra Stretch{"\n"}{"\n"} 00:20
                        </Text>
                    </Card>
                    <Modal visible={ex7Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Box push-ups
                            </Text>
                            <Image source={{uri: 'https://dailyburn.com/life/wp-content/uploads/2015/08/knee-push-up-new.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8{"\n"}{"\n"}  Begin op handen en voeten, met de knieën onder de billen en de handen recht onder de schouders. Buig je ellebogen en druk jezelf op. Keer terug naar de beginpositie en herhaal.      </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx7Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>(setEx7Open(true))}
                        />
                        <Image source={{uri: 'http://d1s9j44aio5gjs.cloudfront.net/2017/09/rebecca_turner_box_push_ups.png'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Box push-ups {"\n"}{"\n"} 3 X 8
                        </Text>
                    </Card>
                    <Modal visible={ex8Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Incline push up{"\n"}{"\n"}       Herhaling: 3 X 8
                            </Text>
                            <Image source={{uri: 'https://i0.wp.com/thumbs.gfycat.com/IdenticalAggressiveChihuahua-size_restricted.gif?w=1155&h=840'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Begin met een gewone push-uppositie, maar dan met de handen boven het lichaam op een stoel of bank. Duw vervolgens je lichaam omhoog en omlaag, vergeet niet om je bovenlijf recht te houden. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx8Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>setEx8Open(true)}
                        />
                        <Image source={{uri: 'https://tse4.mm.bing.net/th?id=OIP._ixsJIrXS4I_XZz1sGMgGAHaD4&pid=Api&P=0&w=349&h=184'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Incline push up  {"\n"}{"\n"} 3 X 8
                        </Text>
                    </Card>
                </Card>
                <Card style={styles.card}>
                    <Image
                        source={{uri: 'https://i.ytimg.com/vi/wY2lZw2d-24/maxresdefault.jpg'}}
                        style={styles.headImg}
                    />
                    <Title style={styles.subTitle}>Buikspieren</Title>
                    <Modal visible={ex9Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Russian Twist
                            </Text>
                            <Image source={{uri: 'https://www.healthier.qld.gov.au/wp-content/uploads/2015/07/25_M_WIP02.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 2 X 15{"\n"}{"\n"}Zit op de vloer met de knieën gebogen, hou je voeten iets van de grond en wat naar achteren gebogen. Hou je armen bij elkaar draai je armen voorzichtig naar één kant en hou je voeten stevig op en je armen parrallel aan de vloer. Keer terug naar de beginpositie en herhaal aan de andere kant. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx9Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx9Open(true)}
                        />
                        <Image source={{uri: 'https://tse4.mm.bing.net/th?id=OIP._ixsJIrXS4I_XZz1sGMgGAHaD4&pid=Api&P=0&w=349&h=184'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Russian Twist  {"\n"}{"\n"} 2 X 15
                        </Text>
                    </Card>
                    <Modal visible={ex10Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Leg Raises
                            </Text>
                            <Image source={{uri: 'https://i0.wp.com/healthcaremastery.com/wp-content/uploads/2018/02/How-to-implement-leg-raise-exercises-in-my-gym-routine.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8{"\n"}{"\n"}Ga op je rug liggen en zet ter ondersteuning je handen onder je heupen. Til vervolgens je benen omhoog in een rechte beweging, totdat ze loodrecht op de vloer staan. Keer terug naar de beginpositie en herhaal. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx10Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>

                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx10Open(true)}
                        />

                        <Image source={{uri: 'https://i.ytimg.com/vi/Wp4BlxcFTkE/maxresdefault.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Leg Raises{"\n"}{"\n"} 3 x 8
                        </Text>
                    </Card>
                    <Modal visible={ex11Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Plank
                            </Text>
                            <Image source={{uri: 'https://media.self.com/photos/57fea6654b7c91b2239d76eb/master/w_1600%2Cc_limit/FOREARM_PLANK_ROCKS.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Tijd: 00:20{"\n"}{"\n"}Plaats je ellebogen op de grond en laat je lichaam op je knieën rusten. Span de buikspieren strak op zodat je lichaam een rechte lijn vorm van je hoofd tot de knieën. Blijf vooruit kijken. Hou deze positie aan.</Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx11Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>(setEx11Open(true))}
                        />
                        <Image source={{uri: 'https://i.ytimg.com/vi/-4yRmx9IXuk/maxresdefault.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Plank  {"\n"}{"\n"} 00:20
                        </Text>
                    </Card>
                    <Modal visible={ex12Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Crunches
                            </Text>
                            <Image source={{uri: 'https://www.doctorasky.com/wp-content/uploads/2018/08/sit-up-and-crunches.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8{'\n'}{'\n'}Lig op je rug met je armen vooruit. Plaats je voeten op de grond met je benen gebogen. Til je bovenlichaam op zodat je schouders van de grond komen. Span je buikspieren maximaal op, houd deze houding 1 à 2 tellen aan en ga terug.  </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx12Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>setEx12Open(true)}
                        />
                        <Image source={{uri: 'https://i.ytimg.com/vi/_M2Etme-tfE/maxresdefault.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Crunches  {"\n"}{"\n"} 3 X 8
                        </Text>
                    </Card>
                </Card>
                <Card style={styles.card}>
                    <Image
                        source={{uri: 'http://images.askmen.com/1080x540/2016/05/05-051556-best_back_workout_routines.jpg'}}
                        style={styles.headImg}
                    />
                    <Title style={styles.subTitle}>Rug & Schouder</Title>
                    <Modal visible={ex13Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Kat-koe houding
                            </Text>
                            <Image source={{uri: 'https://www.verywellfit.com/thmb/rJ7DmtaJuP-M1PFTy-GCuJrpZhQ=/1500x1000/filters:fill(FFDB5D,1)/Fitness_Gif-1500x1000-catcow-5c5c85cac9e77c0001566641.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Ga op uw armen en benen staan met uw knieën onder uw billen en uw handen recht onder uw schouders. Haal adem en laat uw buik zakken, uw schouders achterover rollen en wend uw hoofd richting plafond. Terwijl u uitademt, buigt u uw rug omhoog en laat u uw hoofd zakken. Herhaal. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx13Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx13Open(true)}
                        />
                        <Image source={{uri: 'https://tse2.mm.bing.net/th?id=OIP.eh06m_0O3Jh8DVskSIAOmwHaHa&pid=Api&P=0&w=300&h=300'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Kat-koe houding  {"\n"}{"\n"} 3 X 8
                        </Text>
                    </Card>
                    <Modal visible={ex14Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Armen zijwaarts optillen
                            </Text>
                            <Image source={{uri: 'https://www.diet.com/fitness/images/arms_side.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling : 3 X 10 {'\n'}{'\n'}Ga op de vloer staan en houd uw voeten een schouderbreedte uit elkaar. Til uw armen omhoog tot de hoogte van uw schouders en laat ze vervolgens weer zakken. Herhaal de oefening. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx14Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>

                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx14Open(true)}
                        />

                        <Image source={{uri: 'http://4.bp.blogspot.com/--V0h1jB54_U/T834EzWm2VI/AAAAAAAAAcc/c7VlmKzq-sc/s1600/Lateral-shoulder-raises.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Armen zijwaarts {"\n"}{"\n"} 3 X 10
                        </Text>
                    </Card>
                    <Modal visible={ex15Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Knee push up
                            </Text>
                            <Image source={{uri: 'https://media.self.com/photos/583c641ca8746f6e65a60c7e/master/w_1600%2Cc_limit/DIAMOND_PUSHUP_MOTIFIED.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8 {'\n'}{'\n'} Begin met een gewone push-up, waarbij je knieën de grond raken terwijl je je voeten van de vloer houdt. {'\n'} Duw vervolgens je bovenlijf omhoog en omlaag. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx15Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>(setEx15Open(true))}
                        />
                        <Image source={{uri: 'https://tse4.mm.bing.net/th?id=OIP.PFfnOw3XWogOoHcJqgwShQHaHP&pid=Api&P=0&w=161&h=159'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Knee push up   {"\n"}{"\n"} 3 X 8
                        </Text>
                    </Card>
                    <Modal visible={ex16Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Tricep dips
                            </Text>
                            <Image source={{uri: 'https://media1.popsugar-assets.com/files/thumbor/1lxmRF1WIJfwK5pdABd-FCgDJxQ/fit-in/1024x1024/filters:format_auto-!!-:strip_icc-!!-/2016/01/25/997/n/28443503/1fcaefea0dc607b6_Gif08/i/Tricep-Dips.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8 {'\n'}{'\n'}Ga op de vloer zitten en strek je armen achteruit op de grond.{"\n"}Buig je armen ligt en duw u terug omhoog. Keer terug naar de beginpositie en herhaal. </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx16Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>setEx16Open(true)}
                        />
                        <Image source={{uri: 'https://tse3.mm.bing.net/th?id=OIP.bSbIljk7-FCZpDhNmOMoIgHaHa&pid=Api&P=0&w=300&h=300'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Tricep dips  {"\n"}{"\n"} 3 X 4
                        </Text>
                    </Card>
                </Card>
                <Card style={styles.card}>
                    <Image
                        source={{uri: 'https://tse3.mm.bing.net/th?id=OIP.WVUVLfREK6SrkHJoYUS-1QHaFj&pid=Api&P=0&w=211&h=159'}}
                        style={styles.headImg}
                    />
                    <Title style={styles.subTitle}>Benen</Title>
                    <Modal visible={ex17Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Zijwaarts springen
                            </Text>
                            <Image source={{uri: 'https://www.verywellfit.com/thmb/OGBfXurimidWOdgrCvj1nl53Qr8=/3000x2000/filters:fill(FFDB5D,1)/4-3119999-Lateral-Plyometric-Jumps-GIF-38eb617fcb63412cbb7d2b4490a85f23.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Tijd: 00:30 {'\n'}{'\n'}Sta rechtop, hou je handen voor je en spring van zij naar zij.</Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx17Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx17Open(true)}
                        />
                        <Image source={{uri: 'https://tse2.mm.bing.net/th?id=OIP.FCZoKrWGw7fNs7_95ng_fAHaGl&pid=Api&P=0&w=183&h=164'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Zijwaarts springen  {"\n"}{"\n"} 00:30
                        </Text>
                    </Card>
                    <Modal visible={ex18Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Squats
                            </Text>
                            <Image source={{uri: 'https://www.sparkpeople.com/assets/exercises/Squats.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8 {'\n'}{'\n'}Ga rechtstaan met je voeten op heupbreedte uit elkaar en je armen vooruit gestrekt. Buig dan door je knieën, terwijl je je rug recht houdt, je buikspieren intrekt. Doe dit tot je heupen parrallel met de vloer staan, span dan je achterwerk samen en ga terug rechtstaan.</Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx18Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>

                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=> setEx18Open(true)}
                        />

                        <Image source={{uri: 'https://tse1.mm.bing.net/th?id=OIP.3pYZ_9wujiXS_ofOvNl0OgHaEo&pid=Api&P=0&w=286&h=180'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Squats{"\n"}{"\n"} 00:30
                        </Text>
                    </Card>
                    <Modal visible={ex19Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Achterwaartse lunge
                            </Text>
                            <Image source={{uri: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/766/fitgif-friday-reverse-lunge-with-pulse-slider-thumbnail-override-1504017362.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 10 {"\n"}{"\n"} Sta rechtop, houd je voeten een schouderlengte uit elkaar en leg je handen op je heupen. Neem een grote stap naar achteren met je rechterbeen en zak met je lichaam tot je linkerdij evenwijdig aan de vloer is. Keer terug naar beginpositie en herhaal.</Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx19Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>(setEx19Open(true))}
                        />
                        <Image source={{uri: 'http://media1.popsugar-assets.com/files/2014/04/30/868/n/1922729/0eda7d75483dde1f_Reverse-lunge.xxxlarge/i/Backward-Power-Lunge.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Achterwaartse lunge {'\n'}{'\n'} 3 X 10
                        </Text>
                    </Card>
                    <Modal visible={ex20Open}>
                        <View style={styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Donkey kicks
                            </Text>
                            <Image source={{uri: 'https://www.healthier.qld.gov.au/wp-content/uploads/2015/07/15_M_WIP02.gif'}}
                                   style={styles.modalGif}

                            />

                            <Text style={styles.modalInfoText}>Herhaling: 3 X 8 {'\n'}{'\n'} Begin met handen en knieën op de grond. Til afwisselend je been omhoog en knijp je billen zo ver mogelijk samen.  </Text>
                            <TouchableOpacity style={styles.modalBackButton} onPress={()=>(setEx20Open(false))}><Text style={styles.modalBackButtonText}>Ga terug</Text></TouchableOpacity>
                        </View>
                    </Modal>
                    <Card style={styles.cardItem}>
                        <Entypo
                            name='open-book'
                            size={45}
                            color={Colors.normalText}
                            style={styles.bookIcn}
                            onPress={()=>setEx20Open(true)}
                        />
                        <Image source={{uri: 'https://media1.popsugar-assets.com/files/thumbor/Uh7wOyaQEL0--0nt5NHjDSxDn_Q/fit-in/2048xorig/filters:format_auto-!!-:strip_icc-!!-/2013/10/18/884/n/1922729/785196b0c13177bc_donkey-kicks-with-weights/i/Weighted-Donkey-Kick.jpg'}}
                               style={styles.exGif}

                        />
                        <Text style={styles.exTitle}>
                            Donkey kicks  {"\n"}{"\n"} 3 X 8
                        </Text>
                    </Card>
                </Card>
            </ScrollView>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.secondary,

    },
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary,
        height: '100%'
    },
    modalBackButton:{
        width: 100,
        height: 50,
        backgroundColor: Colors.secondary,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 45,
        marginTop: 20
    },
    modalBackButtonText:{
        fontSize: 20,
        color: Colors.normalText
    },
    modalCloseLogo:{
        marginTop: 15
    },
    headImg:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height:160,
        borderRadius: 25,
        borderColor:Colors.accent,
        borderWidth:2,
        overflow:'hidden',
        opacity: 0.9,
        marginBottom: 5
    },
    bookIcn:{
        paddingTop: 40,
        marginLeft: 10
    },
    exTitle:{
        color: Colors.normalText,
        fontSize: 20,
        paddingTop: 20,
        justifyContent: 'center',
        marginRight: 140
    },
    exGif:{
        width: 100,
        height: 100 ,
        marginHorizontal: 15,
        alignItems: 'center',
        marginTop: 10
    },
    modalGif:{
        width: 250,
        height: 250,
        marginTop: 40
    },
    cardItem:{
        backgroundColor: Colors.secondary,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 15,
        width: '100%',
        borderRadius: 15
    },
    viewContainer: {
        marginBottom: 65
    },
    title: {
        color: Colors.normalText,
        fontSize: 25


    },
    modalTitle:{
        fontSize: 35,
        color: Colors.normalText,
        backgroundColor: Colors.secondary,
        borderRadius: 75,
        width: 300,
        textAlign: 'center',
        height: 50
    },
    card: {
        backgroundColor: Colors.primary,
        width: '95%',
        borderRadius: 25,
        shadowColor: 'grey',
        borderWidth: 2,
        borderColor: Colors.normalText,
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        alignItems: 'center',

    },
    modalInfoText:{
        color: Colors.accent,
        width: '90%',
        textAlign: 'center',
        borderStyle: 'dotted',
        borderColor: Colors.normalText,
        fontSize: 18,
        marginTop: 20
    },
    buttonContainer: {
        backgroundColor: '#222',
        borderRadius: 5,
        padding: 10
    },
    buttonText: {
        fontSize: 20,
        color: '#fff'
    },
    subTitle: {
        color: Colors.normalText,
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: 25,
        fontWeight: 'bold'
    },
    header:{
        width: '100%',
        height: 65,
        backgroundColor: Colors.primary,
        justifyContent:'center',
        textAlign: 'center'
    }
})

export default AdvancedScreen;
