import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import {Header, Title, Content, Button, Card, CardItem, Left, Right, Body} from 'native-base';
import MyHeader from '../components/Header'
import {Ionicons} from "@expo/vector-icons";
import Colors from "../constants/Colors";



const Home = props => {
  const { navigation } = props
  return (
    <View>
      <Header style={styles.header}>
        <Left>
          <Button transparent>
            <Ionicons
                name='md-menu'
                size={50}
                color= {Colors.normalText}
                onPress={ () =>
                    navigation.openDrawer()
                }
            />
          </Button>
        </Left>
        <Body>
          <Title style={styles.title}>Home Training App</Title>
        </Body>
      </Header>
      <View style={styles.container}>
      <Image source={{uri: 'https://tse1.mm.bing.net/th?id=OIP.OVZB3NsArQa8tUYITVmYlQHaE8&pid=Api&P=0&w=229&h=153'}}
                        style={styles.homeImage}/>
                        <Title style={styles.homeTitle}>Blijf fit thuis!</Title>
        <Text style={styles.homeText}>Welkom bij de Home Training App!{'\n'}  Hier vindt u tal van oefeningen zowel voor beginners als gevorderden. Alle oefeningen kunnen worden uitgevoerd zonder toestellen, zo kan iedereen fit blijven van thuis uit!  </Text>
        <TouchableOpacity style={styles.homeBackButton} onPress={()=> navigation.navigate('Exercises')}
        ><Text style={styles.homeBackButtonText}>Start hier!</Text></TouchableOpacity>
        <Image source={{uri: 'https://i.ytimg.com/vi/u1OsZn0dD5k/maxresdefault.jpg'}}
             style={styles.homeQuoteImg}/>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: Colors.primary,
    height: '100%'
  },
  homeQuoteImg:{
    width: 250,
    height: 150,
    borderRadius: 75,
    borderColor:Colors.accent,
    borderWidth:2,
    overflow:'hidden',
    opacity: 0.8,
    marginTop: 10
  },
  homeTitle: {
color: Colors.accent,
    fontSize: 25,
    justifyContent: 'center',
    textAlign: 'center',
    borderRadius: 75,
    width: 175,
    height: 40,
    backgroundColor: Colors.secondary
  },
  homeText:{
    textAlign: 'center',
    width: '85%',
    color: Colors.accent,
    fontSize: 18,
    paddingVertical: 10
  },
  homeImage: {
    width: '100%',
    height: '25%',
    marginBottom: 10
  },
  title: {
    color: Colors.normalText,
    fontSize: 28,
    fontWeight: 'bold',
    justifyContent: 'center',
    marginRight: 10
  },
  subTitle: {
    color: Colors.primary
  },
  buttonContainer: {
    backgroundColor: '#222',
    borderRadius: 5,
    padding: 10,
    margin: 20
  },
  buttonText: {
    fontSize: 20,
    color: '#fff'
  },
  header:{
    width: '100%',
    height: 65,
    backgroundColor: Colors.primary,
    justifyContent:'center',
    textAlign: 'center'
  },
  homeBackButton:{
    width: 100,
    height: 50,
    backgroundColor: Colors.secondary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 45
  },
  homeBackButtonText:{
    fontSize: 20,
    color: Colors.accent,
    textDecorationLine: 'underline'
  }
})

export default Home;
