package chocolate_factory;

public class Chocolate {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                ChocolateBoiler chocolateBoiler = ChocolateBoiler.getInstance();
                System.out.println("instance id " + System.identityHashCode(chocolateBoiler));
                chocolateBoiler.fill();


            }
        });


        Thread t2 = new Thread(new Runnable() {
            public void run() {
                ChocolateBoiler chocolateBoiler = ChocolateBoiler.getInstance();
                System.out.println("instance id " + System.identityHashCode(chocolateBoiler));
                chocolateBoiler.drain();
            }
        });
        t1.start();
        t2.start();
    }
}




