package chocolate_factory;

public class ChocolateBoiler {
    private boolean empty;
    private boolean boiled;

    private static ChocolateBoiler uniqueInstance;

    private ChocolateBoiler() {
        System.out.println("instance created");
        empty = true;
        boiled = false;
    }

    public static ChocolateBoiler getInstance() {
        if (uniqueInstance == null) {
            synchronized (ChocolateBoiler.class){
                if(uniqueInstance ==null) {
                    uniqueInstance = new ChocolateBoiler();
                }
            }
            uniqueInstance = new ChocolateBoiler();
        }
        return uniqueInstance;
    }


    public void fill() {
        System.out.println("filling");
        if (isEmpty()) {
            empty = false;
            boiled = false;
        }
    }

    public void boil() {
        System.out.println("boiling");
        if (!isEmpty() && !isBoiled()) {
            boiled = true;
        }
    }

    public void drain() {
        System.out.println("Draining");
        if (!isEmpty() && isBoiled()) {
            empty = true;
        }
    }

    public boolean isEmpty() {
        return empty;
    }

    public boolean isBoiled() {
        return boiled;
    }
}



