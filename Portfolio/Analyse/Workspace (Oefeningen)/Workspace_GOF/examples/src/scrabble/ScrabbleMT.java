package scrabble;

public class ScrabbleMT {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                Singleton singleton = Singleton.getInstance();
                System.out.println(singleton.getLetterList());
                System.out.println("player 1" + singleton.getTiles(7));


            }
        });
        Thread t2 = new Thread(new Runnable() {
            public void run() {
                Singleton singleton = Singleton.getInstance();
                System.out.println(singleton.getLetterList());
                System.out.println("player 2" + singleton.getTiles(7));
            }
        });
        t1.start();
        t2.start();
    }
}

