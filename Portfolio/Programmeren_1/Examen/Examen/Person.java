package java_collections;

public class Person {

	private String name;
	private java_collections.Gender gender;
	
	public Person(String name, java_collections.Gender gender) {
		this.name = name;
		this.gender = gender;
	}

	public Person() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Person [name= " + name + ", gender= " + gender.getGen() + "]";
	}
	
	
	
}
