package java_collections;

public enum Gender {
	M("Male"),V("Female");
	
	
	private String gen;
	
	Gender(String gen) {
		this.gen = gen;
	}

	public String getGen() {
		return gen;
	}
	
}
