package abstractArchitecture;

import java.util.ArrayList;

public abstract class Building {
	private ArrayList<Level> levels;
	private String buildingnr;

	public Building(String buildingnr) {
		this.levels = new ArrayList<Level>();
		setBuildingnr(buildingnr);
	}

	public Building() {
		this("A");
	}

	public void addLevel(Level level) {
		levels.add(level);
	}

	public ArrayList<Level> getLevels() {
		return levels;
	}

	public void setLevels(ArrayList<Level> levels) {
		this.levels = levels;
	}

	public String getBuildingnr() {
		return buildingnr;
	}

	public void setBuildingnr(String buildingnr) {
		this.buildingnr = buildingnr.toUpperCase();
	}

}
