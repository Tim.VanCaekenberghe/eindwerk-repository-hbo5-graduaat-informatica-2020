package abstractArchitecture;

import java.util.ArrayList;

public class Level {
	private ArrayList<Room> rooms;
	private String levelnr;
	private String buildingnr;


	public Level(int levelnr,String buildingnr) throws RangeException {
		setLevelnr(levelnr,buildingnr);
		this.rooms = new ArrayList<Room>();
	}

	public void addRoom(Room room) {
		this.rooms.add(room);
	}

	public ArrayList<Room> getRooms() {
		return rooms;
	}

	public void setRooms(ArrayList<Room> rooms) {
		this.rooms = rooms;
	}

	public String getLevelnr() {
		return  buildingnr + "."+levelnr;
	}

	public void setLevelnr(int levelnr,String buildingnr) throws RangeException {
		if(levelnr >99 || levelnr <-9) {
			throw new RangeException("Levels can range from -9 basement to +99");
		}
		else {
			if (levelnr >= 0) {
				this.levelnr = this.levelnr.toString();
			}
			else {
				this.levelnr = "B" + this.levelnr.toString();
			}
		}
		this.buildingnr = buildingnr;
	}

	public void setLevelnr(String levelnr) {
		this.levelnr = levelnr;
	}

	}
