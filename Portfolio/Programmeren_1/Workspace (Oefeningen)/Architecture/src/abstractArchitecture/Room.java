package abstractArchitecture;

import java.util.ArrayList;

public class Room implements RoomInterface {
	private String function;
	private ArrayList<String> furniture;
	private int roomnr;
	private String levelnr;
	private String buildingnr;

	public Room(String function, int roomnr, String buildingnr, int levelnr) throws RangeException {
		this.function = function;
		setRoomnr(roomnr, levelnr, buildingnr);
		this.furniture = new ArrayList<String>();
	}

	public Room(String function, int roomnr, String buildingnr, String levelnr){
		this.function = function;
		this.furniture = new ArrayList<String>();
		this.levelnr = levelnr;
		this.buildingnr = buildingnr;
	}

	@Override
	public void addFurniture(String item) {
		this.furniture.add(item);

	}

	@Override
	public void showFurniture() {
		for (String s : this.furniture) {
			System.out.println(s);
		}
	}

	public void removeFurniture(int i) {
		furniture.remove(i);
	}

	public void removeFurniture(String s) {
		furniture.remove(s);
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public ArrayList<String> getFurniture() {
		return furniture;
	}

	public String getRoomnr() {
		return buildingnr + "." + levelnr + "." + roomnr;
	}

	public void setRoomnr(int roomnr, int levelnr, String buildingnr) throws RangeException {
		if (roomnr > 0 && roomnr < 100) {
			this.roomnr = roomnr;
		} else {
			throw new RangeException("the roomnumber is not in the range of 0 -> 99");
		}
		if (levelnr > 99 || levelnr < -9) {
			throw new RangeException("Levels can range from -9 basement to +99");
		} else {
			if (levelnr >= 0) {
				this.levelnr = this.levelnr.toString();
			} else {
				this.levelnr = "B" + this.levelnr.toString();
			}
		}
		this.buildingnr = buildingnr;
	}

}
