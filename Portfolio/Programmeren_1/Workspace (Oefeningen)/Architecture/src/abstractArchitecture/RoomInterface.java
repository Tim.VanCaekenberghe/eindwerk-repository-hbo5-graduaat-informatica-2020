package abstractArchitecture;


public interface RoomInterface {
	public void addFurniture(String item);
	public void showFurniture();

}
