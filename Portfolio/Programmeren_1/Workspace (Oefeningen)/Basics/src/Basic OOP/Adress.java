package basicobjectoriented;

public class Adress {
	private String streetName;
	private int houseNumber;
	private int postalCode;
	private String city;
	private String busNumber;
	
	public Adress() {
		streetName = "Homeless";
		houseNumber = 0;
		postalCode = 0;
		city = "nowhere";
	}
	public Adress(String street,int number,String city,int postalcode) {
		this.streetName = street;
		setHouseNumber(number);
		this.city = city;
	    setPostalCode(postalcode);
		
	}
	public Adress(String street,int number,String bus,String city,int postalcode) {
		this(street,number,city,postalcode);
		this.busNumber = bus;
	}
	private void invalid() {
		System.out.println("your imput was invalid");
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public int getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(int houseNumber) {
		if (houseNumber >0) {
		this.houseNumber = houseNumber;
		}	else {
			this.houseNumber = 0;
			invalid();
		}
	}
	public int getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(int postalCode) {
			if (postalCode > 0 && postalCode < 10000) {
				this.postalCode = postalCode;
			}
			else {
				this.postalCode = 0;
				invalid();
			}
				
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getBusNumber() {
		return busNumber;
	}
	public void setBusNumber(String busNumber) {
		this.busNumber = busNumber;
	}
	@Override
	public String toString() {
		return "Adress [streetName=" + streetName + ", houseNumber=" + houseNumber + ", postalCode=" + postalCode
				+ ", city=" + city + ", busNumber=" + busNumber + "]";
	}
	

}
