package basicobjectoriented;

public class Counter {
	private int counter;

	public Counter() {
		this.counter = 0;
	}

	public void counterPlus() {
		if ( this.counter < Integer.MAX_VALUE) {
			this.counter++;	
		}
		
	}

	public void counterMin() {
		if (this.counter > 0) {
			this.counter--;
		}

	}

	public void counterReset() {
		this.counter = 0;
	}

	public int getCounter() {
		return this.counter;
	}

	@Override
	public String toString() {
		return "Counter [counter=" + counter + "]";
	}

}
