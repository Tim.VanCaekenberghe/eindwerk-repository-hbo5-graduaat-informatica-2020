package basicobjectoriented;


public class CounterClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Counter teller = new Counter();
		teller.counterPlus();
		System.out.println(teller.getCounter());
		teller.counterMin();
		System.out.println(teller.getCounter());
		teller.counterMin();
		System.out.println(teller);
		teller.counterPlus();
		teller.counterPlus();
		System.out.println(teller);
		teller.counterReset();
		System.out.println(teller.getCounter());
		

	}

}
