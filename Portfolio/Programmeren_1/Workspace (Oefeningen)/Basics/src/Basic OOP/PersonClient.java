package basicobjectoriented;

import constants_and_basics.RequestInputFromUser;

public class PersonClient {

	public static void main(String[] args) {
		Pet petDog = new Pet("Balou", "dog");
		Adress adressAPerson = new Adress("Mollestraat",8,"Bus 52", "Asse",1730);
		Person aPerson = new Person("Jeroen",adressAPerson,25,petDog);
		aPerson.talk();
		Person bPerson = new Person();
		bPerson.setName("Jef");
		System.out.println(bPerson.getName());
		RequestInputFromUser age = new RequestInputFromUser("Set age please", "number");
		bPerson.setAge(age.getInt());
		System.out.println(bPerson.getAge());
		System.out.println(bPerson.toString());
		
		
		RequestInputFromUser.closeScanner();
	}

}
