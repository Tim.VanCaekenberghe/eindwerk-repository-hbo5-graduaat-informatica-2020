package basicexercises;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.time.LocalDate;
import constants_and_basics.PrintConstants;
import constants_and_basics.Printing;
import constants_and_basics.RequestInputFromUser;
import constants_and_basics.Rng;
import memeprint.JediSecrets;

public class Exercises {
	public static void main(String[] args) {
		printHelloWorld();
		displayPattern();
		summation();
		average();
		helloWorldTime();
		bigIntegerCalc();
		printMinMaxofPrimitives();
		JediSecrets.jediSecrets(); // when a method is public you can call it in another class.
		HelloWorld.reference(); // calling a public method from another class.
		averageSumAndHighLowCalc();
		compareNumbers();
		nameAndInitials();
		fibbonachi();
		comparingIntegers();
		cookieCalorie();
		ingredientAdjuster();
		encryptionExercise();
		decryptionExercise();
		populationProjection();
		runwayLength();
		financialValue();
		stringManipexercise();
		rngesusGenerator();
		productPriceCalculator();
		inputArray();
		passFail();
		wordNumber();
		pyramide();
		guessingGame();
		gradesOfmultipleStudents();
		kilometerPerMiles();
		bmi();
		RequestInputFromUser.closeScanner();
	}

	private static void printHelloWorld() {
		System.out.println("Hello, World!");
	}

	private static void displayPattern() {
		System.out.println("J A V V A\r\n" + "J A A V V A A\r\n" + "J J AAAAA V V AAAAA\r\n" + "J J A A V A A");
	}

	private static void summation() {
		int a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7, h = 8, i = 9;
		System.out.println("Sum=" + (a + b + c + d + e + f + g + h + i));
	}

	private static void average() {
		int distance = 14;
		double timeMinutes = 45.5;
		int timeSeconds = 45 * 60 + 30;
		int secondsInHour = 3600;
		double speed;
		double speedSecond;
		speed = ((distance / 1.6) / (timeMinutes / 60));
		speedSecond = (distance / 1.6) / ((double) timeSeconds / secondsInHour); // WARNING cast one of the 2 ints to a
																					// double, else java rounds & you
																					// get a mathematical error
		System.out.println("The runner runs a average of " + speed + " Miles per hour, calculated with minutes.");
		System.out.println("The runner runs a average of " + speedSecond + " Miles per hour, calculated with seconds.");
	}

	public static void helloWorldTime() { // REMINDER IMPORT UTILITY TO RUN LOCALDATE
		LocalDate date = LocalDate.now();
		System.out.println("Hello, World, today's date: " + date);
	}

	private static void bigIntegerCalc() {// REMINDER IMPORT JAVA.MATH
		BigInteger integer1 = new BigInteger("2000000000");
		BigInteger sumInteger = new BigInteger("0");
		sumInteger = integer1.add(integer1);
		System.out.println("De uitkomst van " + integer1 + " + " + integer1 + " = " + sumInteger);
	}

	public static void printMinMaxofPrimitives() {
		System.out.println("byte min value = " + Byte.MIN_VALUE);
		System.out.println("byte max value = " + Byte.MAX_VALUE);
		System.out.println("short min value = " + Short.MIN_VALUE);
		System.out.println("short max value = " + Short.MAX_VALUE);
		System.out.println("int min value = " + Integer.MIN_VALUE);
		System.out.println("int max value = " + Integer.MAX_VALUE);
		System.out.println("long min value = " + Long.MIN_VALUE);
		System.out.println("long max value = " + Long.MAX_VALUE);
		System.out.println("float min value = " + Float.MIN_VALUE);
		System.out.println("float max value = " + Float.MAX_VALUE);
		System.out.println("double min value = " + Double.MIN_VALUE);
		System.out.println("double max value = " + Double.MAX_VALUE);
		System.out.println("character min value = " + (int) Character.MIN_VALUE);
		System.out.println("character max value = " + (int) Character.MAX_VALUE);
	}

	public static void averageSumAndHighLowCalc() {
		RequestInputFromUser firstNumber = new RequestInputFromUser("a integer please", "number");
		RequestInputFromUser secondNumber = new RequestInputFromUser("a integer please", "number");
		RequestInputFromUser thirdNumber = new RequestInputFromUser("a integer please", "number");
		
		System.out.println(
				"The average of " + firstNumber.getInt() + PrintConstants.COMMA + secondNumber.getInt() + PrintConstants.ANDSYMBOL
						+ thirdNumber.getInt() + PrintConstants.EQUALSYMBOL + ((firstNumber.getInt() + secondNumber.getInt() + thirdNumber.getInt()) / 3));
		System.out.println("The sum of " + firstNumber + PrintConstants.COMMA + secondNumber + PrintConstants.ANDSYMBOL
				+ thirdNumber.getInt() + PrintConstants.EQUALSYMBOL + ((firstNumber.getInt() + secondNumber.getInt() + thirdNumber.getInt())));
		System.out.println(
				"The highest number entered is: " + (Math.max(firstNumber.getInt(), Math.max(secondNumber.getInt(), thirdNumber.getInt()))));
		System.out.println(
				"The lowest number entered is: " + (Math.min(firstNumber.getInt(), Math.min(secondNumber.getInt(), thirdNumber.getInt()))));
	}

	public static void compareNumbers() {
		int firstNumber = new RequestInputFromUser("a integer please", "number").getInt();
		int secondNumber = new RequestInputFromUser("a integer please", "number").getInt();
		if (firstNumber > secondNumber) {
			System.out.println(firstNumber + "is larger");
		}
		if (firstNumber < secondNumber) {
			System.out.println(secondNumber + "is larger");
		}
		if (firstNumber == secondNumber) {
			System.out.println("These numbers are equal");
		}
	}

	private static void fibbonachi() {
		System.out.println("The first 20 numbers of Fibonacci are: ");

		StringBuilder sb = new StringBuilder();
		int[] fibonacci = new int[20];
		fibonacci[0] = 1;
		fibonacci[1] = 1;
		int number1 = fibonacci[0] + fibonacci[1];
		sb.append(fibonacci[0] + " " + fibonacci[1] + " ");
		for (int i = 2; i < fibonacci.length; i++) {
			fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
			sb.append(fibonacci[i] + " ");
			number1 = number1 + fibonacci[i];
		}
		String result = sb.toString();
		System.out.println(result);
		double number3 = (double) number1 / fibonacci.length;
		System.out.println("The average is " + number3);
	}

	private static void comparingIntegers() {
		int value1 = new RequestInputFromUser("a integer please", "number").getInt();
		int value2 = new RequestInputFromUser("a integer please", "number").getInt();
		int value3 = new RequestInputFromUser("a integer please", "number").getInt();
		System.out.println("The sum of your input equals " + (value1 + value2 + value3));
		if (value1 == value2 && value1 == value3) {
			System.out.println("the numbers you've entered are equal");

		} else {
			System.out.println("The average of your input equals " + ((value1 + value2 + value3) / 3));
			System.out.println((Math.max(value1, Math.max(value2, value3))) + " is larger");
		}

	}

	private static void cookieCalorie() {
		int cookiesEaten = new RequestInputFromUser("how many cookies did you eat?", "number").getInt();
		while (true) {
			if (cookiesEaten < (Integer.MAX_VALUE / 30)) {
				break;
			} else {
				cookiesEaten = new RequestInputFromUser("You pig, it's physically impossible to eat more than " + (Integer.MAX_VALUE / 30) +"cookies.\nHow many cookies did you really eat?", "number").getInt();
			}
		}

		int singlecookieCalories = 30;
		System.out.println(
				"You Monster, you consumed " + (singlecookieCalories * cookiesEaten) + " Calories, you dirty pig!");
	}

	private static void nameAndInitials() {
		String firstName = new RequestInputFromUser("your first name please.", "text").getInput();
		String middleName = new RequestInputFromUser("your middle name please.", "text").getInput();
		String lastName = new RequestInputFromUser("your sur name please.", "text").getInput();
		char firstInitial = firstName.charAt(0);
		char middleInitial = middleName.charAt(0);
		char lastInitial = lastName.charAt(0);
		System.out
				.println(new StringBuilder().append(firstInitial).append(middleInitial).append(lastInitial).toString());
	}

	private static void ingredientAdjuster() {
		int sugar = 2;
		int butter = 1;
		double flour = 2.75;
		int produce = 48; 
		int requestedCookies = new RequestInputFromUser("how many cookies do you want to bake?", "number").getInt();
		System.out.println("You will need " + (sugar / ((double) produce / requestedCookies)) + " cups of sugar,");
		System.out.println("also grab " + (butter / ((double) produce / requestedCookies)) + " cups of butter,");
		System.out
				.println("to finish off, grab " + (flour / ((double) produce / requestedCookies)) + " cups of floor.");
	}

	private static int encryptionPin(int pin) {
		String InputPin = String.format("%04d", (pin));
		int[] pinSeperated = new int[4];
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < pinSeperated.length; i++) {
			pinSeperated[i] = Character.getNumericValue(InputPin.charAt(i));
			pinSeperated[i] = (pinSeperated[i] + 7) % 10;
		}
		sb.append(pinSeperated[2]);
		sb.append(pinSeperated[3]);
		sb.append(pinSeperated[0]);
		sb.append(pinSeperated[1]);
		return Integer.parseInt(sb.toString());
	}

	private static int decryptionPin(int pin) {
		String InputPin = String.format("%04d", (pin));
		int[] pinSeperated = new int[4];
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < pinSeperated.length; i++) {
			if (InputPin.charAt(i) == 0) {
				pinSeperated[i] = 3 % 10;
			} else {
				pinSeperated[i] = Character.getNumericValue(InputPin.charAt(i));
				pinSeperated[i] = (pinSeperated[i] + 3) % 10;
			}
		}
		sb.append(pinSeperated[2]);
		sb.append(pinSeperated[3]);
		sb.append(pinSeperated[0]);
		sb.append(pinSeperated[1]);
		return Integer.parseInt(sb.toString());
	}

	private static void encryptionExercise() {
		int inputtedPin = new RequestInputFromUser("4 Digit Pin Please.", "number").getInt();
		while (true) {
			if (inputtedPin > 9999) {
				inputtedPin =new RequestInputFromUser("You have not entered a 4 digit pin, please put in a 4 digit pin.", "number").getInt();
			} else {
				System.out.println("the Encrypted pin is " + String.format("%04d", (encryptionPin(inputtedPin))));
				return;
			}
		}

	}

	private static void decryptionExercise() {
		int inputtedPin = new RequestInputFromUser("4 Digit Pin Please.", "number").getInt();
		while (true) {
			if (inputtedPin > 9999) {
				inputtedPin = new RequestInputFromUser("You have not entered a 4 digit pin, please put in a 4 digit pin.", "number").getInt();
			} else {
				System.out.println("the Encrypted pin is " + String.format("%04d", (decryptionPin(inputtedPin))));
				return;
			}
		}
	}

	private static void populationProjection() {
		int birth = 7;
		int death = 13;
		int immigrant = 45;
		int currentpop = 312032486;
		double numberOfYearsPassed = (new RequestInputFromUser("how many years have passed?", "number").getDouble());
		int expectedPop = (currentpop)
				+ (int) ((numberOfYearsPassed * ((364.25) * (24 * 60 * 60))) * (1 / birth + 1 / death + 1 / immigrant));

		System.out.println("The Expected population in " + numberOfYearsPassed + " years is " + expectedPop);
	}

	private static void runwayLength() {
		double acceleration = new RequestInputFromUser("What is the acceleration in m/s?", "number").getDouble();
		double takeoffSpeed = new RequestInputFromUser("What is the Takeoff Speed of the planein m/s^2?", "number").getDouble();
		System.out.println(
				"The runway needs be at least " + ((Math.pow(takeoffSpeed, 2)) / acceleration) + " Meter long");
	}

	private static void financialValue() {
		double currentCash = new RequestInputFromUser("What amount of cash are you starting with?", "number").getDouble();
		double intrestMonth = new RequestInputFromUser("What's the monthly intrest?\\ngive me the % without the % sign.", "number").getDouble();
		double intrestYear =new RequestInputFromUser("What's the yearly intrest?\\ngive me the % without the % sign.", "number").getDouble();
		double savingValue = new RequestInputFromUser("How much are you saving each month?", "number").getDouble();
		int timeSpend = new RequestInputFromUser("How long are you saving for?\\nThis is expressed in a number of months.", "number").getInt();
		double yearlystart = 0;
		DecimalFormat df = new DecimalFormat("#.##");
		for (int i = 0; i <= timeSpend; i++) {
			if (i == 0) {
				yearlystart = currentCash;
			}
			if ((i + 1) % 12 == 0 && i != 0) {
				currentCash = currentCash + (yearlystart * (intrestYear / 100));
				yearlystart = currentCash;
			}
			currentCash = currentCash + currentCash * (intrestMonth / 100);
			currentCash = currentCash + savingValue;

		}

		System.out
				.println("after " + timeSpend + " months, you will have �" + df.format(currentCash) + " to your name.");
	}

	private static void stringManipexercise() {
		StringBuilder sb = new StringBuilder();
		String[] name = new RequestInputFromUser("name please", "text").getInput().split(" ");
		sb.append(Character.toUpperCase(name[0].charAt(0)));
		sb.append((name[0].substring(1).toLowerCase()));
		sb.append(" ");
		for (int i = 1; i < name.length; i++) {
			sb.append(name[i].toUpperCase());
			sb.append(" ");
		}
		System.out.println(sb.toString());
	}

	private static void rngesusGenerator() {
		int minimalValue = new RequestInputFromUser("Please give me a minimal value boundery.","number").getInt();
		int maximalValue = new RequestInputFromUser("Please give me a maximal value boundery.", "number").getInt();
		int randomInt = Rng.RandomInteger(minimalValue, maximalValue);
		System.out.println(randomInt);
	}

	private static String[][] generateMultiplcationTable(int limit) {
		{
			String[][] table = new String[limit + 1][limit + 1];
			for (int row = 0; row < table.length; row++) {
				for (int column = 0; column < table[row].length; column++) {
					if (row == 1 && !(row == 1 && column == 1)) {
						table[row][column] = "-";
					} else if (column == 1 && !(row == 1 && column == 1)) {
						table[row][column] = "|";
					} else if (row == 1 && column == 1) {
						table[row][column] = "+";
					} else if (row == 0 && column > 1) {
						table[row][column] = Integer.toString((row + 1) * (column));
					} else if (column == 0 && row > 1) {
						table[row][column] = Integer.toString((row) * (column + 1));
					} else if (column == 0 && row == 0) {
						table[row][column] = Integer.toString((row + 1) * (column + 1));
					} else {
						table[row][column] = Integer.toString((row) * (column));
					}
				}

			}
			return table;
		}
	}

	private static void inputArray() {
		int limit = new RequestInputFromUser("Please give me the LIMIT of the Table","number").getInt();
		System.out.println("The multiplication table of " + limit);
		Printing.print2DArrayString(generateMultiplcationTable(limit));

	}

	private static void productPriceCalculator() {
		int price = 35;
		int quantity = 5;
		int taxRate = 21;
		System.out.println("Total price : " + ((price * quantity) * ((1 + ((double) taxRate / 100)))));
	}

	private static void passFail() {
		int check = new RequestInputFromUser("please give grade", "number").getInt();
		if (check >= 50) {
			System.out.println("PASS");

		} else {
			System.out.println("FAIL");
		}
	}

	private static void wordNumber() {
		int digit = new RequestInputFromUser("enter a single digit number.", "number").getInt();
		String number = " ";
		while (true) {
			if (digit > 9) {
				digit = new RequestInputFromUser("You have not entered a 1 digit number, please put in a 1 digit number.", "number").getInt();
			} else {
				switch (digit) {
				case 1:
					number = "ONE";
					break;
				case 2:
					number = "TWO";
					break;
				case 3:
					number = "THREE";
					break;
				case 4:
					number = "FOUR";
					break;
				case 5:
					number = "FIVE";
					break;
				case 6:
					number = "SIX";
					break;
				case 7:
					number = "SEVEN";
					break;
				case 8:
					number = "EIGHT";
					break;
				case 9:
					number = "NINE";
					break;
				}
				System.out.println(number);
				return;
			}
		}
	}

	private static void pyramide() {
		int start = 2;
		int height = new RequestInputFromUser("how high will you go?", "number").getInt();
		while (true) {
			if (height >= 32) {
				height = new RequestInputFromUser("Thou can't go higher than 31!\\nPlease enter a number below 32.", "number").getInt();
			} else {
				System.out.println(" " + 1);
				for (int i1 = 1; i1 < height; i1++) {
					StringBuilder sbReversed = new StringBuilder();
					StringBuilder sbNormal = new StringBuilder();
					for (int i2 = 0; i2 < i1; i2++) {
						int solved = (int) (start / (Math.pow(2, (i2 + 1))));
						if (solved > 10) {
							String solvedString = Integer.toString(solved);
							StringBuilder solvedStringBuilder = new StringBuilder();
							solvedStringBuilder.append(solvedString);
							solvedStringBuilder.reverse();
							sbReversed.append(solvedStringBuilder + " ");
							sbNormal.append(solvedString + " ");
						} else {
							sbNormal.append(solved + " ");
							sbReversed.append(solved + " ");
						}
					}
					sbReversed.reverse();
					sbReversed.append(" ");
					sbReversed.append(start + " ");
					sbReversed.append(sbNormal);
					System.out.println(sbReversed.toString());
					start = start * 2;
				}
				return;
			}
		}
	}

	private static void guessingGame() {
		double[] inputMinMax = new double[2];
		inputMinMax[0] = new RequestInputFromUser("what's the minimal value", "number").getDouble();
		inputMinMax[1] = new RequestInputFromUser("what's the maximal value", "number").getDouble();
		while (true) {
			if (inputMinMax[1] < inputMinMax[0] || inputMinMax[0] < 0 || inputMinMax[1] < 0) {
				System.out.println(
						"the #1 value is min, the #2 value is max, please don't make #2 larger then #1.\nOr make value #1 | value #2 negative.");
				inputMinMax[0] = new RequestInputFromUser("what's the minimal value", "number").getDouble();
				inputMinMax[1] = new RequestInputFromUser("what's the maximal value", "number").getDouble();
			} else {
				break;
			}
		}
		int toBeGuessedValue = Rng.RandomInteger((int) inputMinMax[0], (int) inputMinMax[1]);
		System.out.println(PrintConstants.getLine(29, "="));
		System.out.println("Welcome to the Guessing game!\nyou need to guess a value between " + (int) inputMinMax[0]
				+ PrintConstants.ANDSYMBOL + (int) inputMinMax[1]);
		System.out.println(PrintConstants.getLine(29, "="));
		System.out.println(PrintConstants.SPACE);
		System.out.println("lives left = 3");
		for (int i = 0; i < 3; i++) {
			int guess = new RequestInputFromUser("Guess the value!", "number").getInt();
			if (toBeGuessedValue == guess) {
				System.out.println("Congratulations, you guessed correctly!");
				return;
			} else {
				if (toBeGuessedValue < guess) {
					System.out.println("Unfortunatly you didn't guess correctly, you guessed too HIGH!");
				} else {
					System.out.println("Unfortunatly you didn't guess correctly, you guessed too LOW!");
				}
				System.out.println("Try again!\nLives Left = " + (3 - (i + 1)));
			}
		}
		System.out.println("GAME OVER!\nThe value you needed to guess was: " + toBeGuessedValue);
	}

	private static void gradesOfmultipleStudents() {
		int[] grades = new int[new RequestInputFromUser("How many students do you have?", "number").getInt()];
		int sumOfAllGrades = 0;
		int maxScore = 0;
		int minScore = 0;
		for (int i = 0; i < grades.length; i++) {
			grades[i] = new RequestInputFromUser("Enter the grade for student  " + (i + 1), "number").getInt();
			if (grades[i] < 0) {
				System.out.println("Cancelled by user");
				return;
			}
			maxScore = Math.max(grades[i], maxScore);
			sumOfAllGrades = sumOfAllGrades + grades[i];
			if (i == 0) {
				minScore = grades[i];
			}
			minScore = Math.min(grades[i], minScore);

		}
		System.out.println("the average grade is: " + (sumOfAllGrades / grades.length));
		System.out.println("The lowest score is: " + minScore);
		System.out.println("the highest score is: " + maxScore);
	}

	private static void kilometerPerMiles() {
		double milesEntered = new RequestInputFromUser("What amount of miles do you want to convert to kilometers?", "number").getDouble();
		double kilometersCalculated = milesEntered * PrintConstants.KILOMETERS_PER_MILE;
		System.out.println(milesEntered + " in kilometers is: " + kilometersCalculated);
	}

	private static void bmi() {
		double[] inputs = new double[2];
		System.out.println(
				"In the first value, please enter your weight in pounds,\nin the second value, please enter your height in inches.");
		for (int i = 0; i < inputs.length; i++) {
			inputs[i] = new RequestInputFromUser("value " + (i + 1) + " please", "number").getDouble();
		}
		inputs[0] = inputs[0] * PrintConstants.POUNDS_PER_KILO;
		inputs[1] = inputs[1] * PrintConstants.INCH_PER_METER;
		System.out.println("your BMI is " + (inputs[0] / (inputs[1] * inputs[1])));
	}

}
