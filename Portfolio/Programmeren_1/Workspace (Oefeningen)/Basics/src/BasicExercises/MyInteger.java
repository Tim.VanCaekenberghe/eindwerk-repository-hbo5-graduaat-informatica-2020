package basicexercises;

public class MyInteger {
	private int value;

	public MyInteger() {
		this.value = 0;
	}

	public MyInteger(int value) {
		this.value = value;
	}

	public boolean isEven() {
		return this.value % 2 == 0;

	}

	public boolean isOdd() {
		return !this.isEven();
	}

	public boolean isPrime() {
		if (value == 1) {
			return false;
		} else if (value == 2 || value == 3) {
			return true;
		} else {
			for (int i = 2; i <= (value / 2); i++) {
				if (value % i == 0) {
					return false;
				}
			}
			return true;
		}

	}

	private boolean equals(MyInteger x) {
		return this.value == x.getValue();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MyInteger) {
			return equals((MyInteger) obj);
		}
		if (obj instanceof Integer) {
			return equals(new MyInteger((Integer) obj));
		}
		return false;
	}

	public int getValue() {
		return value;
	}

	public static MyInteger parseInt(char[] chars) {
		String sequence = "";
		for (char c : chars) {
			sequence += c;
		}
		return parseInt(sequence);
	}

	public static MyInteger parseInt(String s) {
		return new MyInteger(Integer.valueOf(s.trim()));
	}
}
