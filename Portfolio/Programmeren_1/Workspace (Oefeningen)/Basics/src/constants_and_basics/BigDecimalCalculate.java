package constants_and_basics;

import java.math.BigDecimal;

public class BigDecimalCalculate {
	
	public static BigDecimal BigDecimalcalculateSum(double double1 ,double double2) {
		BigDecimal bigDecimal1 = new BigDecimal(double1);
		BigDecimal bigDecimal2 = new BigDecimal(double2);
		BigDecimal bigDecimalResult = new BigDecimal(0);
		bigDecimalResult = bigDecimal1.add(bigDecimal2);
		return bigDecimalResult;
	}
	public static BigDecimal BigDecimalcalculateMultiply(double double1 ,double double2) {
		BigDecimal bigDecimal1 = new BigDecimal(double1);
		BigDecimal bigDecimal2 = new BigDecimal(double2);
		BigDecimal bigDecimalResult = new BigDecimal(0);
		bigDecimalResult = bigDecimal1.multiply(bigDecimal2);
		return bigDecimalResult;
	}

}
