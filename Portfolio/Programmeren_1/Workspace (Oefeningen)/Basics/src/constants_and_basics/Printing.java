package constants_and_basics;

public class Printing {
	public static void print2DArrayInt(int[][] array2d) {
		for(int col=0;col<array2d.length;col++) {
			for(int row=0; row < (array2d[col].length);row++) {
				System.out.print(array2d[col][row] + " ");
			}
			System.out.println(" ");
		}
	}
	public static void print2DArrayString(String[][] array2d) {
		for(int col=0;col<array2d.length;col++) {
			for(int row=0; row < (array2d[col].length);row++) {
				System.out.print(array2d[col][row] + " ");
			}
			System.out.println(" ");
		}
	}
	public static void print2DArrayDbl(double[][] array2d) {
		for(int col=0;col<array2d.length;col++) {
			for(int row=0; row < (array2d[col].length);row++) {
				System.out.print(array2d[col][row] + " ");
			}
			System.out.println(" ");
		}
	}
	public static void print3DArrayInt(int[][][] array3d) {
		for(int height = 0; height<array3d.length;height++) {
		for(int col=0;col<array3d[height].length;col++) {
			for(int row=0; row < (array3d[col][height].length);row++) {
				System.out.print(array3d[col][row][height] + " ");
			}
			System.out.println(" ");
		}
		System.out.println("\n");
		}
	}
	public static void print3DArrayString(String[][][] array3d) {
		for(int height = 0; height<array3d.length;height++) {
		for(int col=0;col<array3d[height].length;col++) {
			for(int row=0; row < (array3d[col][height].length);row++) {
				System.out.print(array3d[col][row][height] + " ");
			}
			System.out.println(" ");
		}
		System.out.println("\n");
		}
	}
	public static void print3DArrayDbl(double[][][] array3d) {
		for(int height = 0; height<array3d.length;height++) {
		for(int col=0;col<array3d[height].length;col++) {
			for(int row=0; row < (array3d[col][height].length);row++) {
				System.out.print(array3d[col][row][height] + " ");
			}
			System.out.println(" ");
		}
		System.out.println("\n");
		}
	}
	public static void main(String[] args) {
		int[][][] test= new int[2][2][2];
		int counter = 0;
		for(int h=0;h<test.length;h++) {
			for( int c=0;c<test[h].length;c++) {
				for(int r=0;r<test[h][c].length;r++) {
				test[c][r][h]= counter;
				counter++;
				}
			}
		}
		print3DArrayInt(test);
		
	}
}
