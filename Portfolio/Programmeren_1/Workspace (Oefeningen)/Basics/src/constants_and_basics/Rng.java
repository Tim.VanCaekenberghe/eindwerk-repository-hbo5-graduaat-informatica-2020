package constants_and_basics;

public class Rng {
	public static int RandomInteger(int min, int max) {
		double randomDouble = Math.random();
		randomDouble = randomDouble * max + min;
		int randomInt = (int) randomDouble;
		return randomInt;
	}
}
