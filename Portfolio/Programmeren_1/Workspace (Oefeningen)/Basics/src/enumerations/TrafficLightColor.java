package enumerations;

public enum TrafficLightColor {
	RED(5),
	GREEN(8),
	YELLOW(2);
	
	private int duration;
	
	private TrafficLightColor(int duration) {
		this.duration = duration;
	}
	
	@Override
	public String toString() {
		return this.name() + " duration " + this.duration;
	}

}
