package newUser;

public class Account {
       private String code;
       private double saldo;
       
       public Account(String code, double saldo) {
             if(isValidCode(code)) {
                    this.code = code;
                    this.saldo = saldo;
             } else {
                    System.out.println("Ongeldige code");
             }
       }
       
       public Account(String code){
             this(code, 0);
       }
       
       public boolean verifyCode(String code) {
             if(this.code.contentEquals(code)){
                    return true;
             } else {
                    return false;
             }
       }
       
       private boolean isValidCode(String code) {
             if(code.matches("[0-9]{4}")) {
                    return true;
             } else {
                    return false;
             }
       }
       
       public String getCode() {
             return code;
       }

       public void setCode(String code) {
             if(isValidCode(code)) {
                    this.code = code;
             } else {
                    System.out.println("Ongeldige code");
             }
       }

       public double getSaldo() {
             return saldo;
       }

       public void setSaldo(double saldo) {
             this.saldo = saldo;
       }
}
