package newUser;

public class UserData {
       private User[] users;
       private int numberOfUsers;
       
       public UserData() {
             this.users = new User[5];
             this.numberOfUsers = 0;
       }
       
       public void save(User user) {
             if(numberOfUsers < users.length) {
                    users[numberOfUsers] = user;
                    numberOfUsers++;
             }
       }
}
