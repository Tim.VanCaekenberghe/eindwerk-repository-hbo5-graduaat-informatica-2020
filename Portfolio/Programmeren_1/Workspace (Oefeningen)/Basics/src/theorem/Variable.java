package theorem;

public class Variable {
int InitIalised;
	
public static void main(String[] args){
	//Declaration variables of type primitive
	/*variable*/ int ditIseenInteger = 0; /*letterlijke waarde:litteral*/
	byte ditIseenByte =12;
	int overFlowGenerator = 2147483646;
	boolean TRUE= false; 
	short ditIsEenShort = 11;
	char letterA = 'a';
	float floatvalue =5.8F; //doesn't compile without the F, without it java think's it's a double
	double doubleValue = 5.8;
	boolean newBool = TRUE; //we can use a already declared variable here
	
	
	System.out.println(overFlowGenerator);
	System.out.println(newBool);
	System.out.println(ditIseenInteger);
	System.out.println(ditIseenByte);
	System.out.println(ditIsEenShort);
	System.out.println(floatvalue);
	System.out.println(letterA);
	System.out.println(doubleValue);
	
	if(ditIseenByte > 0) {
		if(ditIseenByte >10) {
			if(ditIseenByte >30) {
				System.out.println(">30");
				return;
			}
			System.out.println(">10");
			return;
		}
		System.out.println(">0");
		return;
	}

	
}

}
