package Animals;


import interfaces.Flying;
import interfaces.Noisy;

public abstract class Animal implements Flying, Noisy {
	private String gender;

	public Animal(String gender) {
		super();
		this.gender = gender;
	}

	public Animal() {
		
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}



}
