package Animals;

import interfaces.Flying;
import interfaces.Noisy;

public class Bird extends Animal implements Noisy, Flying {
	
	private String featherColor;

	public Bird(String gender, String featherColor) {
		super(gender);
		this.featherColor = featherColor;
	}

	public Bird() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void fly() {
		System.out.println("Bird flying");
	}
	
	@Override
	public void makeNoise() {
		System.out.println("Bird whistle");
	}

	public String getFeatherColor() {
		return featherColor;
	}

	public void setFeatherColor(String featherColor) {
		this.featherColor = featherColor;
	}

}
