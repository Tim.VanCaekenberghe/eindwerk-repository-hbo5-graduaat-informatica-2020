package Animals;

import interfaces.Noisy;

public class Cat extends Animal implements Noisy {

	private String name;

	public Cat(String name) {
		super();
		this.name = name;
	}

	public Cat() {
		super();
	}
	
	@Override
	public void makeNoise() {
		System.out.println("Miauw");
	}
	
}
