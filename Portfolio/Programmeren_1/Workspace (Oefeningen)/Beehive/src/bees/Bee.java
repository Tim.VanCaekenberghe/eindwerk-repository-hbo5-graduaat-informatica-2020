package bees;

import Animals.Animal;
import interfaces.Flying;
import interfaces.Noisy;

public abstract class Bee extends Animal implements Noisy, Flying {

	private String color;
	private double size;
	private double wingspan;
	
	public Bee(String color, double size, double wingspan) {
		super();
		this.color = color;
		this.size = size;
		this.wingspan = wingspan;
	}

	public Bee() {
		this.color = "Black/Yellow";
		this.size = 1.0;
		this.wingspan = 1.0;
	}
	
	public void fly() {}
	
	public void makeNoise() {
		System.out.println("Bzzzzz");
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getWingspan() {
		return wingspan;
	}

	public void setWingspan(double wingspan) {
		this.wingspan = wingspan;
	}
	
	
	
	
}
