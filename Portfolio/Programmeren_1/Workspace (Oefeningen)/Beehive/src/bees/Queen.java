package bees;

public class Queen extends Bee {

	private String food;

	public Queen(String color, double size, double wingspan, String food) {
		super(color, size, wingspan);
		this.food = food;
	}
	
	

	public String getFood() {
		return food;
	}

	public void setFood(String food) {
		this.food = food;
	}
	

	
}
