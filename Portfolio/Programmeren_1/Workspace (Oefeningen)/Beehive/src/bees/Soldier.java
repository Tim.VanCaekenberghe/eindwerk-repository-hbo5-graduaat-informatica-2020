package bees;

public class Soldier extends Bee {

	private String weapon;

	public Soldier(String color, double size, double wingspan, String weapon) {
		super(color, size, wingspan);
		this.weapon = weapon;
	}

	public String getWeapon() {
		return weapon;
	}

	public void setWeapon(String weapon) {
		this.weapon = weapon;
	}
	
	
}
