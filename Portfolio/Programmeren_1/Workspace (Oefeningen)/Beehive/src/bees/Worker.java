package bees;

public class Worker extends Bee {

	private double capacity;
	private double load;
	private int strength;
	
	
	
	public Worker() {
		super();
		this.capacity = 2;
		this.load = 2;
		this.strength = 5;
	}

	public Worker(String color, double size, double wingspan, double capacity, double load, int strength) {
		super(color, size, wingspan);
		this.capacity = capacity;
		this.load = load;
		this.strength = strength;
	}
	
	public void gatherNectar() {
		System.out.println("Gathering nectar");
	}
	
	public void repairHive() {
		System.out.println("repair hive");
	}

	public double getCapacity() {
		return capacity;
	}

	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}

	public double getLoad() {
		return load;
	}

	public void setLoad(double load) {
		this.load = load;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}
	
	
}
