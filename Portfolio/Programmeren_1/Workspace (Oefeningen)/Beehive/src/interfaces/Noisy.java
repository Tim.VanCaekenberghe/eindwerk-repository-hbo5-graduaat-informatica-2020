package interfaces;

public interface Noisy {

	void makeNoise();
}
