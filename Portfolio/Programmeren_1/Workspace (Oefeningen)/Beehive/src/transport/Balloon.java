package transport;

import interfaces.Flying;

public class Balloon extends Transport implements Flying {

	private String mandType;

	public Balloon(int speed, String mandType) {
		super(speed);
		this.mandType = mandType;
	}
	
	public Balloon() {
		super();
	}
	
	@Override
	public void fly() {
		System.out.println("balloon flies");
	}

	public String getMandType() {
		return mandType;
	}

	public void setMandType(String mandType) {
		this.mandType = mandType;
	}

	
		
	}
	
	

