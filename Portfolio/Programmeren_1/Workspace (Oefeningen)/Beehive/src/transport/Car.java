package transport;

import interfaces.Noisy;

public class Car extends Transport implements Noisy{

	private int pk;

	public Car(int speed, int pk) {
		super(speed);
		this.pk = pk;
	}
	
	public Car() {
		
	}
	public void makeNoise() {
		System.out.println("vroem vroem");
	}

	public int getPk() {
		return pk;
	}

	public void setPk(int pk) {
		this.pk = pk;
	}
	
	
}
