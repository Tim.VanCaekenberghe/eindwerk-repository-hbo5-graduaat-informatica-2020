package transport;

import interfaces.Flying;
import interfaces.Noisy;

public class Plane extends Transport implements Noisy, Flying {

	private int numberOfPassengers;

	
	public Plane(int numberOfPassengers) {
		super();
		this.numberOfPassengers = numberOfPassengers;
	}
	
	public Plane() {
		super();
	}
	
	@Override
	public void fly() {
		System.out.println("plane flies");
	}

	@Override
	public void makeNoise() {
		System.out.println("vlieg vlieg");
	}

	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}

	public void setNumberOfPassengers(int numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}

	
		
	}
	
	

