package transport;

public abstract class Transport {

	private int speed;

	public Transport(int speed) {
		super();
		this.speed = speed;
	}
	
	public Transport() {
		
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	
}
