
public class Calculator {
private Calculator(){}

	public static int add(int x, int y) {
		return x + y;
	}

	public static int sub(int x,int y) {
		return x - y;

	}

	public static int multi(int x, int y) {
		return x * y;
	}

	public static int devide(int x, int y) throws ArithmeticException {
		if (y != 0) {
			return Math.round((x / y));
		} else {
			throw new ArithmeticException();
		}
	}
	
}
