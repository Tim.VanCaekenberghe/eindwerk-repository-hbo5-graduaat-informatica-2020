package exceptions;

public class BallException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BallException(String name) {
		super(name);
	}

}
