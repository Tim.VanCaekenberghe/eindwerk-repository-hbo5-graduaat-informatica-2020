package exceptions;

public class DivideByZeroApp {
	//Can you divide by zero?? 
	
	public static void main(String[] args) {
		//catch me if you can
		System.out.println(divide(15,0));
	}
	
	public static double divide(int x, int y) {
		return x/y;
	}
}
