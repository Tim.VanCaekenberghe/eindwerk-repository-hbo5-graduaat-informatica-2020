package exceptions;

public class MyCrazyException extends Exception{
	
	public MyCrazyException(String message) {
		super(message);
	}

}
