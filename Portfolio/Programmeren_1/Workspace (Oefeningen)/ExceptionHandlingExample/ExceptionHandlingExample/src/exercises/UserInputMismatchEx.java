package exercises;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInputMismatchEx {

	private static Scanner scan;

	public static void main(String[] args) {
		// TODO 2.c vang de nodige exceptions of die door de methode startApp 
		//aangeduid worden als checked Exception
		startApp();
	}

	// TODO ex.1 Wanneer de gebruiker geen getal ingeeft zal er iets gebeuren
	// probeer de exception te behandelen en geef de gebruiker de
	// kans om opnieuw een getal in te geven.
	private static void startApp() {
		scan = new Scanner(System.in);
		System.out.println("Geef een getal tussen 0 en 10 ");
		//TODO ex.2a Maak een class NumberUserInputException (reeds gemaakt) aan die extends van Exception, 
		//zorg dan dat wanneer het getal niet tussen 0 en 10 ligt er een new NumberUserInputException gegooid
		//wordt.
		int i = scan.nextInt();
		System.out.println("het getal is " + i);
	}

}
