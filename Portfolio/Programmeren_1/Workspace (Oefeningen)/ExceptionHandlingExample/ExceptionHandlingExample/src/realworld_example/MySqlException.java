package realworld_example;

public class MySqlException extends Exception {
	
	private static final String MESSAGE = "****************************\n"
			+ "* 500 Something went wrong *\n"
			+ "****************************";
	
	public MySqlException() {
		super(MESSAGE);
	}

}
