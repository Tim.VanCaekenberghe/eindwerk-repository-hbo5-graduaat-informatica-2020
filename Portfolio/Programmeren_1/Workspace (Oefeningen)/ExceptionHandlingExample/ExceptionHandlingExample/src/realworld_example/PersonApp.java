package realworld_example;

import java.util.ArrayList;
import java.util.List;

public class PersonApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PersonRepository personRepository = new PersonRepository();
		PersonService personService = new PersonService(personRepository);
//		List<Person> persons = personService.findAll();
//		print(persons);
//		persons = personService.findPersonWhere(QueryHelper.createQueryWhereFirstNameLike("Steven"));
//		print(persons);
//		persons = personService.findPersonWhere(QueryHelper.createQueryWhereLastNameLike("De C"));
//		print(persons);
//		persons = personService.findPersonWhere(QueryHelper.createQueryWithError());
		
	}
	
	private static void print(List<Person>persons) {
		System.out.println("Print of list : ");
		persons.forEach(p -> System.out.println(p));
		System.out.println();
	}

}
