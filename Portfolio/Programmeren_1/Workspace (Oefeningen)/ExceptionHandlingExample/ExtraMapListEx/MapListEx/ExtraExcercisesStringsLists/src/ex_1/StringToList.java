package ex_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class StringToList {

	private static Scanner scanner;

	public static void main(String[] args) {
//String s = userRandomStringInput();
//		ArrayList<String> stringList = createListFromString(s);
//		print(stringList);
//		System.out.println();
//		print(sortAlphabetically(stringList));
//		print(randomizeAllWords(stringList));
	}
	
	
	//TODO 1.1 Create a string by asking the user for a sentence (more than one word)
	private static String userRandomStringInput() {
		System.out.println("Please enter a sentence");
		scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return "";
	}
	
	
	//TODO 1.2 Create a list for the string s, by separating the words using a space
	private static ArrayList<String> createListFromString(String s){
		ArrayList<String> strings = new ArrayList<String>();
		
		return new ArrayList<String>();
	}
	
	//TODO 1.3 Sort the string list alphabetically
	//https://docs.oracle.com/javase/8/docs/api/?java/util/Collections.html
	//See if you can use something that exists in the api
	private static ArrayList<String> sortAlphabetically(ArrayList<String> stringList){
		return stringList;
	}
	
	

	//TODO 1.4 a.For every word in the string list you need to scramble each word, letter by letter
	private static ArrayList<String> randomizeAllWords(ArrayList<String> stringList){
		ArrayList<String> result = new ArrayList<>();

		return result;
	}
	//TODO 1.4 b. Use this method to scramble a word, try to take it fully apart and rebuild the string using the output.
	private static String randomizeWord(String s) {
		List<Character> characters = new ArrayList<>();
		String output = "";
		return output.toLowerCase().trim();
	}
	
	
	//I will give this for free
	private static void print(ArrayList<String> stringList) {
		stringList.forEach(s -> System.out.print(s+" "));
	}
	
	


	

}
