package exceptions;

public class DivideByZeroApp {
	//Can you divide by zero?? 
	
	public static void main(String[] args) {
		//catch me if you can
		System.out.println(divide(15,1));
	}
	
	public static double divide(int x, int y) {
		double c = x/y;
		return c;
	}
}
