package realworld_example;

import java.util.List;

public class PersonService {

	private PersonRepository personRepository;

	public PersonService(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	public List<Person> findPersonWhere(String query) throws MySqlException {
		return this.personRepository.findAllPersonsWhere(query);
	}

	public List<Person> findAll() throws MySqlException {
		return this.personRepository.findAllPersonsWhere(QueryHelper.createQueryAll());
	}

	public Person findById(int id) throws MySqlException {
		try {
			return this.personRepository.findAllPersonsWhere(QueryHelper.createQueryFindById(id)).get(0);
		} catch (IndexOutOfBoundsException e) {
			// TODO: handle exception
			System.out.println("No person with id " + id);
			return null;
		}
	}
}
