package interfaces;

public interface Geometricobject {
	double getPerimeter();

	double getArea();

}
