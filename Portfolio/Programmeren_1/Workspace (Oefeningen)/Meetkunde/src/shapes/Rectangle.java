package shapes;

public class Rectangle extends Shape {
	protected double width;
	protected double length;

	public Rectangle() {
		super();
		width = 1.0;
		length = 1.0;
	}

	public Rectangle(double length, double width, String color, boolean filled) {
		super(color, filled);
		this.width = width;
		this.length = length;
	}

	public Rectangle(double length, double width) {
		super();
		this.width = width;
		this.length = length;
	}

	@Override
	public double getArea() {
		return this.length * this.width;
	}

	@Override
	public double getPerimeter() {
		return this.length * 2 + this.width * 2;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public String toString() {
		return "Rectangle with width= " + this.width + " and length= " + this.length + " , which is a subclass of:\n"
				+ super.toString();
	}

}
