package shapes;

import interfaces.Refactor;

public class ResizableRectangle extends Rectangle implements Refactor {

	public ResizableRectangle() {
		super();
	}

	public ResizableRectangle(double length, double width, String color, boolean filled) {
		super(length, width, color, filled);
	}

	public ResizableRectangle(double length, double width) {
		super(length, width);
	}

	@Override
	public void resize(int percent) {
		setLength(Math.abs(getLength() * percent * 0.01));
		setWidth(getWidth() * percent * 0.01);
		System.out.println("resized to " + percent + "% of the original size");

	}

}
