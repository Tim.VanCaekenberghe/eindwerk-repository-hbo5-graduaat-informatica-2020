package exercise_java_exceptions;

public class Excercise3 {
	
	/*
	 * Don't change the main class, use start application to perform the logic.
	 * a) Create the class NotCorrectCCNException, and make sure that this is
	 * a custom exception. Create a constructor where you call the super constructor
	 * with message "Not a credit card number"
	 * b) Ask the user for a credit card number, use the scanner and the method receiveACreditCardNumber(). 
	 * the length of the ccn is 12 digits (use isCorrectNumber) , make sure this is correct, else throw a NotCorrectCCNException.
	 * 	
	 */
	
	public static void main(String[] args) {
		startApplication();
	}
	
	public static boolean isCorrectNumber(String ccn) {
		return true;
	}
	
	
	public static void startApplication() {
		//receive a ccn
		
		//check if it is correct
		
		//if not, ask for a new one (keep doing this until it is correct)
	}
}
