package exercise_java_collections;

import java.util.ArrayList;
import java.util.HashMap;

import java_oop.Person;
import java_oop.Player;

public class Exercise4 {

	/*
	 * 	a)	In 2.java oop, you had to create a few classes. Now build a Team with a 11 players and a coach. Make sure that the id is incremental. 
	 * 		So for each player/coach created there is a unique id, Give them also a name, use the given class to create random first and last name. 
	 * 		Tip : use a loop for the players and add the coach as first element in the list. 
		b)	Use the method print to print out all the objects in the list. 
		c)	Now set the coach, fired on false and remove the coach from the team.
		d) 	Make a Map<String,ArrayList<Player>> where the key is the position of the player on the field and 
			find all the players for the same position and pass it as value.
 * 		e) Print the Map in printMap with first the position and than the values, use the tostring of player
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//create new team
		//use team.setPersons() to set the arraylist of persons
		//
	}
	
	public static void removeCoachFromTeam() {
		
	}
	
	public static ArrayList<Person> createPersonsForTeam(){
		return new ArrayList<>();
	}
	
	public static Player createPlayer(int id) {
		//use StringGenerator.getRandomFirstName, getRandomFirstName, getRandomPositionOnField
		return null;
	}
	
	public static void print(ArrayList<Person>persons) {
		
	}
	
	public static HashMap<String,ArrayList<Player>> findAllPlayersForPosition(){
		return new HashMap<>();
	}
	
	public static void printMap(HashMap<String,ArrayList<Player>> positionMap) {
		
	}

}
