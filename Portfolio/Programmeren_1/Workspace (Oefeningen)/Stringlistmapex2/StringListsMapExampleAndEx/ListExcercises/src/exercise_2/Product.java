package exercise_2;

public class Product {

	
	//TODO 2.1 create class Product
	//a Product has a name (String) and a price (double)
	private String name;
	private double price;

	//constructor with all properties
	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	//Constructor with no args
	}
	public Product() {
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + "]";
	}
	
	

	//Getters and Setters
	

	//override the toString, make it pretty
}
