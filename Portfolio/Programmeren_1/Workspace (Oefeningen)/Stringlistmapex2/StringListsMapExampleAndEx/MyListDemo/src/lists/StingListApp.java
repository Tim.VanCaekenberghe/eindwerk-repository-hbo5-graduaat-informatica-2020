package lists;

import java.util.ArrayList;

public class StingListApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> stringList = new ArrayList<>();
		//add a value
		String first = "Hello";
		stringList.add("Hello");
		for(int i = 0 ;i < 12; i++) {
			stringList.add("Hello "+i);
		}
		System.out.println("Loop 1 ");
		//print values
		for(String s : stringList) {
			System.out.println(s);
		}
		
		System.out.println("Loop 2");
		//indexed loop
		for(int i = 0 ; i < stringList.size() ; i++) {
			//get value by index
			System.out.println(stringList.get(i));
		}
		
		//sublist 
		ArrayList<String>sublist = new ArrayList<>(stringList.subList(1,5));
		System.out.println("Sublist");
		for(String i : sublist) {
			System.out.println(i);
		}
	
		//remove element by index
		stringList.remove(1);
		
		//remove element by reference
		stringList.remove(first);
		System.out.println("Loop 3");
		for(String s : stringList) {
			System.out.println(s);
		}
		//contains an object
		System.out.println(stringList.contains("Hello 11"));
		
		//clear the list
		stringList.clear();
		
		//size of list 
		System.out.println(stringList.size());
	}

}
