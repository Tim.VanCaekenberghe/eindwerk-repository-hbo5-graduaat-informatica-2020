package com.realdolmen;


import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestPersonRepository {

    private IDataSet iDataSet;

    @BeforeEach
    public void setUp() throws Exception {
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/realdolmen", "root", "");
        IDatabaseConnection iDatabaseConnection = new DatabaseConnection(connection);
        iDataSet = new FlatXmlDataSetBuilder().build(new File("src/test/java/com/realdolmen/mockdata.xml"));
        DatabaseOperation.CLEAN_INSERT.execute(iDatabaseConnection, iDataSet);
    }

    @Test
    void testDemo() throws DataSetException {
        System.out.println(iDataSet.getTable("people"));
    }
    @Test
    void findNormal() throws DataSetException {
        String person = iDataSet.getTable("people").getValue(1,"firstName").toString();
        Assertions.assertEquals("Jeanette", person);
    }
    @Test
    void findIDNonExistent() throws DataSetException {
        Assertions.assertThrows(RepositoryException.class, () -> {
            PersonRepository personRepository = new JdbcPersonRepository();
            Person person = personRepository.find(4);

            iDataSet.getTable("people").getValue(1, "firstName");
        });
    }


    @Test
    void iDMustBeNumber() {



    }
    @Test
    void findRows() throws DataSetException {
        String firstName = iDataSet.getTable("People").getValue(1, "firstName").toString();
        String lastName = iDataSet.getTable("People").getValue(1,"lastName").toString();
        String street = iDataSet.getTable("People").getValue(1,"street").toString();

        JdbcPersonRepository jdbcPersonRepository = new JdbcPersonRepository();
        Person person = jdbcPersonRepository.find(1);

        assertAll(
                () -> assertEquals(firstName, person.getFirstName()),
                () -> assertEquals(lastName, person.getLastName()),
                ()->assertEquals(street, person.getAddress().getStreet())
        );






    }

}
