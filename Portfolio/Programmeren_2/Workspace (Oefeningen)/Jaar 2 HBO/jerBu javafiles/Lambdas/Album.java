package com.realdolmen;

import java.util.List;

public class Album {
    private final String albumName;
    private final List<MusicTrack> tracks;
    private List<String> members;

    public Album(String albumName, List<MusicTrack> tracks, List<String> members) {
        this.albumName = albumName;
        this.tracks = tracks;
        this.members = members;
    }

    public String getAlbumName() {
        return albumName;
    }

    public List<MusicTrack> getTracks() {
        return tracks;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "Album{" +
                "albumName='" + albumName + '\'' +
                ", tracks=" + tracks +
                ", members=" + members +
                '}';
    }
}
