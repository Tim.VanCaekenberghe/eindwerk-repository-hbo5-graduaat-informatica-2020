package com.realdolmen;

public class MusicTrack {
    private String trackName;

    public MusicTrack(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    @Override
    public String toString() {
        return "MusicTrack{" +
                "trackName='" + trackName + '\'' +
                '}';
    }
}
