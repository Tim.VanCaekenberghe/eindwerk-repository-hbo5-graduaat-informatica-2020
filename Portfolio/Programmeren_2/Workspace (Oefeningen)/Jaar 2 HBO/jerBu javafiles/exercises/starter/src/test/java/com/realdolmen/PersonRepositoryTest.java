package com.realdolmen;


import com.sun.org.apache.xalan.internal.xsltc.dom.AdaptiveResultTreeImpl;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.*;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.*;

import javax.xml.crypto.Data;
import java.io.File;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PersonRepositoryTest {
    private Connection connection;
    private IDatabaseConnection iDatabaseConnection;
    private IDataSet iDataSet;
    private Date birthDate = new Date();
    private Person person = new Person("test", "test", birthDate, new Address("test", "test", new City("test", "test")));


    @BeforeEach
    public void setUp() throws SQLException, DatabaseUnitException, MalformedURLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/realdolmen", "root", "");
        iDatabaseConnection = new DatabaseConnection(connection);
        iDataSet = new FlatXmlDataSetBuilder().build(new File("src/test/java/com/realdolmen/mockData.xml"));
        DatabaseOperation.CLEAN_INSERT.execute(iDatabaseConnection, iDataSet);
    }

    @AfterEach
    public void powerDown() throws SQLException, DatabaseUnitException {
        DatabaseOperation.TRUNCATE_TABLE.execute(iDatabaseConnection, iDataSet);
        connection.close();
    }

    @Test
    public void emptyTest() {

    }

    @Test
    public void findNormalFieldsNotNull() {
        PersonRepository personRepository = new JdbcPersonRepository();
        Person person = personRepository.find(1);
        Assertions.assertAll(
                () -> {
                    assertNotNull("Adress may not be known", person.getAddress());
                    assertNotNull("city may not be known", person.getAddress().getCity());
                    assertNotNull("City name may not be known", person.getAddress().getCity().getName());
                    assertNotNull("City postal code not be known", person.getAddress().getCity().getPostalCode());
                    assertNotNull("house number may not be known", person.getAddress().getNumber());
                    assertNotNull("streetname may not be known", person.getAddress().getStreet());
                    assertNotNull("Birthdate may not be known", person.getBirthDate());
                    assertNotNull("First name may not be known", person.getFirstName());
                    assertNotNull("Lastname may not be known", person.getLastName());
                    assertEquals((Integer) 1, person.getId());
                }
        );


    }
//TODO change from asserNotNull to checking if they're equal to the pull from the datasheet
    @Test
    public void findNormalFieldsEqual() throws DataSetException {
        PersonRepository personRepository = new JdbcPersonRepository();

        Person person2 = getPersonObjectFromDb(iDataSet.getTable("people"),0);
        person2.setId(1);
        Person person = personRepository.find(1);
        Assertions.assertAll(
                () -> {
                    assertEquals(person2.getAddress().getStreet(),person.getAddress().getStreet());
                    assertEquals(person2.getBirthDate(), person.getBirthDate());
                    assertEquals(person2.getFirstName(), person.getFirstName());
                    assertEquals(person2.getId(), person.getId());
                    assertEquals(person2.getLastName(), person.getLastName());
                }
        );


    }

    @Test
    public void findWrongIdTooHigh() {
        PersonRepository personRepository = new JdbcPersonRepository();
        Assertions.assertThrows(RepositoryException.class, () -> {
            personRepository.find(456789);
        });
    }

    @Test
    public void findWrongIdNegative() {
        PersonRepository personRepository = new JdbcPersonRepository();
        Assertions.assertThrows(RepositoryException.class, () -> {
            personRepository.find(-1);
        });
    }

    @Test
    public void saveNormal() {
        PersonRepository personRepository = new JdbcPersonRepository();
        personRepository.save(person);
        Person fetchedPerson = personRepository.find(person.getId());
        Assertions.assertEquals(person, fetchedPerson);

    }

    @Test
    public void saveDuplicate() {
        PersonRepository personRepository = new JdbcPersonRepository();
        personRepository.save(person);
        Assertions.assertThrows(RepositoryException.class, () -> {
            personRepository.save(person);
        });
    }

    @Test
    public void saveWrongInfoNegativeID() {
        PersonRepository personRepository = new JdbcPersonRepository();
        person.setId(-1);
        Assertions.assertThrows(RepositoryException.class, () -> {
            personRepository.save(person);
        });
    }

    @Test
    public void saveWrongInfoNullID() {
        PersonRepository personRepository = new JdbcPersonRepository();
        person.setId(null);
        Assertions.assertThrows(RepositoryException.class, () -> {
            personRepository.save(person);
        });

    }

    @Test
    public void removeNormal() throws DataSetException {
        PersonRepository personRepository = new JdbcPersonRepository();
        person = getPersonObjectFromDb(iDataSet.getTable("people"),0);
        Assertions.assertDoesNotThrow(() -> {
            personRepository.remove(person);
        });


    }

    @Test
    public void removeNoID() throws DataSetException {
        PersonRepository personRepository = new JdbcPersonRepository();
        person = getPersonObjectFromDb(iDataSet.getTable("people"),0);
        person.setId(null);
        Assertions.assertThrows(RepositoryException.class, () -> {
            personRepository.save(person);
        });
    }

    @Test
    public void removeNegativeID() throws DataSetException {
        PersonRepository personRepository = new JdbcPersonRepository();
        person = getPersonObjectFromDb(iDataSet.getTable("people"),0);
        person.setId(-1);
        Assertions.assertThrows(RepositoryException.class, () -> {
            personRepository.save(person);
        });
    }

    private Person getPersonObjectFromDb(ITable table, int row) {
        try {
            String cityName = table.getValue(row, "city").toString();
            String postalCode = table.getValue(row, "postalcode").toString();
            String street = table.getValue(row, "street").toString();
            String number = table.getValue(row, "number").toString();
            String firstName = table.getValue(row, "firstName").toString();
            String lastName = table.getValue(row, "lastName").toString();
            Date birthDate = new SimpleDateFormat("yyyy-MM-dd").parse(table.getValue(row, "birthDate").toString());
            City city = new City(cityName, postalCode);
            Address address = new Address(street, number, city);
            return new Person(firstName, lastName, birthDate, address);
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }


    }

    @Test
    void saveCorrectData() throws SQLException, DataSetException {
        Date d = new Date(1998, 04, 22);
        City c = new City("Herzele", "9550");
        Address a = new Address("Kouterstraat", "74", c);
        Person p = new Person("Tim", "Van Caekenberghe", d, a);

        JdbcPersonRepository jdbcPersonRepository = new JdbcPersonRepository();
        jdbcPersonRepository.save(p);

        //Person personFromDB = jdbcPersonRepository.find(3);
        ITable people = iDatabaseConnection.createQueryTable("people","SELECT * from people where id ="+p.getId());

        Assertions.assertAll(
                ()-> assertEquals("Tim", getPersonObjectFromDb(people,0).getFirstName()),
                ()-> assertEquals("Van Caekenberghe", getPersonObjectFromDb(people, 0).getLastName())
        );

    }
}


