package com.realdolmen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class TestCalculator {
    @Test
    public void testSum() throws Exception {
        int sum = new Calculator((38), 1).addition();
        assertEquals(39, sum);
    }

    @Test
    public void testFlip() throws Exception {
        int sum = new Calculator(1, 38).addition();
    }

    @Test
    public void overflowExceptionHandlingAddition() {
        Assertions.assertThrows(Exception.class, () -> {
            new Calculator(Integer.MAX_VALUE, 1).addition();
        });

    }

    @Test
    public void negativeNumberAddition() throws Exception {
        int sum = new Calculator(-123, 123).addition();
        assertEquals(0, sum);
    }

    @Test
    public void divisionByZeroExceptionHandling() {
        Assertions.assertThrows(Exception.class, () -> {
            new Calculator(1, 0).division();
        });
    }

    @Test
    public void overflowExceptionHandlingPower() {
        Assertions.assertThrows(Exception.class, () -> {
            new Calculator(Integer.MAX_VALUE, Integer.MAX_VALUE).power();
        });
    }

    @Test
    public void powerOf2numbers() throws Exception{
        double pow = new Calculator(2, 3).power();
        assertEquals(8, (int)pow);
    }
    @Test
    public void negativePower() throws Exception{
        double pow = new Calculator(2,-1).power();
        assertEquals(0.5,pow);
    }
    @Test
    public void negativeNumberPower() throws Exception{
        double pow = new Calculator(-2,2).power();
        assertEquals(4,(int)pow);
    }
    @Test
    public void powerOfZero() throws Exception{
        double pow = new Calculator(1,0).power();
        assertEquals(1,(int)pow);
    }
    @Test
    public void zeroToPower() throws Exception{
        double pow = new Calculator(0,5).power();
        assertEquals(0,(int)pow);
    }

    @ParameterizedTest
    @ValueSource(ints = {0,1,2,3,4,5,6,7,8,9})
    public void parametrizedTestAddition(int arg1)throws Exception{
        double pow = new Calculator(arg1,2).power();
        assertTrue(pow>=0);
    }

}
	

