package com.realdolmen;

public class Calculator {
    private MathUtil mathUtil;

    public Calculator(MathUtil mathUtil) {
        this.mathUtil = mathUtil;
    }

    public int sumAndMultiply(int getal1, int getal2, int multiplyBy) {
        int sum = mathUtil.getSum(getal1, getal2);
        int muliply = mathUtil.multiply(sum, multiplyBy);

        return muliply + mathUtil.getSum(getal1, getal2); //bug
    }

    public void printPerson() {
        mathUtil.showPerson(new Person("Jimmi"));
    }


}

