1. This archive doesn't contain any IDE-specific files. 
2. When importing the code into your favorite IDE, please ensure that you instruct it to create the necessary packages.
3. Java 8 prior to release 45 has some bugs that affect Mockito. Please ensure that you use a version that is higher than this one.