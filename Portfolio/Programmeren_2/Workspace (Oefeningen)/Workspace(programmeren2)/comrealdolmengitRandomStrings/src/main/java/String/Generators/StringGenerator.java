package String.Generators;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class StringGenerator {
    private static Random r;
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static String[] firstNames = {"Maxime","Yoni","Jeroen","Tim"};
    private static String[] lastNames = {"Esnol", "Vindelinckx", "Buelens", "Van Caekenberghe"};
    private static final int ALPHABET_LENGTH = 52;
    private static final String NUMBERS = "0123456789";

//testforbranch if changes are made 
    public static int getRandomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public static String[] getFirstNames() {
        return firstNames;
    }

    public static String[] getLastNames() {
        return lastNames;
    }
    public static String getRandomFirstName() {
        return firstNames[getRandomInt(0, firstNames.length)];
    }
    public static String getRandomLastName() {
        return lastNames[getRandomInt(0,lastNames.length)];
    }



    public static char character() {
        int letterposition = StringGenerator.getRandomInt(0,ALPHABET_LENGTH);
        return ALPHABET.charAt(letterposition);
    }
    public static String name() {
        return firstNames[getRandomInt(0,firstNames.length) - 1] +
                " " +
                lastNames[getRandomInt(0,lastNames.length) - 1];

    }
    public static String email(String firstName, String lastName){
        return firstName + "." + lastName + "@realdolmen.com";
    }
    public static String password(int length){
        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < length; i++){
            if(getRandomInt(0,2) <= 1){
                builder.append(NUMBERS.charAt(getRandomInt(0,10)));
            } else {
                builder.append(ALPHABET.charAt(getRandomInt(0,ALPHABET_LENGTH)));
            }
        }

        return builder.toString();
    }
    static void something() {
        System.out.println("something");
    }
}



