package string.generators;

import String.Generators.StringGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class GeneratorTest {

    @Test
    public void testThrowsExceptionMaxSmallerThanMin(){
        assertThrows(IllegalArgumentException.class, () -> {
            StringGenerator.getRandomInt(0,-10);
        });
    }
    @Test
    public void testThrowsExceptionMaxEqualsToMin(){
        assertThrows(IllegalArgumentException.class, () -> {
            StringGenerator.getRandomInt(0,0);
        });
    }

    @Test
    public void testNumbersBetweenLimits(){
        int x = StringGenerator.getRandomInt(0,3);

        assertTrue(x >= 0 && x<=4);
    }

    @Test
    public void testFirstNameAppearsInArray(){
        String x = StringGenerator.getRandomFirstName();
        String test = Arrays.stream(StringGenerator.getFirstNames()).filter(n -> n.equals(x)).findFirst().get();
        assertEquals(x, test);
    }

    @Test
    public void testLastNameAppearsInArray(){
        String x = StringGenerator.getRandomLastName();
        String test = Arrays.stream(StringGenerator.getLastNames()).filter(n -> n.equals(x)).findFirst().get();
        assertEquals(x, test);
    }

    @Test
    public void testFirstNameIsNotNullOrEmpty(){
        String x = StringGenerator.getRandomFirstName();
        assertFalse(x == null || x.isEmpty());
    }

    @Test
    public void testLastNameIsNotNullOrEmpty(){
        String x = StringGenerator.getRandomLastName();
        assertFalse(x == null || x.isEmpty());
    }

    @Test
    public void testPasswordLengthIsGivenLength(){
        String password = StringGenerator.password(5);
        assertEquals(5, password.length());
    }

    @Test
    public void testEmailIsNameLastNameSuffix() throws EmailMissingNameException {
        String email = StringGenerator.email("maxime", "esnol");
        assertEquals("maxime.esnol@realdolmen.com", email);
    }


}

