package application;


import domain.Borrow;
import domain.Borrower;
import domain.Category;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import services.*;

import java.util.*;

public class Game {

    private Map<Integer, String> menuMap;

    private Scanner scanner;

    private CategoryService categoryService;
    private GameService gameService;
    private DifficultyService difficultyService;
    private BorrowService borrowService;
    private BorrowerService borrowerService;

    private boolean continueGame = true;


    public Game(CategoryService categoryService, GameService gameService, DifficultyService difficultyService, BorrowService borrowService, BorrowerService borrowerService) {
        this.scanner = new Scanner(System.in);

        this.categoryService = categoryService;
        this.gameService = gameService;
        this.difficultyService = difficultyService;
        this.borrowService = borrowService;
        this.borrowerService = borrowerService;

        this.menuMap = new HashMap<>();

        menuMap.put(0, "quit game");
        menuMap.put(1, "Show the first game category");
        menuMap.put(2, "Show the fifth game");
        menuMap.put(3, "Show the first borrower");
        menuMap.put(4, "Show a game of your choice");
        menuMap.put(5, "Show all games");
        menuMap.put(6, "Show a list of games and choose a game ");
        menuMap.put(7, "Show all borrowed games");
        menuMap.put(8, "Advanced search: difficulty ");
        menuMap.put(9, " Complex search: borrowers’");
    }

    public void startGame() {
        System.out.println("**********GAME DB**********");
        System.out.println("*********WELCOME TIM*********");
        System.out.println("-Game options-");
        showMenu();
    }

    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        chooseOption();

    }

    private void chooseOption() {
        ;
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        try {
            System.out.println("Choose your option : ");
            int i = scanner.nextInt();

            scanner.nextLine();
            executeOption(i);
        } catch (InputMismatchException e) {
            System.err.println("Not valid, please enter a number");
            scanner = null;
            chooseOption();
        }


    }

    private void executeOption(int option) {
        switch (option) {
            case 1:
                findCategory(option);
                break;

            case 2:
                findGame(5);
                break;

            case 3:
                findBorrowerNameAndCityByID();
                break;

            case 4:
                showGameOfYourChoiceShort();
                break;

            case 5:
                System.out.println("Loading all games...");
                showAllGames();
                break;

            case 6:
                System.out.println("Loading all games...");
                showShortAllGames();
                showAgameOfYourChoiceLong();
                break;

            case 7:
                System.out.println("Loading all borrowed games...");
                showAllBorrows();
                String name = showAllBorrowersAskName();
                showBorrowsBy(name);
                break;

            case 8:
                int difficulty = askForDifficulty();
                showGamesFromMinDifficulty(difficulty);
                break;


            case 9:
                String borrowerMatch = askForBorrowerNameMatch();
                showBorrowersByNameStartingWith(borrowerMatch);
                break;


            case 0:
                closeGame();
                continueGame = false;
                return;
        }
        if (continueGame) {
            showMenu();

        }
    }

    private void showBorrowersByNameStartingWith(String borrower) {
        System.out.println("Borrowers whose name stars with \"" + borrower + "\":");
        try {
            List<Borrower> borrowers = borrowerService.findBorrowersByNameStartingWith(borrower);

            for (Borrower b : borrowers) {
                System.out.println("----- BORROWER INFORMATION -----");
                System.out.println("NAME:\t" + b.getName());
                System.out.println("CITY:\t" + b.getCity());
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
    }

    private String askForBorrowerNameMatch() {
        System.out.print("\nEnter the name or part of the name:");
        if (scanner.hasNextLine()) {
            return scanner.nextLine();
        } else {
            askForBorrowerNameMatch();
        }
        return null;
    }


    private void showAllBorrows() {
        System.out.println("Game Name\t\tBorrowed by\t\tBorrow date\t\tReturn date");
        try {
            List<Borrow> borrows = borrowService.findAllBorrows();

            for (Borrow b : borrows) {
                String returnDate;
                if (b.getReturnDate() == null) {
                    returnDate = "Not returned";
                } else {
                    returnDate = b.getReturnDate().toString();
                }
                System.out.println(b.getGame().getName() + " - " + b.getBorrower().getName() + " - " + b.getBorrowDate() + " - " + returnDate);
            }

        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }

    }


    private void showShortAllGames() {
        try {
            List<domain.Game> games = gameService.findAllgames();
            System.out.println("ID\t\tGame name/Category");
            for (domain.Game game : games) {
                System.out.println("*" + game.getName() + ", " + game.getCategory().getCategoryName().toUpperCase());
            }
        } catch (NoRecordFoundException e) {
            e.getMessage();
        }

    }

    private void showBorrowsBy(String name) {
        System.out.println("Loading borrows from " + name.toUpperCase() + "... Please wait...");

        try {
            List<Borrow> borrows = borrowService.findAllBorrowsFromBorrower(name);
            System.out.println("Game Name\t\tBorrowed by\t\tBorrow date\t\tReturn date");

            for (Borrow b : borrows) {
                String returnDate;
                if (b.getReturnDate() == null) {
                    returnDate = "Not yet returned";
                } else {
                    returnDate = b.getReturnDate().toString();
                }
                System.out.println(b.getGame().getName() + " - " + b.getBorrower().getName() + " - " + b.getBorrowDate() + " - " + returnDate);
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
    }


    private String askNameToSearch() {
        System.out.println("Please enter a name to search or press enter to quit");
        if (scanner.hasNextLine()) {
            return scanner.nextLine();
        } else {
            System.err.println("Not a valid name");
            askNameToSearch();
        }
        return null;
    }

    private String showAllBorrowersAskName() {
        try {
            List<Borrower> borrowers = borrowerService.findAllBorrowers();

            System.out.println(" Borrowers ");
            for (Borrower b : borrowers) {
                System.out.println(b.getName());
            }

            String name = askNameToSearch();
            return name;
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        return null;

    }

//    private void showAllGamesNameCategory() {
//        try {
//            System.out.println("Loading...");
//            List<domain.Game> games = gameService.findAllgames();
//            for (domain.Game game : games) {
//                System.out.println(game.getCategory() + game.getName());
//            }
//        } catch (NoRecordFoundException e) {
//            e.getMessage();
//        }
//    }

    private void showAllGames() {
        try {

            List<domain.Game> games = gameService.findAllgames();
            System.out.println("-All Games-");
            for (domain.Game game : games) {
                System.out.println("Name = " + game.getName());
                System.out.println("Editor = " + game.getEditor());
                System.out.println("Price = " + game.getPrice());
                System.out.println("-----------------------------");
            }
        } catch (NoRecordFoundException e) {
            e.getMessage();
        }
    }

    private void showGameOfYourChoiceShort() {
        System.out.println("Search: ");
        String match = scanner.nextLine();
        shortFindGameStartingWith(match);
    }

    private void showAgameOfYourChoiceLong() {
        System.out.println("Enter part of game you want to search:");
        String match = scanner.nextLine();
        longFindGameStartingWith(match);
    }

    private void shortFindGameStartingWith(String match) {
        try {
            domain.Game game = gameService.findGameStartingWith(match);
            System.out.println("*********GAME INFORMATION*********");
            System.out.println("\n GameName: " + game.getName());
            System.out.println("\n Publisher: " + game.getAuthor());
            System.out.println("\n Age: " + game.getAge());
            System.out.println("\n Price: " + game.getPrice());
            System.out.println("\n *********choose another option or quit the game********* ");
        } catch (NoRecordFoundException e) {
            e.getMessage();
        }
    }


    private void longFindGameStartingWith(String match) {
        try {
            domain.Game game = gameService.findGameStartingWith(match);
            System.out.println("*********GAME INFORMATION*********");
            System.out.println("\n GameName: " + game.getName());
            System.out.println("\n Publisher: " + game.getAuthor());
            System.out.println("\n Age: " + game.getAge());
            System.out.println("\n Price: " + game.getPrice());
            System.out.println("\n CategoryName: " + game.getCategory());
            System.out.println("\n Difficulty: " + game.getDifficulty());
            System.out.println("\n *********choose another option or quit the game********* ");
        } catch (NoRecordFoundException e) {
            e.getMessage();
        }
    }

    private void findBorrowerNameAndCityByID() {
        try {
            Borrower borrower = borrowerService.findBorrowerById(1);
            String name = borrower.getName();
            String city = borrower.getCity();

            System.out.println("******iNFORMATION FIRST BORROWER*******");
            System.out.println("Name: " + name + "\n City: " + city);

        } catch (IDOutOfBoundException e) {
            e.getMessage();
        } catch (NoRecordFoundException e) {
            e.getMessage();
        }
    }

    private void findGame(int gameId) {
        try {
            System.out.println("Showing game with id" + gameId);
            domain.Game game = gameService.findGameByID(gameId);
            System.out.println("********GAME INFORMATION********");
            System.out.println("NAME:\t \t" + game.getName());
            System.out.println("PUBLISHER:\t" + game.getEditor());
            System.out.println("AGE:\t\t" + game.getAge());
            System.out.println("PRICE\t\t" + game.getPrice());
            System.out.println("********************************");

        } catch (IDOutOfBoundException e) {
            e.printStackTrace();

        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }

    }

    private void closeGame() {
        System.out.println("Closing the game.");
    }

    private void findCategory(int option) {
        try {
            Category category = categoryService.findCategoryById(option);
            System.out.println(category);
        } catch (NoRecordFoundException e) {
            e.getMessage();
            startGame();
        }
    }

    private void showGamesFromMinDifficulty(int difficulty) {
        System.out.println("--- Games -----");
        try {
            List<domain.Game> games = gameService.findGameByMinimumDifficulty(difficulty);

            for (domain.Game game : games) {
                System.out.println("-----------------------------");
                System.out.println("ID:\t\t" + game.getId());
                System.out.println("NAME:\t\t" + game.getName());
                System.out.println("PUBLISHER:\t" + game.getEditor());
                System.out.println("PRICE:\t\t" + game.getPrice());
                System.out.println("-----------------------------");
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
    }

    private int askForDifficulty() {
        System.out.println("----- Search by Difficulty -----");
        System.out.print("\nEnter the minimum difficulty you want for the games: (1 to 5)");
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        } else {
            askForDifficulty();
        }

        return 0;
    }


}
