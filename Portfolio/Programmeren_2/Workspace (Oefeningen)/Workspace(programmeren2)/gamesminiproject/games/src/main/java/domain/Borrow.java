package domain;

import java.util.Date;

/**
 * This is the borrow domain, includes empty constructor, constructor with parameters, to string method to return a borrow and getters&setters.
 **/

public class Borrow {

    private int id;
    private Game game;
    private Borrower borrower;
    private Date borrowDate;
    private Date returnDate;

    public Borrow() {

    }

    public Borrow(int id, Game game, Borrower borrower, Date borrowDate, Date returnDate) {
        this.id = id;
        this.game = game;
        this.borrower = borrower;
        this.borrowDate = borrowDate;
        this.returnDate = returnDate;
    }

    @Override
    public String toString() {
        return "Borrow{" +
                "id=" + id +
                ", Game" + game +
                ", borrower=" + borrower +
                ", borrowDate=" + borrowDate +
                ", returnDate=" + returnDate +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Borrower getBorrower() {
        return borrower;
    }

    public void setBorrower(Borrower borrower) {
        this.borrower = borrower;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }
}
