package domain;

/**
 * This is the category domain, includes empty constructor, constructor with parameters, to string method to return a category and getters&setters.
 **/


public class Category {

    private int id;
    private String categoryName;

    public Category() {

    }

    public Category(int id, String categoryname) {
        this.id = id;
        this.categoryName = categoryname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "Category ID = " + id +
                " CategoryName= " + categoryName;
    }
}
