package domain;

public class Difficulty {
    /**
     * This is the difficulty domain, includes empty constructor, constructor with parameters, to string method to return the difficulty and getters&setters.
     **/


    private int id;
    private String difficultyName;


    public Difficulty(int id, String difficultyName) {
        this.id = id;
        this.difficultyName = difficultyName;
    }

    public Difficulty() {
    }

    @Override
    public String toString() {
        return "Difficulty{" +
                "id=" + id +
                ", difficultyName='" + difficultyName + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDifficultyName() {
        return difficultyName;
    }

    public void setDifficultyName(String difficultyName) {
        this.difficultyName = difficultyName;
    }
}

