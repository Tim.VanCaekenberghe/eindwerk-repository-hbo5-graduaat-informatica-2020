package exceptions;

public class IDOutOfBoundException extends Exception {

    public IDOutOfBoundException() {
        super("The given ID is too small or too large.");
    }

    public String getMessage() {
        String message = "ID is out of bound";
        System.out.println(message);
        return null;
    }
}
