package exceptions;

public class NoRecordFoundException extends Exception {

    private String column;

    public NoRecordFoundException() {
        super("could not found any records");


    }

    public String getMessage() {
        String message = "sorry please try again, we could not find any records matching your request";
        System.out.println(message);
        return null;
    }


}
