package repo;

import domain.Borrow;
import domain.Borrower;
import domain.Game;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import services.BorrowerService;
import services.GameService;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class BorrowDAO {

    private Connection connection;

    public BorrowDAO() {
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * constructor with the connection, so we could give the connection as a parameter
     * @param connection to have acces on the db
     */
    protected BorrowDAO(Connection connection) {
        this.connection = connection;
    }

//*Method to make connection to the DB*//

    /**
     * method to find all borrows in the db, with a query
     */
    public List<Borrow> findAllBorrows() {
        List<Borrow> borrows = new ArrayList<>();

        try {

            String query = "SELECT " +
                    "b.id AS borrowId " +
                    ",borrow_date " +
                    ",return_date " +
                    ",borrower_name" +
                    ",game_id " +
                    ",borrower_id " +
                    "FROM borrow b " +
                    "LEFT JOIN borrower br ON br.id = b.borrower_id " +
                    "ORDER BY borrower_name, borrow_date";
            PreparedStatement s = connection.prepareStatement(query);
            s.execute();

            ResultSet rs = s.getResultSet();

            while (rs.next()) {
                Borrow borrow = new Borrow();
                borrow = createObjectFromSQL(borrow, rs);
                borrows.add(borrow);
            }
            s.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return borrows;
    }


    public List<Borrow> findAllBorrowsByBorrower(String borrowerName) {
        List<Borrow> borrows = new ArrayList<>();

        try {

            String query = "SELECT " +
                    "b.id AS borrowId " +
                    ",borrow_date " +
                    ",return_date " +
                    ",game_id " +
                    ",borrower_id " +
                    "FROM borrow b " +
                    "LEFT JOIN borrower br ON br.id = b.borrower_id " +
                    "WHERE borrower_name = ?" +
                    "ORDER BY borrow_date";
            PreparedStatement s = connection.prepareStatement(query);
            s.setString(1, borrowerName);
            s.execute();

            ResultSet rs = s.getResultSet();
            while (rs.next()) {
                Borrow borrow = new Borrow();
                borrow = createObjectFromSQL(borrow, rs);
                borrows.add(borrow);
            }

            s.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return borrows;
    }
    //* method to find all the borrows on the basis borrower*//

    private Borrow createObjectFromSQL(Borrow borrow, ResultSet rs) throws SQLException {
        borrow.setId(rs.getInt("borrowId"));
        borrow.setBorrowDate(rs.getDate("borrow_date"));
        borrow.setReturnDate(rs.getDate("return_date"));

        if (rs.getInt("game_id") == 0) {
            borrow.setGame(null);
        } else {
            GameDAO gameDAO = new GameDAO();
            GameService gameService = new GameService(gameDAO);
            try {
                Game game = gameService.findGameByID(rs.getInt("game_id"));
                borrow.setGame(game);
            } catch (IDOutOfBoundException e) {
                e.printStackTrace();
            } catch (NoRecordFoundException e) {
                e.printStackTrace();
            }
        }


        if (rs.getInt("borrower_id") == 0) {
            borrow.setBorrower(null);
        } else {
            BorrowerDAO borrowerDAO = new BorrowerDAO();
            BorrowerService borrowerService = new BorrowerService(borrowerDAO);

            try {
                Borrower borrower = borrowerService.findBorrowerById(rs.getInt("borrower_id"));
                borrow.setBorrower(borrower);
            } catch (IDOutOfBoundException e) {
                e.printStackTrace();
            } catch (NoRecordFoundException e) {
                e.printStackTrace();
            }
        }

        return borrow;
    }
}
//*method to set the data of the db in an object*//






