package repo;

import domain.Borrower;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BorrowerDAO {
    private Connection connection;

    public BorrowerDAO() {
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected BorrowerDAO(Connection connection) {
        this.connection = connection;
    }


    public Borrower findBorrowerById(int id) {
        Borrower borrower = null;
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");

            PreparedStatement s = con.prepareStatement("SELECT * FROM borrower WHERE id = ?");
            s.setInt(1, id);
            s.execute();
            ResultSet rs = s.getResultSet();

            if (rs.next()) {
                borrower = createObjectFromSQL(borrower, rs);
            }

            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrower;
    }

    //*Method to find borrower on the basic of ID*//
    public List<Borrower> findBorrowersStartingWith(String match) {
        List<Borrower> borrowers = new ArrayList<>();

        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");

            PreparedStatement s = con.prepareStatement("SELECT * FROM borrower WHERE borrower_name LIKE ? ORDER BY borrower_name");
            s.setString(1, match + "%");
            s.execute();
            ResultSet rs = s.getResultSet();

            while (rs.next()) {
                Borrower borrower = new Borrower();
                borrower = createObjectFromSQL(borrower, rs);
                borrowers.add(borrower);
            }
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrowers;
    }

    public List<Borrower> findAllBorrowers() {
        List<Borrower> borrowers = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");

            PreparedStatement s = con.prepareStatement("SELECT * FROM borrower ORDER BY borrower_name");
            s.execute();
            ResultSet rs = s.getResultSet();

            while (rs.next()) {
                Borrower borrower = new Borrower();
                borrower = createObjectFromSQL(borrower, rs);
                borrowers.add(borrower);
            }

            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return borrowers;
    }

    private Borrower createObjectFromSQL(Borrower borrower, ResultSet rs) {
        borrower = new Borrower();
        try {
            borrower.setId(rs.getInt("id"));
            borrower.setName(rs.getString("borrower_name"));
            borrower.setBusNumber(rs.getString("bus_number"));
            borrower.setCity(rs.getString("city"));
            borrower.setStreet(rs.getString("street"));
            borrower.setHouseNumber(rs.getString("house_number"));
            borrower.setPostcode(rs.getInt("postcode"));
            borrower.setBusNumber(rs.getString("bus_number"));
            borrower.setTelephoneNumber(rs.getInt("telephone"));
            borrower.setEmail(rs.getString("email"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrower;
    }

}


