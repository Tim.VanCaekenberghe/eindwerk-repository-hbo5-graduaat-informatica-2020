package repo;


import domain.Category;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDAO {
    private Connection connection;

    public CategoryDAO() {
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected CategoryDAO(Connection con) {
        this.connection = con;
    }


    public Category findCategoryById(int id) {
        Category category = null;
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM category WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet set = preparedStatement.getResultSet();
            if (set.next()) {
                category = new Category();
                category.setId(set.getInt("id"));
                category.setCategoryName(set.getString("category_name"));
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return category;
    }

    public List<Category> findCategoriesWhereNameStartsWith(String match) {
        List<Category> matches = new ArrayList<>();
        try {
            Connection con = getConnection();
            PreparedStatement s = con.prepareStatement("SELECT * FROM category WHERE category_name LIKE ?");
            s.setString(1, match + "%");
            s.execute();
            ResultSet rs = s.getResultSet();
            while (rs.next()) {

                Category c = new Category();
                c.setId(rs.getInt("id"));
                c.setCategoryName(rs.getString("category_name"));
                matches.add(c);
            }

            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return matches;
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
    }

}
