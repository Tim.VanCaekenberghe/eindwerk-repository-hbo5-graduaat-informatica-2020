package repo;

import domain.Difficulty;
import utilities.DataBaseUtil;

import java.sql.*;

public class DifficultyDAO {

    public Difficulty difficultyByID(int id) {
        Difficulty difficulty = new Difficulty();
        try {
            Connection connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM difficulty where id =?");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            if (rs.next()) {
                difficulty = new Difficulty();
                difficulty.setId(rs.getInt("id"));
                difficulty.setDifficultyName(rs.getString("difficulty_name"));
            }
            preparedStatement.close();
            connection.close();
            return difficulty;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return difficulty;
    }
}
