package repo;

import domain.Category;
import domain.Game;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import services.CategoryService;
import services.DifficultyService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static utilities.DataBaseUtil.URL;
import static utilities.DataBaseUtil.USERNAME;

public class GameDAO {
    private Connection connection;

    public GameDAO() {
        try {
            this.connection = DriverManager.getConnection(URL, USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected GameDAO(Connection con) {
        this.connection = con;
    }


    public Game findGameByID(int id) {
        Game game = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM game WHERE id = ?");

            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();

            if (rs.next()) {
                game = this.createObjectFromSQL(game, rs);

                CategoryDAO categoryDAO = new CategoryDAO();
                CategoryService categoryService = new CategoryService(categoryDAO);
                Category c = categoryService.findCategoryById(rs.getInt("category_id"));

                game.setCategory(c);
                game.setDuration(rs.getString("play_duration"));

                DifficultyDAO difficultyDAO = new DifficultyDAO();
                DifficultyService difficultyService = new DifficultyService(difficultyDAO);
                game.setDifficulty(difficultyService.findDifficultyById(rs.getInt("difficulty_id")));

                game.setPrice(rs.getFloat("price"));
                game.setImage(rs.getString("image"));


            }
            connection.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.getMessage();
        } catch (NoRecordFoundException | IDOutOfBoundException e) {
            e.getMessage();
        }
        return game;
    }

    public Game findGameStartingWith(String match) {
        Game game = null;
        try {

            PreparedStatement s = connection.prepareStatement("SELECT * FROM game WHERE game_name LIKE ? ORDER BY game_name ASC LIMIT 1");
            s.setString(1, match + "%");
            s.execute();

            ResultSet rs = s.getResultSet();
            if (rs.next()) {
                game = this.createObjectFromSQL(game, rs);
            }
            s.close();
            connection.close();
        } catch (SQLException e) {
            e.getMessage();
        }
        return game;
    }

    public List<Game> findAllGames() {
        List<Game> games = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(URL, USERNAME, "");

            PreparedStatement s = con.prepareStatement("SELECT * FROM game ORDER BY game_name ASC");
            s.execute();

            ResultSet rs = s.getResultSet();
            while (rs.next()) {
                Game game = new Game();
                game = this.createObjectFromSQL(game, rs);

                games.add(game);
            }
            s.close();
            con.close();
            return games;
        } catch (SQLException e) {
            e.getMessage();
        }
        return games;
    }

    /**
     * Creates a new Game object based on a ResultSet.
     *
     * @param game the Game object that has to be created
     * @param rs   the ResultSet of the SQL query
     * @return a Game object created based on the SQL query
     */
    private Game createObjectFromSQL(Game game, ResultSet rs) {
        try {
            game = new domain.Game();
            game.setId(rs.getInt("id"));
            game.setName(rs.getString("game_name"));
            game.setEditor(rs.getString("editor"));
            game.setAuthor(rs.getString("author"));
            game.setYear(rs.getInt("year_edition"));
            game.setAge(rs.getString("age"));
            game.setMinimumPlayers(rs.getInt("min_players"));
            game.setMaximumPlayers(rs.getInt("max_players"));


            if (rs.getInt("category_id") == 0) {
                game.setCategory(null);
            } else {
                CategoryDAO categoryDAO = new CategoryDAO();
                CategoryService categoryService = new CategoryService(categoryDAO);
                Category c = categoryService.findCategoryById(rs.getInt("category_id"));
                game.setCategory(c);
            }


            game.setDuration(rs.getString("play_duration"));

            if (rs.getInt("difficulty_id") == 0) {
                game.setDifficulty(null);
            } else {
                DifficultyDAO difficultyDAO = new DifficultyDAO();
                DifficultyService difficultyService = new DifficultyService(difficultyDAO);
                game.setDifficulty(difficultyService.findDifficultyById(rs.getInt("difficulty_id")));

            }
            game.setPrice(rs.getFloat("price"));
            game.setImage(rs.getString("image"));
        } catch (IDOutOfBoundException e) {
            e.getMessage();
        } catch (SQLException e) {
            e.getMessage();
        } catch (NoRecordFoundException e) {
            e.getMessage();
        }
        return game;
    }

    public List<Game> findGameByMinimumDifficulty(int difficulty) {
        List<Game> games = new ArrayList<>();

        try {


            String query = "SELECT * FROM game g" +
                    "       WHERE difficulty_id >= ?" +
                    "       ORDER BY game_name ASC, difficulty_id ASC";
            PreparedStatement s = connection.prepareStatement(query);
            s.setInt(1, difficulty);
            s.execute();

            ResultSet rs = s.getResultSet();
            while (rs.next()) {
                Game game = new Game();
                game = createObjectFromSQL(game, rs);
                games.add(game);
            }
            s.close();
            connection.close();
            return games;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return games;
    }

}


