package services;

import domain.Borrow;
import exceptions.NoRecordFoundException;
import repo.BorrowDAO;

import java.util.List;


public class BorrowService {

    private BorrowDAO borrowDAO;

    public BorrowService(BorrowDAO borrowdao) {
        this.borrowDAO = borrowdao;
    }

    public List<Borrow> findAllBorrows() throws NoRecordFoundException {
        List<Borrow> borrows = borrowDAO.findAllBorrows();
        if (borrows.isEmpty() || (borrows == null)) {
            throw new NoRecordFoundException();
        } else
            return borrows;
    }

    public List<Borrow> findAllBorrowsFromBorrower(String name) throws NoRecordFoundException {
        if (name.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            List<Borrow> borrows = borrowDAO.findAllBorrowsByBorrower(name);
            if (borrows == null) {
                throw new NoRecordFoundException();
            }
            return borrows;
        }
    }


}

