package services;

import domain.Borrower;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.BorrowerDAO;

import java.util.ArrayList;
import java.util.List;

public class BorrowerService {
    private BorrowerDAO dao;

    public BorrowerService(BorrowerDAO dao) {
        this.dao = dao;
    }

    public List<Borrower> findAllBorrowers() throws NoRecordFoundException {
        List<Borrower> borrowers = dao.findAllBorrowers();

        if (borrowers.isEmpty()) {
            throw new NoRecordFoundException();
        } else {
            return borrowers;
        }
    }

    public List<Borrower> findBorrowersByNameStartingWith(String match) throws NoRecordFoundException {
        List<Borrower> borrowers = new ArrayList<>();
        if (match.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            borrowers = dao.findBorrowersStartingWith(match);
            if (borrowers.isEmpty()) {
                throw new NoRecordFoundException();
            } else {
                return borrowers;
            }
        }
    }


    public Borrower findBorrowerById(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if (id <= 0) {
            throw new IDOutOfBoundException();
        } else {
            Borrower borrower = dao.findBorrowerById(id);

            if (borrower == null) {
                throw new NoRecordFoundException();
            } else {
                return borrower;
            }
        }
    }
}


