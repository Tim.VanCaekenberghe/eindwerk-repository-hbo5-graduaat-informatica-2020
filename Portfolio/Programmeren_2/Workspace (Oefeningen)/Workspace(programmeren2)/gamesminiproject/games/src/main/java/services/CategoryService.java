package services;

import domain.Category;
import exceptions.NoRecordFoundException;
import repo.CategoryDAO;

import java.util.List;

public class CategoryService {

    private CategoryDAO categoryDAO;

    public CategoryService(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    public Category findCategoryById(int id) throws NoRecordFoundException {
        Category category = categoryDAO.findCategoryById(id);
        if (category == null) {
            throw new NoRecordFoundException();
        } else {
            return category;
        }
    }

    public List<Category> findCategoryWhereNameStartsWith(String match) throws NoRecordFoundException {
        List<Category> matches = categoryDAO.findCategoriesWhereNameStartsWith(match);
        if (matches.isEmpty() || matches == null) {
            throw new NoRecordFoundException();
        } else {
            return matches;
        }
    }


}


