package services;

import domain.Difficulty;
import exceptions.IDOutOfBoundException;
import repo.DifficultyDAO;

public class DifficultyService {
    private DifficultyDAO difficultyDAO;

    public DifficultyService(DifficultyDAO difficultyDAO) {
        this.difficultyDAO = difficultyDAO;
    }

    public Difficulty findDifficultyById(int id) throws IDOutOfBoundException {
        Difficulty difficulty = difficultyDAO.difficultyByID(id);
        if (id <= 0) {
            throw new IDOutOfBoundException();
        }
        return difficulty;
    }

}
