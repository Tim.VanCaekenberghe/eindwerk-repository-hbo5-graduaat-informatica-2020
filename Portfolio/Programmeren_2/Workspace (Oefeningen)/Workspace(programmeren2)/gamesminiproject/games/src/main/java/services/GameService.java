package services;

import domain.Game;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.GameDAO;

import java.util.List;


public class GameService {

    private GameDAO gameDao;

    public GameService(GameDAO gameDao) {
        this.gameDao = gameDao;
    }

    public Game findGameByID(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if (id <= 0) {
            throw new IDOutOfBoundException();
        } else {
            Game game = gameDao.findGameByID(id);
            if (game == null) {
                throw new NoRecordFoundException();
            } else {
                return game;
            }
        }
    }

    public Game findGameStartingWith(String match) throws NoRecordFoundException {
        if (match.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            Game game = gameDao.findGameStartingWith(match);
            if (game == null) {
                throw new NoRecordFoundException();
            } else
                return game;
        }

    }

    public List<Game> findAllgames() throws NoRecordFoundException {
        List<Game> games = gameDao.findAllGames();

        if (games.isEmpty()) {
            throw new NoRecordFoundException();
        } else
            return games;
    }

    public List<Game> findGameByMinimumDifficulty(int difficulty) throws NoRecordFoundException {
        if (difficulty <= 0 && difficulty > 5) {
            throw new IllegalArgumentException();
        } else {
            List<Game> games = gameDao.findGameByMinimumDifficulty(difficulty);
            if (games.isEmpty()) {
                throw new NoRecordFoundException();
            } else {
                return games;
            }
        }
    }

}
