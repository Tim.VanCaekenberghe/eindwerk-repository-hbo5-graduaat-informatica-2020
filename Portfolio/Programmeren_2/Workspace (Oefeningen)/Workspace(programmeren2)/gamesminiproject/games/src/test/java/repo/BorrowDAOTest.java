package repo;

import domain.Borrow;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

class BorrowDAOTest {

    private Connection connection;

    @BeforeEach
    public void connection(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/gamesTest", DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFindAllBorrows(){
        BorrowDAO dao = new BorrowDAO(connection);
        List<Borrow> results = dao.findAllBorrows();
        Assertions.assertFalse(results.isEmpty());
    }

    @Test
    public void testFindAllBorrowsByBorrower(){
        BorrowDAO dao = new BorrowDAO(connection);
        List<Borrow> results = dao.findAllBorrowsByBorrower("Jan Peeters");
        Assertions.assertFalse(results.isEmpty());
    }
}


