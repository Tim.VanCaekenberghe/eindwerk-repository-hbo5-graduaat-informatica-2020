package repo;


import domain.Borrower;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

    public class BorrowerDAOTest {
        private Connection connection;

        @BeforeEach
        public void connection(){
            try {
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/gamesTest", DataBaseUtil.USERNAME, "");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Test
        public void testFindBorrowersStartingWith(){
            BorrowerDAO dao = new BorrowerDAO(connection);
            List<Borrower> results = dao.findBorrowersStartingWith("Jan Peeter");
            Assertions.assertFalse(results.isEmpty());
        }

        @Test
        public void testFindAllBorrowers(){
            BorrowerDAO dao = new BorrowerDAO(connection);
            List<Borrower> results = dao.findAllBorrowers();
            Assertions.assertFalse(results.isEmpty());
        }

        @Test
        public void testFindBorrowerByID(){
            BorrowerDAO dao = new BorrowerDAO(connection);
            Borrower result = dao.findBorrowerById(1);
            Assertions.assertEquals("Jan Peeters", result.getName());
        }
    }


