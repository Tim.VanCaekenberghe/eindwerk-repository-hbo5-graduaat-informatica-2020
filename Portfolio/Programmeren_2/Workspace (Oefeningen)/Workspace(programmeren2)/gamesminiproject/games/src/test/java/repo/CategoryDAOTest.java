package repo;


import domain.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

    public class CategoryDAOTest {
        private Connection con;

        @BeforeEach
        public void connection(){
            try {
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/gamesTest", DataBaseUtil.USERNAME, "");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }



        @Test
        public void testFindCategoryByID(){
            CategoryDAO dao = new CategoryDAO(con);
            Category result = dao.findCategoryById(1);
            Assertions.assertEquals("combination", result.getCategoryName());
        }

        @Test
        public void testFindCategoriesNameStartsWith(){
            CategoryDAO dao = new CategoryDAO(con);
            List<Category> results = dao.findCategoriesWhereNameStartsWith("combination");
            Assertions.assertFalse(results.isEmpty());
        }
    }

