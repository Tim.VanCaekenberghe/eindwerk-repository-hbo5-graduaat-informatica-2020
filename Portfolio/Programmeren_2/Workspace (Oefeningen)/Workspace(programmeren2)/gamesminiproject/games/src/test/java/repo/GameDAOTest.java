package repo;

import domain.Category;
import domain.Difficulty;
import domain.Game;
import exceptions.IDOutOfBoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import services.CategoryService;
import services.DifficultyService;
import utilities.DataBaseUtil;

import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

    public class GameDAOTest {

        private Connection con;

        @BeforeEach
        public void connection(){
            try {
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/gamesTest", DataBaseUtil.USERNAME, "");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Test
        public void testFindGameByIdReturnsAllGameDetails(){
            Game game = setupGameObject();

            GameDAO dao = new GameDAO(con);
            Game result = dao.findGameByID(2);
            Assertions.assertEquals(game.toString(), result.toString());
        }

        @Test
        public void testFindFirstGameStartingWith(){
            Game game = setupGameObject();

            GameDAO dao = new GameDAO(con);
            Game result = dao.findGameStartingWith("Sabotag");
            Assertions.assertEquals(game.toString(), result.toString());
        }

        @Test
        public void testFindGameByMinDifficulty(){
            GameDAO dao = new GameDAO(con);
            List<Game> results = dao.findGameByMinimumDifficulty(1);

            Assertions.assertFalse(results.isEmpty());
        }

        @Test
        public void testFindAllGames(){
            GameDAO dao = new GameDAO(con);
            List<Game> results = dao.findAllGames();

            Assertions.assertFalse(results.isEmpty());
        }

        private Game setupGameObject() {
            Game game = new Game();
            game.setId(2);
            game.setName("Sabotage");
            game.setEditor("Franjos");
            game.setAuthor("Abbott Robert");
            game.setYear(1996);
            game.setAge("as of 9y");
            game.setMinimumPlayers(2);
            game.setMaximumPlayers(2);
            game.setCategory(new Category(12, "gambling"));
            game.setDuration("16 min to 45 min");
            game.setDifficulty(new Difficulty(2, "easy"));
            game.setPrice(new Float(17.50));
            game.setImage("sabotage.jpg");
            return game;
        }

    }


