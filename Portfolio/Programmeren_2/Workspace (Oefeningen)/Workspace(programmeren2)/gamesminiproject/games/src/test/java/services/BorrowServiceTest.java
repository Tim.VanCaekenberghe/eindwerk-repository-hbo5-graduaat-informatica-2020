package services;


import domain.Borrow;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.BorrowDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

    @ExtendWith(MockitoExtension.class)
    public class BorrowServiceTest {

        @Mock
        BorrowDAO dao;

        @InjectMocks
        BorrowService service;

        @Test @Tag("search")
        public void testFindAllBorrows() throws NoRecordFoundException {
            List<Borrow> borrows = new ArrayList<>();
            borrows.add(new Borrow());
            when(dao.findAllBorrows()).thenReturn(borrows);
            List<Borrow> results = service.findAllBorrows();
            verify(dao, times(1)).findAllBorrows();
            assertEquals(borrows, results);
        }

        @Test @Tag("search")
        public void testFindAllBorrowsFromBorrower() throws NoRecordFoundException {
            List<Borrow> borrows = new ArrayList<>();
            borrows.add(new Borrow());
            when(dao.findAllBorrowsByBorrower("Jan")).thenReturn(borrows);
            List<Borrow> results = service.findAllBorrowsFromBorrower("Jan");
            verify(dao, times(1)).findAllBorrowsByBorrower(anyString());
            assertEquals(borrows, results);
        }

        @Test @Tag("exceptions")
        public void testFindBorrowsFromBorrowerThrowsIllegalArg(){
            Assertions.assertThrows(IllegalArgumentException.class, () -> {
                service.findAllBorrowsFromBorrower("");
            });
        }

        @Test @Tag("exceptions")
        public void testFindBorrowsFromBorrowerThrowsNoRecordExc(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                List<Borrow> borrows = new ArrayList<>();
                when(dao.findAllBorrowsByBorrower("No one")).thenReturn(borrows);
                service.findAllBorrowsFromBorrower("No one");
            });
        }

        @Test @Tag("exceptions")
        public void testFindAllBorrowsThrowsNoRecordException(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                List<Borrow> borrows = new ArrayList<>();
                when(dao.findAllBorrows()).thenReturn(borrows);
                service.findAllBorrows();
            });
        }
    }


