package services;

import domain.Borrower;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.BorrowerDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

    @ExtendWith(MockitoExtension.class)
    public class BorrowerServiceTest {
        @Mock
        BorrowerDAO dao;

        @InjectMocks
        BorrowerService service;

        @Test @Tag("search")
        public void testFindAllBorrowers() throws NoRecordFoundException {
            List<Borrower> borrowers = new ArrayList<>();
            borrowers.add(new Borrower());
            when(dao.findAllBorrowers()).thenReturn(borrowers);
            List<Borrower> results = service.findAllBorrowers();
            verify(dao, times(1)).findAllBorrowers();
            assertEquals(borrowers, results);
        }

        @Test @Tag("search")
        public void testFindBorrowersByName() throws NoRecordFoundException {
            List<Borrower> borrowers = new ArrayList<>();
            borrowers.add(new Borrower());
            when(dao.findBorrowersStartingWith("Jan")).thenReturn(borrowers);
            List<Borrower> results = service.findBorrowersByNameStartingWith("Jan");
            verify(dao, times(1)).findBorrowersStartingWith(anyString());
            assertEquals(borrowers, results);
        }

        @Test @Tag("search")
        public void testFindBorrowerById() throws NoRecordFoundException, IDOutOfBoundException {
            Borrower borrower = new Borrower();
            when(dao.findBorrowerById(1)).thenReturn(borrower);
            Borrower result = service.findBorrowerById(1);
            verify(dao, times(1)).findBorrowerById(anyInt());
            assertEquals(borrower, result);
        }

        @Test @Tag("exceptions")
        public void testAllBorrowsNoRecordFound(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                List<Borrower> borrowers = new ArrayList<>();
                when(dao.findAllBorrowers()).thenReturn(borrowers);
                service.findAllBorrowers();
            });
        }

        @Test @Tag("exceptions")
        public void testSearchNameNoRecordFound(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                List<Borrower> borrowers = new ArrayList<>();
                when(dao.findBorrowersStartingWith("No one")).thenReturn(borrowers);
                service.findBorrowersByNameStartingWith("No one");
            });
        }

        @Test @Tag("exceptions")
        public void testSearchNameIllegalArgument(){
            Assertions.assertThrows(IllegalArgumentException.class, () -> {
                service.findBorrowersByNameStartingWith("");
            });
        }

        @Test @Tag("exceptions")
        public void testSearchByIDOutOfBound(){
            Assertions.assertThrows(IDOutOfBoundException.class, () -> {
                service.findBorrowerById(-10);
            });
        }

        @Test @Tag("exceptions")
        public void testFindByIDNoRecordFound(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                Borrower borrower = null;
                when(dao.findBorrowerById(1000)).thenReturn(borrower);
                service.findBorrowerById(1000);
            });
        }
    }


