package services;

import domain.Category;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.CategoryDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {


        @InjectMocks
        CategoryService categoryService;

        @Mock
        private CategoryDAO categoryDAO;

        @Test
        public void testCategoryFindById() throws NoRecordFoundException {
            Category category =new Category();
            when(categoryDAO.findCategoryById(anyInt())).thenReturn(category);

            Category result = categoryService.findCategoryById(54654);

            assertEquals(category,result);
            verify(categoryDAO,times(1)).findCategoryById(anyInt());
        }
    @Test
    public void testFindCategoryById() throws NoRecordFoundException {
        Category category = new Category();
        when(categoryDAO.findCategoryById(1)).thenReturn(category);
        Category category1 = categoryService.findCategoryById(1);
        Assertions.assertEquals(category, category1);
        verify(categoryDAO, times(1)).findCategoryById(1);
    }



    @Test
    public void errorWhenNoRecordsAreFound(){
        when(categoryDAO.findCategoryById(0)).thenReturn(null);

        Assertions.assertThrows(NoRecordFoundException.class, () ->{
            Category result = categoryService.findCategoryById(0);
        });
    }

    @Test
    public void testFindCategoryStartingWith() throws NoRecordFoundException {
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(new Category());
        when(categoryDAO.findCategoriesWhereNameStartsWith("ca")).thenReturn(categoryList);

        List<Category> results = categoryService.findCategoryWhereNameStartsWith("ca");

        Assertions.assertEquals(categoryList, results);
    }
}


//    @Test
//    public void testFindByIdThrowsNoRecordFoundException(){
//
//        when(categoryDAO.findCategoryById(1)).thenReturn(null);
//
//        assertThrows(NoRecordFoundException.class,()->{
//            Category result = categoryService.findCategoryById(1);
//        });
//    }



