package services;


import domain.Difficulty;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.DifficultyDAO;

import static org.mockito.Mockito.*;

    @ExtendWith(MockitoExtension.class)

    public class DifficultyServiceTest {

        @Mock
        DifficultyDAO dao;

        @InjectMocks
        DifficultyService service;

        @Test
        public void testFindDifficulty() throws IDOutOfBoundException, NoRecordFoundException {
            Difficulty difficulty = new Difficulty(1, "very easy");
            when(dao.difficultyByID(3))
                    .thenReturn(difficulty);

            Difficulty result = service.findDifficultyById(3);
            verify(dao, times(1)).difficultyByID(anyInt());
            Assertions.assertEquals(difficulty, result);
        }

        @Test
        public void testDifficultyThrowsIDExceptionWhenIDTooSmall(){
            Assertions.assertThrows(IDOutOfBoundException.class, () -> {
                Difficulty result = service.findDifficultyById(0);
            });
        }

        @Test
        public void testDifficultyThrowsNoRecordExceptionWhenNoRecordsExist(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                when(dao.difficultyByID(5)).thenReturn(null);
                Difficulty result = service.findDifficultyById(5);
            });
        }
    }


