package services;



import domain.Game;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.GameDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

    @ExtendWith(MockitoExtension.class)
    public class GameServiceTest {

        @Mock
        private GameDAO dao;

        @InjectMocks
        GameService service;

        @Test @Tag("search")
        public void testFindGameByID() throws IDOutOfBoundException, NoRecordFoundException {
            Game game = new Game();
            when(dao.findGameByID(1)).thenReturn(game);
            Game result = service.findGameByID(1);
            verify(dao, times(1)).findGameByID(Mockito.anyInt());
            assertEquals(game, result);
        }

        @Test @Tag("search")
        public void testFindGameStartingWith() throws NoRecordFoundException {
            Game game = new Game();
            when(dao.findGameStartingWith("Ab")).thenReturn(game);
            Game result = service.findGameStartingWith("Ab");
            verify(dao, times(1)).findGameStartingWith(anyString());
            assertEquals(game, result);
        }

        @Test @Tag("search")
        public void testFindGamesMinDifficulty() throws NoRecordFoundException {
            List<Game> games = new ArrayList<>();
            games.add(new Game());
            when(dao.findGameByMinimumDifficulty(1)).thenReturn(games);
            List<Game> results = service.findGameByMinimumDifficulty(1);
            verify(dao, times(1)).findGameByMinimumDifficulty(anyInt());
            assertEquals(games, results);
        }

        @Test @Tag("search")
        public void testFindAllGamesReturnsResults() throws NoRecordFoundException {
            List<Game> games = new ArrayList<>();
            games.add(new Game());
            when(dao.findAllGames()).thenReturn(games);
            List<Game> results = service.findAllgames();
            verify(dao, times(1)).findAllGames();
            assertEquals(games, results);
        }

        @Test @Tag("exceptions") @DisplayName("findAllGames returns NoRecordFoundException when no games returned")
        public void testFindALlGamesThrowsNoRecordsFound(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                List<Game> games = new ArrayList<>();
                when(dao.findAllGames()).thenReturn(games);
                service.findAllgames();
            });
        }

        @Test @Tag("exceptions") @DisplayName("findGamesByMinimumDifficulty throwing IllegalArgumentException when argument is invalid difficulty")
        public void testFindGamesMinDifficultyThrowingIllegalArgException(){
            Assertions.assertThrows(IllegalArgumentException.class, () -> {
                service.findGameByMinimumDifficulty(10);
            });
        }

        @Test @Tag("exceptions") @DisplayName("findGamesByMinimumDifficulty throwing NoRecordsFoundException when list is empty")
        public void testFindGamesMinDifficultyThrowingNoRecordsFound(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                List<Game> games = new ArrayList<>();
                when(dao.findGameByMinimumDifficulty(1)).thenReturn(games);
                service.findGameByMinimumDifficulty(1);
            });
        }

        @Test @Tag("exceptions")  @DisplayName("findGameStartingWith throwing illegalargumentexception when argument is empty")
        public void testGameStartWithEmptyArgThrowsException(){
            Assertions.assertThrows(IllegalArgumentException.class, () -> {
                service.findGameStartingWith("");
            });
        }

        @Test @Tag("exceptions") @DisplayName("findGameStartingWith throwing NoRecordFoundException when no records are found")
        public void testGameStartWithNoRecordsThrowsException(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                when(dao.findGameStartingWith("NonExistentGame")).thenReturn(null);
                service.findGameStartingWith("NonExistentGame");
            });
        }

        @Test @Tag("exceptions")
        public void testTooSmallIDThrowsError(){
            Assertions.assertThrows(IDOutOfBoundException.class, () -> {
                Game game = service.findGameByID(-1);
            });
        }

        @Test @DisplayName("Find Game By ID throws error when no records are found") @Tag("exceptions")
        public void testFindGameByIDThrowsNoRecordException(){
            Assertions.assertThrows(NoRecordFoundException.class, () -> {
                when(dao.findGameByID(500)).thenReturn(null);
                Game game = service.findGameByID(500);
            });
        }
    }


