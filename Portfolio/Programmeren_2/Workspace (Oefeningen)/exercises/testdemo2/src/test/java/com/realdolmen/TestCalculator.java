package com.realdolmen;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)

public class TestCalculator {

    @Mock
    MathUtil mathUtil;

    @InjectMocks
    Calculator calculator;

    @BeforeEach
    void setUp() {
    /*Mockito.when(mathUtil.getSum(5,5)).thenReturn(10);
    Mockito.when(mathUtil.multiply(15,10)).thenReturn(200);*/

    }

    @Test
    public void testSumAndMultiply() {
    int result = calculator.sumAndMultiply(5,5,10);
        Assertions.assertEquals(100, result);

    }

    @Test
    public void testPersonHasCorrectName() {
        ArgumentCaptor<Person> personArgumentCaptor = ArgumentCaptor.forClass(Person.class);
        calculator.printPerson();
        Mockito.verify(mathUtil).showPerson(personArgumentCaptor.capture());
        Assertions.assertEquals("Jimmi", personArgumentCaptor.getValue().getName());

    }


    }


