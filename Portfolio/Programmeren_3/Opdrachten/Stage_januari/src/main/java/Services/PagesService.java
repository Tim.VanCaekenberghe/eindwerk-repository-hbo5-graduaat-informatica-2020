package Services;

import Domain.Product;

import java.util.ArrayList;

public class PagesService {
    public static ArrayList<ArrayList<Product>> createPages(ArrayList<Product> allProducts) {
        ArrayList<ArrayList<Product>> pages = new ArrayList<>();
        ArrayList<Product> temporary = new ArrayList<>();
        for (int counter = 1; allProducts.size() + 1 > counter; counter++) {
            Product product = allProducts.get(counter - 1);
            temporary.add(product);
            if (counter % 20 == 0) {
                pages.add(temporary);
                temporary = new ArrayList<>();
            }
        }
        if (!temporary.isEmpty()) {
            pages.add(temporary);
        }
        return pages;
    }


}
