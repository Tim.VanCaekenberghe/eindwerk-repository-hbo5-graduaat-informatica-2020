package Services;

import Domain.Product;
import Exceptions.NoRecordsFoundException;

import java.util.ArrayList;


public class SearchService {

    public static ArrayList<Product> searchOnName(String search, ArrayList<Product> allObjects) throws NoRecordsFoundException {
        ArrayList<Product> searchResults = new ArrayList<>();
        if (allObjects.isEmpty() || allObjects.size() == 0) {
            throw new NoRecordsFoundException();
        } else
            for (Product p : allObjects) {
                if (p.getName().toLowerCase().contains(search.toLowerCase())) {
                    searchResults.add(p);
                }
            }
        return searchResults;
    }


    public static ArrayList<Product> searchOnArticleId(String search, ArrayList<Product> allObjects) throws NoRecordsFoundException {
        ArrayList<Product> searchResults = new ArrayList<>();
        if (allObjects.isEmpty() || allObjects.size() == 0) {
            throw new NoRecordsFoundException();
        } else
            for (Product p : allObjects) {
                if (Long.toString(p.getArticleId()).contains(search)) {
                    searchResults.add(p);
                }
            }
        return searchResults;

    }


    public static ArrayList<Product> searchOnBrand(String search, ArrayList<Product> allObjects) throws NoRecordsFoundException {
        ArrayList<Product> searchResults = new ArrayList<>();
        if (allObjects.isEmpty() || allObjects.size() == 0) {
            throw new NoRecordsFoundException();
        }
        for (Product p : allObjects) {
            if (p.getBrand().toLowerCase().equals(search.toLowerCase())) {
                searchResults.add(p);
            }
        }
        return searchResults;
    }


    public static ArrayList<Product> searchOnCategory(String search, ArrayList<Product> allObjects) throws NoRecordsFoundException {
        ArrayList<Product> searchResults = new ArrayList<>();
        if (allObjects.isEmpty() || allObjects.size() == 0) {
            throw new NoRecordsFoundException();
        } else
            for (Product p : allObjects) {
                if (p.getCategory().toLowerCase().contains(search.toLowerCase())) {
                    searchResults.add(p);
                }
            }
        return searchResults;
    }
}

