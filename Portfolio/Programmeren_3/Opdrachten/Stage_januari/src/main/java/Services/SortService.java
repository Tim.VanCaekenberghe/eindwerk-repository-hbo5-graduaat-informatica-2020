package Services;

import Domain.Product;

import java.util.ArrayList;
import java.util.Comparator;

public class SortService {

    private SortService() {
    }

    public static void sortAlphabet(ArrayList<Product> listToSort) {
        listToSort.sort(Comparator.comparing(Products -> Products.getName().toLowerCase()));
    }

    public static void sortByNumber(ArrayList<Product> listToSort) throws NullPointerException {
        listToSort.sort(Comparator.comparing(Product::getNumber));
    }

    public static void sortByPrice(ArrayList<Product> listToSort) throws NullPointerException {
        listToSort.sort(Comparator.comparing(Product::getPrice));
    }


    public static void sortByArticleId(ArrayList<Product> listToSort) {
        listToSort.sort(Comparator.comparing(Product::getArticleId));
    }
}