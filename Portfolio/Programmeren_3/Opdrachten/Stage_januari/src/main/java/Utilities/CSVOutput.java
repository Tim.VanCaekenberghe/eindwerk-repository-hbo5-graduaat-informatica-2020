package Utilities;


import Domain.Product;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;


public class CSVOutput {

    private CSVPrinter csvPrinter = new CSVPrinter();
    Locale locale = Locale.FRANCE;
    Currency currency = Currency.getInstance(locale);
    String points = "..";

    public void printPage(ArrayList<Product> page) {
        csvPrinter.clearRows();
        csvPrinter.setShowVerticalLines(true);
        csvPrinter.setHeaders("artikelnr", "naam", "categorie", "merk", "prijs", "aantal");
        for (int i = 0; i < page.size(); i++) {
            Double number = page.get(i).getNumber();
            String aantal = (number < 0) ? "Out of stock" : Double.toString(number);
            csvPrinter.addRow(Long.toString(page.get(i).getArticleId()), page.get(i).getName(), getPartOfName(page.get(i).getCategory()), page.get(i).getBrand(), currency.getSymbol() + String.format("%.2f", (page.get(i).getPrice())).replace(".", ","), aantal);
        }
        csvPrinter.print();
    }

    private String getPartOfName(String categoryName) {
        int splitAfter = 26;
        String firstPart;
        if (categoryName.length() > 25) {
            firstPart = categoryName.substring(0, splitAfter) + points;
        } else firstPart = categoryName;
        return firstPart;
    }

    public void printAllRecords(ArrayList<Product> allObjects) {
        csvPrinter.clearRows();
        csvPrinter.setShowVerticalLines(true);
        csvPrinter.setHeaders("artikelnr", "naam", "categorie", "merk", "prijs", "aantal");
        for (int i = 0; i < allObjects.size(); i++) {
            Double number = allObjects.get(i).getNumber();
            String aantal = (number < 0) ? "Out of stock" : Double.toString(number);
            csvPrinter.addRow(Long.toString(allObjects.get(i).getArticleId()), allObjects.get(i).getName(), getPartOfName(allObjects.get(i).getCategory()), allObjects.get(i).getBrand(), currency.getSymbol() + String.format("%.2f", allObjects.get(i).getPrice()).replace(".", ","), aantal);
        }
        csvPrinter.print();

    }

    public void setCsvPrinter(CSVPrinter csvPrinter) {
        this.csvPrinter = csvPrinter;
    }

}
