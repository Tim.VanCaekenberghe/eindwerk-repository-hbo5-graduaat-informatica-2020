package Services;

import Domain.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class PagesServiceTest {


    @Test
    public void pageServiceTest60() {
        ArrayList<Product> input = new ArrayList<>();
        Product product = new Product(1, "iets", "nogiets", " nogeens", 1.00, 1.0);
        for (int i = 0; i < 60; i++) {
            input.add(product);
        }
        ArrayList<ArrayList<Product>> output = PagesService.createPages(input);
        ArrayList<Product> lijst = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            lijst.add(product);
        }
        ArrayList<ArrayList<Product>> expected = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            expected.add(lijst);
        }
        Assertions.assertArrayEquals(expected.toArray(), output.toArray());

    }

    @Test
    public void pageServiceTest24() {

        ArrayList<Product> input = new ArrayList<>();
        Product product = new Product(1, "iets", "nogiets", " nogeens", 1.00, 1.0);
        for (int i = 0; i < 24; i++) {
            input.add(product);
        }
        ArrayList<ArrayList<Product>> output = PagesService.createPages(input);
        ArrayList<Product> lijst20 = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            lijst20.add(product);
        }
        ArrayList<Product> lijst4 = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            lijst4.add(product);
        }
        ArrayList<ArrayList<Product>> expected = new ArrayList<>();
        expected.add(lijst20);
        expected.add(lijst4);
        Assertions.assertArrayEquals(expected.toArray(), output.toArray());

    }

    @Test
    public void pageServiceTest4() {

        ArrayList<Product> input = new ArrayList<>();
        Product product = new Product(1, "iets", "nogiets", " nogeens", 1.00, 1.0);
        for (int i = 0; i < 4; i++) {
            input.add(product);
        }
        ArrayList<ArrayList<Product>> output = PagesService.createPages(input);
        ArrayList<Product> lijst4 = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            lijst4.add(product);
        }
        ArrayList<ArrayList<Product>> expected = new ArrayList<>();
        expected.add(lijst4);
        Assertions.assertArrayEquals(expected.toArray(), output.toArray());

    }

    @Test
    public void createPages() {
        ArrayList<Product> expected = new ArrayList<>();
        ArrayList<Product> actual = new ArrayList<>();

        Product item = new Product(1, "koelen", "huishoudproducten", "lipson", 120.00, 15);
        Product item2 = new Product(2, "koelen", "huishoudproducten", "lipson", 120.00, 16);
        Product item3 = new Product(3, "koelen", "huishoudproducten", "lipson", 120.00, 17);

        expected.add(item);
        expected.add(item2);
        expected.add(item3);

        actual.add(item);
        actual.add(item2);
        actual.add(item3);


        assertFalse(expected == actual);
        assertFalse(expected.toArray() == actual.toArray());
        assertTrue(expected.size() > 0);
        assertFalse(expected.size() < 1);
        assertFalse(expected.isEmpty());
        assertTrue(actual.size() > 0);
        assertEquals(3, expected.size());
        assertTrue(expected.size() < 4);
        assertTrue(expected.get(1) != expected.get(2));


    }

}