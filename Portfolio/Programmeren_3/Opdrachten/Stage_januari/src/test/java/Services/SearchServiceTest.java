package Services;

import Domain.Product;
import Exceptions.NoRecordsFoundException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class SearchServiceTest {

    @Test
    public void searchOnNameTest() throws NoRecordsFoundException {
        String expected = "LEKBAK";
        ArrayList<Product> list = new ArrayList<>();
        Product product = new Product(12l, expected, expected, expected, 1.0, 1.0);
        for (int i = 0; i < 25; i++) {
            list.add(product);
        }
        SearchService search = new SearchService();
        ArrayList<Product> searchResults = search.searchOnName(expected, list);
        assertArrayEquals(list.toArray(), searchResults.toArray());
        assertNotSame(list.toArray(), searchResults.toArray());


    }

    @Test
    public void searchOnNameThrowsNoException() {
        assertDoesNotThrow(() -> {
            String expected = "LEKBAK";
            ArrayList<Product> list = new ArrayList<>();
            Product product = new Product(12l, expected, expected, expected, 1.0, 1.0);
            for (int i = 0; i < 25; i++) {
                list.add(product);
            }
            SearchService searchService = new SearchService();
            searchService.searchOnName(expected, list);
        });
    }

    @Test
    public void searchOnNameExpectedException() throws NoRecordsFoundException {
        String notExpected = "elite";
        ArrayList<Product> list = new ArrayList<>();
        SearchService searchService = new SearchService();
        assertThrows(NoRecordsFoundException.class, () -> {
            searchService.searchOnName(notExpected, list);
        });


    }


    @Test
    public void searchOnArticleIdTest() throws NoRecordsFoundException {
        String expected = String.valueOf(1);
        ArrayList<Product> list = new ArrayList<>();
        Product product = new Product(12l, expected, expected, expected, 1.0, 1.0);
        for (int i = 0; i < 25; i++) {
            list.add(product);
        }
        SearchService search = new SearchService();
        ArrayList<Product> searchResults = search.searchOnArticleId(expected, list);
        assertArrayEquals(list.toArray(), searchResults.toArray());
        assertNotSame(list.toArray(), searchResults.toArray());

    }

    @Test
    public void searchOnId2() {
        String notExpected = "135412";
        ArrayList<Product> list = new ArrayList<>();
        SearchService searchService = new SearchService();
        assertThrows(NoRecordsFoundException.class, () -> {
            searchService.searchOnArticleId(notExpected, list);
        });
    }

    @Test
    public void searchOnIdThrowsNoException() {
        assertDoesNotThrow(() -> {
            String expected = "121";
            ArrayList<Product> list = new ArrayList<>();
            Product product = new Product(121, expected, expected, expected, 1.0, 1.0);
            for (int i = 0; i < 25; i++) {
                list.add(product);
            }
            SearchService searchService = new SearchService();
            searchService.searchOnArticleId(expected, list);
        });
    }

    @Test
    public void searchOnBrand2Test() throws NoRecordsFoundException {
        String expected = "BULK";
        ArrayList<Product> list = new ArrayList<>();
        Product product = new Product(12l, expected, expected, expected, 1.0, 1.0);
        for (int i = 0; i < 25; i++) {
            list.add(product);
        }
        SearchService search = new SearchService();
        ArrayList<Product> searchResults = search.searchOnBrand(expected, list);
        assertArrayEquals(list.toArray(), searchResults.toArray());
        assertNotSame(list.toArray(), searchResults.toArray());

    }

    @Test
    public void searchOnBrandTest() {
        String notExpected = "elite";
        ArrayList<Product> list = new ArrayList<>();
        SearchService searchService = new SearchService();
        assertThrows(NoRecordsFoundException.class, () -> {
            searchService.searchOnBrand(notExpected, list);
        });
    }

    @Test
    public void searchOnBrandThrowsNoException() {
        assertDoesNotThrow(() -> {
            String expected = "LEKBAK";
            ArrayList<Product> list = new ArrayList<>();
            Product product = new Product(12l, expected, expected, expected, 1.0, 1.0);
            for (int i = 0; i < 25; i++) {
                list.add(product);
            }
            SearchService searchService = new SearchService();
            searchService.searchOnBrand(expected, list);
        });
    }

    @Test
    public void searchOnCategoryTest() throws NoRecordsFoundException {
        String expected = "LEKBAK";
        ArrayList<Product> list = new ArrayList<>();
        Product product = new Product(12l, expected, expected, expected, 1.0, 1.0);
        for (int i = 0; i < 25; i++) {
            list.add(product);
        }
        SearchService search = new SearchService();
        ArrayList<Product> searchResults = search.searchOnCategory(expected, list);
        assertArrayEquals(list.toArray(), searchResults.toArray());
        assertNotSame(list.toArray(), searchResults.toArray());

    }

    @Test
    public void searchOnCategory2Test() {
        String notExpected = "elite";
        ArrayList<Product> list = new ArrayList<>();
        SearchService searchService = new SearchService();
        assertThrows(NoRecordsFoundException.class, () -> {
            searchService.searchOnCategory(notExpected, list);
        });
    }

    @Test
    public void searchOnCategoryThrowsNoException() {
        assertDoesNotThrow(() -> {
            String expected = "LEKBAK";
            ArrayList<Product> list = new ArrayList<>();
            Product product = new Product(12l, expected, expected, expected, 1.0, 1.0);
            for (int i = 0; i < 25; i++) {
                list.add(product);
            }
            SearchService searchService = new SearchService();
            searchService.searchOnCategory(expected, list);
        });
    }
}