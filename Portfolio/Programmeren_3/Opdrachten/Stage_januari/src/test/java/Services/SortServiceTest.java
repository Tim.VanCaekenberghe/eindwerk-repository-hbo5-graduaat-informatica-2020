package Services;

import Domain.Product;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class SortServiceTest {

    @Test
    public void sortAlphabet() {

        // set up data
        ArrayList<Product> nameList = new ArrayList<>();
        ArrayList<Product> sortedList = new ArrayList<>();

        Product item = new Product(1, "A", "huishoudproducten", "lipson", 120.00, 15);
        Product item2 = new Product(2, "C", "huishoudproducten", "lipson", 120.00, 16);
        Product item3 = new Product(3, "B", "huishoudproducten", "lipson", 120.00, 16);

        nameList.add(item);
        nameList.add(item2);
        nameList.add(item3);

        sortedList.add(item);
        sortedList.add(item2);
        sortedList.add(item3);
        ArrayList<Product> result = nameList;
        SortService.sortAlphabet(sortedList);


        // assert
        assertEquals(result.get(0).getName(), sortedList.get(0).getName());
        assertEquals(result.get(2).getName(), sortedList.get(1).getName());
        assertFalse(result.toArray() == sortedList.toArray());
        assertTrue(sortedList.size() > 0);
        assertFalse(sortedList.size() < 1);
        assertFalse(sortedList.isEmpty());

    }


    @Test
    public void sortByNumber() {
        ArrayList<Product> sortedList = new ArrayList<>();
        ArrayList<Product> actual = new ArrayList<>();

        Product item = new Product(1, "koelen", "huishoudproducten", "lipson", 120.00, 15);
        Product item2 = new Product(2, "koelen", "huishoudproducten", "lipson", 120.00, 20);
        Product item3 = new Product(3, "koelen", "huishoudproducten", "lipson", 120.00, 18);

        actual.add(item);
        actual.add(item2);
        actual.add(item3);

        sortedList.add(item);
        sortedList.add(item2);
        sortedList.add(item3);

        SortService.sortByNumber(sortedList);

        assertEquals(actual.get(0).getNumber(), sortedList.get(0).getNumber());
        assertEquals(actual.get(1).getNumber(), sortedList.get(2).getNumber());
        assertFalse(actual.toArray() == sortedList.toArray());
        assertTrue(actual.size() > 0);
        assertFalse(actual.size() < 1);
        assertFalse(actual.isEmpty());
    }


    @Test
    public void sortByPrice() {
        ArrayList<Product> sortedList = new ArrayList<>();
        ArrayList<Product> actual = new ArrayList<>();

        Product item = new Product(1, "koelen", "huishoudproducten", "lipson", 121.00, 15);
        Product item2 = new Product(2, "koelen", "huishoudproducten", "lipson", 123.00, 15);
        Product item3 = new Product(3, "koelen", "huishoudproducten", "lipson", 122.00, 15);

        actual.add(item);
        actual.add(item2);
        actual.add(item3);

        sortedList.add(item3);
        sortedList.add(item2);
        sortedList.add(item);

        SortService.sortByPrice(sortedList);

        assertEquals(actual.get(0).getNumber(), sortedList.get(0).getNumber());
        assertEquals(actual.get(1).getNumber(), sortedList.get(2).getNumber());
        assertFalse(sortedList.toArray() == actual.toArray());
        assertTrue(sortedList.size() > 0);
        assertTrue(sortedList.size() < 200);
        assertFalse(sortedList.isEmpty());
    }


    @Test
    public void sortByArticleId() {
        ArrayList<Product> sortedList = new ArrayList<>();
        ArrayList<Product> actual = new ArrayList<>();

        Product item = new Product(1, "koelen", "huishoudproducten", "lipson", 120.00, 15);
        Product item2 = new Product(3, "koelen", "huishoudproducten", "lipson", 120.00, 15);
        Product item3 = new Product(2, "koelen", "huishoudproducten", "lipson", 120.00, 15);

        actual.add(item);
        actual.add(item2);
        actual.add(item3);

        sortedList.add(item3);
        sortedList.add(item2);
        sortedList.add(item);

        SortService.sortByArticleId(sortedList);

        assertEquals(actual.get(0).getNumber(), sortedList.get(0).getNumber());
        assertEquals(actual.get(1).getNumber(), sortedList.get(2).getNumber());
        assertFalse(sortedList.toArray() == actual.toArray());
        assertTrue(sortedList.size() > 0);
        assertFalse(sortedList.size() < 1);
        assertFalse(sortedList.isEmpty());
    }

}
