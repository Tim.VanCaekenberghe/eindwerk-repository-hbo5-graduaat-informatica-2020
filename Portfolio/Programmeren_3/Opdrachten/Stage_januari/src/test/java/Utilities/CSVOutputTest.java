package Utilities;

import Domain.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;


@ExtendWith(MockitoExtension.class)


public class CSVOutputTest {

    @Test
    public void printPage() {
        CSVOutput csvOutput = new CSVOutput();
        CSVPrinter mockPrinter = Mockito.mock(CSVPrinter.class);

        csvOutput.setCsvPrinter(mockPrinter);


        ArrayList<Product> page = new ArrayList<>();
        Product product = new Product(140120001, "LEKBAK", "Witgoed\\Universeel\\Diverse..", "SCANPART", 19.99, 519);
        page.add(product);


        csvOutput.printPage(page);

        Mockito.verify(mockPrinter).setShowVerticalLines(true);

        Mockito.verify(mockPrinter).setHeaders("artikelnr", "naam", "categorie", "merk", "prijs", "aantal");

        Mockito.verify(mockPrinter).addRow(Long.toString(product.getArticleId()), product.getName(), product.getCategory(), product.getBrand(), "€" + Double.toString(product.getPrice()).replace(".", ","), Double.toString(product.getNumber()));

        Mockito.verify(mockPrinter).print();

        Mockito.verify(mockPrinter).clearRows();

        Mockito.verify(mockPrinter, Mockito.times(1)).addRow(Long.toString(product.getArticleId()), product.getName(), product.getCategory(), product.getBrand(), "€" + Double.toString(product.getPrice()).replace(".", ","), Double.toString(product.getNumber()));

        Mockito.verify(mockPrinter, Mockito.times(1)).addRow(Mockito.any());

    }

    @Test
    public void printAllRecordsTest() {
        CSVOutput csvOutput = new CSVOutput();
        CSVPrinter mockPrinter = Mockito.mock(CSVPrinter.class);

        csvOutput.setCsvPrinter(mockPrinter);

        ArrayList<Product> all = new ArrayList<>();
        Product product = new Product(140120001, "LEKBAK", "Witgoed\\Universeel\\Diverse..", "SCANPART", 19.99, 519);
        all.add(product);

        csvOutput.printAllRecords(all);

        Mockito.verify(mockPrinter).setShowVerticalLines(true);

        Mockito.verify(mockPrinter).setHeaders("artikelnr", "naam", "categorie", "merk", "prijs", "aantal");

        Mockito.verify(mockPrinter).addRow(Long.toString(product.getArticleId()), product.getName(), product.getCategory(), product.getBrand(), "€" + Double.toString(product.getPrice()).replace(".", ","), Double.toString(product.getNumber()));

        Mockito.verify(mockPrinter).print();

        Mockito.verify(mockPrinter, Mockito.times(1)).addRow(Long.toString(product.getArticleId()), product.getName(), product.getCategory(), product.getBrand(), "€" + Double.toString(product.getPrice()).replace(".", ","), Double.toString(product.getNumber()));

        Mockito.verify(mockPrinter, Mockito.times(1)).addRow(Mockito.any());

    }


}


