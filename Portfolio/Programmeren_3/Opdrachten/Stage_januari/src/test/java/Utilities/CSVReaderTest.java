package Utilities;

import Domain.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CSVReaderTest {

    private final Product product = new Product(140120001, "LEKBAK", "Witgoed\\Universeel\\Diverse accessoires", "SCANPART", 19.99, 519);

    CSVReader csvReader;

    @BeforeEach
    public void setUp() {
        csvReader = new CSVReader();
    }

    @Test
    public void loadDataFromFile() throws URISyntaxException, FileNotFoundException {
        ArrayList<Product> products = csvReader.loadDataFromFile(getAbsolutePath("/testfiles/products.csv"));

        assertTrue(products.size() > 0);
        assertEquals(25, products.size());
        assertEquals(product, products.get(0));
        assertTrue(products.size() < 26);
        assertFalse(products.isEmpty());
        assertFalse(products.get(2) == products.get(0));

    }

    @Test
    public void loadDataFromFileEmptyLine() throws URISyntaxException, FileNotFoundException {
        ArrayList<Product> products = csvReader.loadDataFromFile(getAbsolutePath("/testfiles/productsEmptyLine.csv"));

        assertTrue(products.size() > 0);
        assertEquals(24, products.size());
        assertEquals(product, products.get(0));
        assertTrue(products.size() < 25);
        assertFalse(products.isEmpty());
        assertTrue(products.get(1) != products.get(2));
    }

    private String getAbsolutePath(String relativePath) throws URISyntaxException {
        URI uri = this.getClass().getResource(relativePath).toURI();
        return Paths.get(uri).toFile().getAbsolutePath();
    }

    @Test
    public void loadDataFromFileException() {
        String faultPath = "fdfd";
        assertThrows(FileNotFoundException.class, () -> {
            csvReader.loadDataFromFile(faultPath);
        });
    }

    @Test
    public void exeptionTest() {
        String faultPath = "fdfd";
        assertThrows(Exception.class, () -> {
            csvReader.loadDataFromFile(faultPath);
        });
    }

    @Test
    public void IoExeptionTest() {
        String faultPath = "fdfd";
        assertThrows(IOException.class, () -> {
            csvReader.loadDataFromFile(faultPath);
        });
    }

    @Test
    public void getRecordNumber() {

    }


    @Test
    public void getNumberOfRecords() throws FileNotFoundException, URISyntaxException {
        CSVReader csvReader = new CSVReader();
        int expected = 25;
        ArrayList<Product> list = csvReader.loadDataFromFile(getAbsolutePath("/testfiles/products.csv"));
        int actual = list.size();
        assertEquals(expected, actual);

    }


    @Test
    public void getAllRecords() throws URISyntaxException, FileNotFoundException {
        CSVReader csvReader = new CSVReader();
        ArrayList<Product> actual = csvReader.loadDataFromFile(getAbsolutePath("/testfiles/products.csv"));
        ArrayList<Product> expected = csvReader.getAllRecords();
        assertTrue(expected.size() > 0);
        assertEquals(25, expected.size());
        assertEquals(product, expected.get(0));
        assertTrue(expected.size() < 26);
        assertFalse(expected.isEmpty());
        assertTrue(expected.get(2) != expected.get(0));


    }

    @Test
    public void getAndSetTest() {
        int articleId = 140120001;
        String name = "LEKBAK";

        Product products = new Product(140120001, "LEKBAK", "Witgoed\\Universeel\\Diverse accessoires", "SCANPART", 19.99, 519);
        products.setArticleId(articleId);
        products.setName(name);

        assertTrue(products.getArticleId() == articleId, String.valueOf(products.getName().equals(name)));
    }


}


