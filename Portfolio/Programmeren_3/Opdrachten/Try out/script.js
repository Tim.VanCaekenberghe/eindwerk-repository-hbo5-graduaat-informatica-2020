function calculate() {
    let number1 = window.prompt("Please enter the first number")
    let number2 = window.prompt("Please enter the second number")
    let result = parseInt(number1) + parseInt(number2);
    let msg = '<h3>Hello Tim, the result = ' + result + '</h3>'
    document.writeln(msg)
}

function showProfit() {
    var array = new Array(
        {month:"january", figure:150000},
        {month:"february", figure: 250000},
        {month:"march", figure:145000},
        {month:"april", figure: 90000},
        {month:"may", figure:60000},
        {month:"june", figure: 200000},
        {month:"july", figure:195000},
        {month:"august", figure: 56000},
        {month:"september", figure:85000},
        {month:"october", figure: 86000},
        {month:"november", figure:45000},
        {month:"december", figure: 275000},
    );
    return array;
}

function calculateProfit(figure) {
    return parseInt(figure/100)*30
}

function printSales() {
    var array = showProfit();
    var sumFigure = 0;
    for(var i=0; i < array.length; ++i){
    document.getElementById("sales").innerHTML += "<li>"+array[i].month+" "+calculateProfit(array[i].figure)+"</li>";
    sumFigure += array[i].figure;
    }
    document.getElementById("sales").innerHTML += "<p>"+ calculateProfit(sumFigure) +"</p>"
}