function mussels() {

    const months = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    for (var i = 1; i < months.length + 1; i++) {
        if (i >= 5 && i <= 8) {
            continue;
        }
        document.write("Je mag mosselen eten in " + months[i - 1] + "<br>")

    }
}

function musselsSwitch() {
    let currentMonth = "May";
    switch (currentMonth) {
        case "January":
        case "February":
        case "March":
        case "April":
        case "September":
        case "October":
        case "November":
        case "December":
            document.writeln("You can eat mussels in " + currentMonth);
            break;
        case "May":
        case "June":
        case "July":
        case "August":
            document.writeln("You can't eat mussels in " + currentMonth);
            break;
        default: document.writeln("Not a valid month")
    }
}