/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.net;

import javax.persistence.*;

/**
 *
 * @author TVCBL87
 */
@Entity
public class Point {
    @Id
    @GeneratedValue 
    private int id;
    private int y;
    private int x; 

    public Point( int y, int x) {
       
        this.y = y;
        this.x = x;
    }

    public Point() {
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    @Override
    public String toString() {
        return "Point{" + "id=" + id + ", y=" + y + ", x=" + x + '}';
    }

    
   
}
