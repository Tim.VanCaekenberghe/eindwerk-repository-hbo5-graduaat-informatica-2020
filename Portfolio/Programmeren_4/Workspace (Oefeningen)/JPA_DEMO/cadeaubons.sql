ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD';


DROP TABLE bestelling cascade constraints;
DROP TABLE bon cascade constraints;
DROP TABLE thema cascade constraints;
DROP TABLE klant cascade constraints;
DROP SEQUENCE thema_seq;
DROP SEQUENCE bon_seq;
DROP SEQUENCE klant_seq;
DROP SEQUENCE bestelling_seq;

CREATE TABLE thema
(
   id			    smallint        primary key,
   thema		    varchar(50)	    not null
);

CREATE SEQUENCE thema_seq;

INSERT INTO thema VALUES (thema_seq.nextval,'Actief genieten');
INSERT INTO thema VALUES (thema_seq.nextval,'Heerlijk ontspannen');
INSERT INTO thema VALUES (thema_seq.nextval,'Lekker eten');
INSERT INTO thema VALUES (thema_seq.nextval,'Zalig logeren');


CREATE TABLE bon
(
	id			smallint	     primary key,
	bonnr			varchar(16)	not null,
    	themaID		smallint        not null,
	naam			varchar(50)	not null,
    	informatie		varchar(300)    not null,
	leverancier	varchar(50)	not null,
	prijs			numeric(5,2)	not null,
    	geldig_tot		date			not null,
	foreign key (themaID) references thema(id)
);

CREATE SEQUENCE bon_seq;

INSERT INTO bon VALUES (bon_seq.nextval,'ACTBNG09-002837',1,'Avontuur 2016','Deze BONGO bevat een boekje en een bon die 1 persoon recht geeft op 1 van de 40 geselecteerde avontuurlijke activiteiten','www.bongo.be',49.90,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'ACTBNG09-002838',1,'Fietsweekend 2016','Deze BONGO bevat een doosje met daarin een boekje en een bon die recht geeft op een overnachting voor 2 personen, inclusief ontbijt, diner, picknick en gebruik van fietsen, in 1 van de 40 hotels.','www.bongo.be',179.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'ONTBNG09-100245',2,'BONGO Sauna voor twee','Een doosje met daarin een boekje en een bon die recht geeft op toegang voor 2 personen in 1 van de volgende 40 saunacentra.','www.bongo.be',39.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGBNG09-002841',4,'BONGO Citytrip','Deze Bongo bevat een cadeaubon en een boekje. De cadeaubon geeft recht op twee overnachtingen voor twee personen inclusief ontbijt in ��n van de 43 hotels.','www.bongo.be',299.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGBNG09-002842',4,'BONGO Stijlvolle Bed & Breakfast ','Deze BONGO bevat een boekje en een cadeaubon die recht geeft op een verblijf van twee nachten voor twee personen inclusief ontbijt in ��n van de vijftig luxueuze b&b adresjes.','www.bongo.be',199.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGBNG09-002843',4,'BONGO Hippe Hotels ','Deze BONGO bevat een doosje met daarin een boekje en een bon die recht geeft op een overnachting voor 2 personen, inclusief ontbijt in 1 van de 40 hotels.','www.bongo.be',124.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'ETEBNG09-788100',3,'BONGO Healthy Lunch','Deze BONGO bevat een boekje en een bon die 2 personen recht geeft op een gezonde lunch exclusief dranken in 1 van de 30 geselecteerde eetadresjes.','www.bongo.be',29.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'ETEBNG09-788101',3,'BONGO Wereldkeuken 2016','Een doosje met daarin een boekje en een bon. De bon geeft recht op een introductiemaaltijd voor 2 personen exclusief dranken in 1 van 30 geselecteerde wereldkeuken restaurants.','www.bongo.be',34.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'ONTVIV09-100641',2,'Relaxed','1 cadeaubon geldig voor een arrangement voor 2 personen in ��n van de 20 voorgestelde centra (het arrangement bestaat minimaal uit een hele dag vrije toegang tot de thermen)','www.vivaboxes.com',49.5,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'ONTVIV09-100642',2,'Fit=Fun','1 cadeaubon geldig voor een maandabonnement bij Passage Fitness First of voor ��n van de 12 vrijetijdsboeken ','www.vivaboxes.com',35,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'ONTVIV09-100643',2,'Sauna & Thermen','1 cadeaubon geldig voor een arrangement in ��n van de 16 voorgestelde centra ','www.vivaboxes.com',49.9,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'ONTVIV09-100644',2,'Wellness','1 cadeaubon geldig voor een arrangement in ��n van de 24 voorgestelde centra ','www.vivaboxes.com',89.5,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'ACTVIV09-100541',1,'Voetbaltickets ','2 cadeaubonnen, elk in te ruilen voor ��n ticket voor een thuismatch van de Belgische competitie van ��n van de 6 voorgestelde voetbalploegen ','www.vivaboxes.com',49.95,'2017-05-31');
INSERT INTO bon VALUES (bon_seq.nextval,'ACTVIV09-100542',1,'Basketbal tickets ','2 cadeaubonnen, elk in te ruilen voor ��n ticket voor een thuismatch van ��n van de 6 voorgestelde basketbalclubs ','www.vivaboxes.com',29.5,'2017-05-31');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGVIV09-188511',4,'Origineel overnachten','1 cadeaubon geldig voor een arrangement voor 2 personen in ��n van de 20 voorgestelde logies (keuze uit hotels, bed & breakfasts, een woonwagen, een boot, een kunstwerk, ...) ','www.vivaboxes.com',149,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGVIV09-188512',4,'Weekend op het platteland','1 cadeaubon geldig voor een arrangement met 2 nachten voor 2 personen in ��n van de 33 voorgestelde landelijke logies. ','www.vivaboxes.com',175,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGVIV09-188513',4,'Weekend in Champagne','1 cadeaubon geldig voor een overnachting voor 2 personen inclusief ontbijt in ��n van de 2 voorgestelde hotels in Reims ','www.vivaboxes.com',189,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGVIV09-188514',4,'Weekend Bella Italia','1 cadeaubon geldig voor 2 overnachtingen voor 2 personen inclusief ontbijt in ��n van de door ons geselecteerde hotels in Toscane, Umbri� of De Marken ','www.vivaboxes.com',299,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'ETEVIV09-245510',3,'Geschenkbox "Koken met Jeroen" ','4 top producten om te ontdekken en 1 kookboek gehandtekend door Jeroen Meus ','www.vivaboxes.com',49.5,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'ETEVIV09-245511',3,'Tapas voor twee ','1 cadeaubon geldig voor een tapas menu voor 2 personen in ��n van de 20 voorgestelde bars/restaurants ','www.vivaboxes.com',49.5,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'ETEVIV09-245512',3,'Koken met bekende chefs ','1 cadeaubon geldig voor een kookles met 1 van de bekende Kookeiland chefs ','www.vivaboxes.com',79.5,'2017-09-30');
INSERT INTO bon VALUES (bon_seq.nextval,'ACTCAD09-321100',1,'Nieuwe passie','Pak uit met passie als cadeau','www.cadeaubox.be',49.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'ACTCAD09-321101',1,'Luchtdoop','Geef het luchtruim cadeau','www.cadeaubox.be',134.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'ACTCAD09-321102',1,'Adrenaline','Geef een kick met een strik. De cadeaubon in deze Cadeaubox geeft recht op een sensationele ervaring voor ��n tot drie personen bij ��n van de vermelde organisatoren.','www.cadeaubox.be',184.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'ONTACT09-541123',2,'Tijd voor 2','De cadeaubon in deze Cadeaubox geeft recht op een ontspannende activiteit voor 2 personen naar keuze.','www.cadeaubox.be',39.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGCAD09-877410',4,'Weekend puur natuur','De cadeaubon in deze Cadeaubox geeft recht op een overnachting met ontbijt voor 2 personen in ��n van de geselecteerde hotels.','www.cadeaubox.be',79.9,'2017-02-28');
INSERT INTO bon VALUES (bon_seq.nextval,'LOGCAD09-877420',4,'Verwenweekend','De cadeaubon in deze Cadeaubox geeft recht op een verwenarrangement voor twee bestaande uit een overnachting in een tweepersoonskamer met ontbijt en een culinair diner of toegang tot een wellnesscentrum in ��n van de geselecteerde hotels of kastelen.','www.cadeaubox.be',219,'2017-02-28');


CREATE TABLE klant
(
   id 		smallint         primary key,
   naam         varchar(40)		not null,
   voornaam	varchar(20)	not null,
   telefoon	varchar(10),
   email		varchar(30)	not null
);

CREATE SEQUENCE klant_seq;

INSERT INTO klant VALUES (klant_seq.nextval, 'Beernaers','Joris','0478642151','jorisbeernaers@gmail.com');

CREATE TABLE bestelling
(
   id			smallint		primary key,
   bonID		smallint		not null,
   klantID		smallint		not null,
   aantal		smallint		not null,
   datum		date			not null,
   foreign key (bonID) references bon(id),
   foreign key (klantID) references klant(id)
);

CREATE SEQUENCE bestelling_seq;

INSERT INTO bestelling VALUES (bestelling_seq.nextval, 1, 1, 1, '2016-04-29');
