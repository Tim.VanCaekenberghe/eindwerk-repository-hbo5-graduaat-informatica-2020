/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package access;

import beans.Lener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author TVCBL87
 */
public class LenerDao {
    private final String url, login, password;
    
    public LenerDao(String url, String password, String login, String driver) throws ClassNotFoundException{
        Class.forName(driver);
        this.url = url;
        this.password = password;
        this.login = login;
    }
    
    public Lener findlener(){
        Lener lener = null;
       try{
           Connection connection = DriverManager.getConnection(url, login, password);
           Statement statement = connection.createStatement();
           ResultSet resultSet = statement.executeQuery("SELECT * FROM LENER where nr = 1");
           if(resultSet.next()){
               lener = createObjectFromSql(lener, resultSet);
              
    
              
       }
       }catch(SQLException e){
           e.getMessage();
       }return lener;
    }
    
    
           private Lener createObjectFromSql(Lener lener, ResultSet resultSet){
               try {
               lener = new Lener();
               lener.setNr(resultSet.getInt("NR"));
               lener.setLenernaam(resultSet.getString("LENERNAAM"));
               lener.setStraat(resultSet.getString("STRAAT"));
               lener.setHuisnr(resultSet.getString("HUISNR"));
               lener.setBusnr(resultSet.getString("BUSNR"));
               lener.setPostcode(resultSet.getInt("POSTCODE"));
               lener.setGemeente(resultSet.getString("GEMEENTE"));
               lener.setTelefoon(resultSet.getInt("TELEFOON"));
               lener.setEmail(resultSet.getString("EMAIL"));
            
            
        
    }catch(SQLException e){
        e.getMessage();
    }
        return lener;
           }
}

