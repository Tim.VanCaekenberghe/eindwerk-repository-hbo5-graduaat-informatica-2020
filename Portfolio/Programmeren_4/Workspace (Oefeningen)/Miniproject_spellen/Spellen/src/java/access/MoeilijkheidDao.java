/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package access;
import beans.Moeilijkheid;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
/**
 *
 * @author TVCBL87
 */
public class MoeilijkheidDao {
     private final String url, login, password; 

    
     
     
     // Constructor met 4 parameters
    public MoeilijkheidDao(String url, String login, String password, String driver) throws ClassNotFoundException {
        Class.forName(driver);
        this.url = url;
        this.login = login;
        this.password = password;
    }

    public Moeilijkheid getMoeilijkheid() {
        Moeilijkheid moeilijkheid = null;
        try (
                Connection connection = DriverManager.getConnection(url, login, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM moeilijkheid where NR = 1");) {
            if (resultSet.next()) {
                moeilijkheid = new Moeilijkheid();
                moeilijkheid.setNr(resultSet.getInt("NR"));
                moeilijkheid.setMoeilijkheidnaam(resultSet.getString("MOEILIJKHEIDNAAM"));
           
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return moeilijkheid;
    }

}

