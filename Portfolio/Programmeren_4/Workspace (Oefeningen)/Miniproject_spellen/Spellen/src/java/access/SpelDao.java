/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package access;

import beans.Soort;
import beans.Moeilijkheid;

import beans.Spel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TVCBL87
 */
public class SpelDao {
    private final String url, login, password;
    
    public SpelDao(String url, String password, String login, String driver) throws ClassNotFoundException{
        Class.forName(driver);
        this.url = url;
        this.password = password;
        this.login = login;
    }
    
    public Spel findSpelByNR(){
        Spel spel = null;
       try{
           Connection connection = DriverManager.getConnection(url, login, password);
           Statement statement = connection.createStatement();
           ResultSet resultSet = statement.executeQuery("SELECT * FROM spel where nr = 5");
           if(resultSet.next()){
               spel = createObjectFromSql(spel, resultSet);
              
    
              
       }
       }catch(SQLException e){
           e.getMessage();
       }return spel;
    }
    public ArrayList<Spel> findSpellenByName(String match){
        ArrayList<Spel> spellen = new ArrayList<>();
        Spel spel = null;
        try{
           Connection connection = DriverManager.getConnection(url, login, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM spel where naam like ?");

            statement.setString(1,"%"+ match + "%");
            ResultSet resultset = statement.executeQuery();
           while(resultset.next()){
               do{
               spellen.add(createObjectFromSql(spel, resultset));
        }while(resultset.next());
    }
        }catch (SQLException ex) {
           ex.getMessage();
        }return spellen;
    }
    
        
    
           private Spel createObjectFromSql(Spel spel, ResultSet resultSet){
               try {
               spel = new Spel();
               spel.setNr(resultSet.getInt("NR"));
               spel.setNaam(resultSet.getString("NAAM"));
               spel.setUitgever(resultSet.getString("UITGEVER"));
               spel.setAuteur(resultSet.getString("AUTEUR"));
               spel.setJaar_uitgifte(resultSet.getInt("JAAR_UITGIFTE"));
               spel.setLeeftijd(resultSet.getString("LEEFTIJD"));
               spel.setMin_spelers(resultSet.getInt("MIN_SPELERS"));
               spel.setMax_spelers(resultSet.getInt("MAX_SPELERS"));
               spel.setSoortnr(resultSet.getInt("SOORTNR"));
               spel.setSpeelduur(resultSet.getString("SPEELDUUR"));
               spel.setMoeilijkheidnr(resultSet.getInt("MOEILIJKHEIDNR"));
               spel.setPrijs(resultSet.getFloat("PRIJS"));
               spel.setAfbeelding(resultSet.getString("AFBEELDING"));
            
        
    }catch(SQLException e){
        e.getMessage();
    }
        return spel;
           }
}
