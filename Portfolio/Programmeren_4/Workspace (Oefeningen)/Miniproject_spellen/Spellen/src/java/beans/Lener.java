/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author TVCBL87
 */
public class Lener {
    private int nr;
    private String lenernaam;
    private String straat;
    private String huisnr;
    private String busnr;
    private int postcode;
    private String gemeente;
    private int telefoon;
    private String email;

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getLenernaam() {
        return lenernaam;
    }

    public void setLenernaam(String lenernaam) {
        this.lenernaam = lenernaam;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    public String getHuisnr() {
        return huisnr;
    }

    public void setHuisnr(String huisnr) {
        this.huisnr = huisnr;
    }

    public String getBusnr() {
        return busnr;
    }

    public void setBusnr(String busnr) {
        this.busnr = busnr;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getGemeente() {
        return gemeente;
    }

    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }

    public int getTelefoon() {
        return telefoon;
    }

    public void setTelefoon(int telefoon) {
        this.telefoon = telefoon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Lener{" + "nr=" + nr + ", lenernaam=" + lenernaam + ", straat=" + straat + ", huisnr=" + huisnr + ", busnr=" + busnr + ", postcode=" + postcode + ", gemeente=" + gemeente + ", telefoon=" + telefoon + ", email=" + email + '}';
    }
    
}
