/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author TVCBL87
 */
public class Soort {
    private int id;
    private String soortnaam;

    public Soort() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSoortnaam() {
        return soortnaam;
    }

    public void setSoortnaam(String soortnaam) {
        this.soortnaam = soortnaam;
    }

    @Override
    public String toString() {
        return "Soort{" + "id=" + id + ", soortnaam=" + soortnaam + '}';
    }
    
    
}
