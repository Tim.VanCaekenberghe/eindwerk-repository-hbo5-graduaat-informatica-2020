/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author TVCBL87
 */
public class Spel {
    private int nr;
    private String naam;
    private String uitgever;
    private String auteur;
    private int jaar_uitgifte;
    private String leeftijd;
    private int min_spelers;
    private int max_spelers;
    private int soortnr;
    private String speelduur;
    private int moeilijkheidnr;
    private float prijs;
    private String afbeelding;

    public Spel() {
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getUitgever() {
        return uitgever;
    }

    public void setUitgever(String uitgever) {
        this.uitgever = uitgever;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public int getJaar_uitgifte() {
        return jaar_uitgifte;
    }

    public void setJaar_uitgifte(int jaar_uitgifte) {
        this.jaar_uitgifte = jaar_uitgifte;
    }

    public String getLeeftijd() {
        return leeftijd;
    }

    public void setLeeftijd(String leeftijd) {
        this.leeftijd = leeftijd;
    }

    public int getMin_spelers() {
        return min_spelers;
    }

    public void setMin_spelers(int min_spelers) {
        this.min_spelers = min_spelers;
    }

    public int getMax_spelers() {
        return max_spelers;
    }

    public void setMax_spelers(int max_spelers) {
        this.max_spelers = max_spelers;
    }

    public int getSoortnr() {
        return soortnr;
    }

    public void setSoortnr(int soortnr) {
        this.soortnr = soortnr;
    }

    public String getSpeelduur() {
        return speelduur;
    }

    public void setSpeelduur(String speelduur) {
        this.speelduur = speelduur;
    }

    public int getMoeilijkheidnr() {
        return moeilijkheidnr;
    }

    public void setMoeilijkheidnr(int moeilijkheidnr) {
        this.moeilijkheidnr = moeilijkheidnr;
    }

    public float getPrijs() {
        return prijs;
    }

    public void setPrijs(float prijs) {
        this.prijs = prijs;
    }

    public String getAfbeelding() {
        return afbeelding;
    }

    public void setAfbeelding(String afbeelding) {
        this.afbeelding = afbeelding;
    }

    @Override
    public String toString() {
        return "Spel{" + "nr=" + nr + ", naam=" + naam + ", uitgever=" + uitgever + ", auteur=" + auteur + ", jaar_uitgifte=" + jaar_uitgifte + ", leeftijd=" + leeftijd + ", min_spelers=" + min_spelers + ", max_spelers=" + max_spelers + ", soortnr=" + soortnr + ", speelduur=" + speelduur + ", moeilijkheidnr=" + moeilijkheidnr + ", prijs=" + prijs + ", afbeelding=" + afbeelding + '}';
    }
    
    
}
