/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import access.LenerDao;
import access.SoortDao;
import access.SpelDao;
import beans.Lener;
import beans.Soort;
import beans.Spel;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TVCBL87
 */
@WebServlet(name = "ManageServlet", urlPatterns = {"/ManageServlet"}, initParams = {
    @WebInitParam(name = "url", value = "jdbc:oracle:thin:@localhost:1521:XE")
    ,
		@WebInitParam(name = "login", value = "admin")
    ,
		@WebInitParam(name = "password", value = "sql")
    ,
@WebInitParam(name = "driver", value = "oracle.jdbc.driver.OracleDriver")})
public class ManageServlet extends HttpServlet {

    private SoortDao soortdao = null;
    private SpelDao spelDao = null;
    private LenerDao lenerDao = null;

    public ManageServlet() {
        super();
    }

    @Override
    public void init() throws ServletException {
        try {
            String url = getInitParameter("url");
            String login = getInitParameter("login");
            String password = getInitParameter("password");
            String driver = getInitParameter("driver");

            if (soortdao == null) {
                soortdao = new SoortDao(url, login, password, driver);
            }
            if (spelDao == null) {
                spelDao = new SpelDao(url, password, login, driver);
            }
            if (lenerDao == null) {
                lenerDao = new LenerDao(url, password, login, driver);
            }
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
     try{
        RequestDispatcher rd = null;
        String gekozen = request.getParameter("gekozen");

        if (request.getParameter("spelsoortknop") != null) {
            rd = toonEersteSoort(request, response);
        } else if (request.getParameter("vijfdespelknop") != null) {
            rd = toonVijfdeSpel(request, response);
        } else if (request.getParameter("spelkeuzeknop") != null) {
            String keuze = request.getParameter("spelnaarkeuze");
            rd = toonSpelOpNaam(keuze, request, response);
        } else if (gekozen != null) {
            if (gekozen.equals("lener")) {
                Lener lener = lenerDao.findlener();
                request.setAttribute("lener", lener);
                rd = request.getRequestDispatcher("lener.jsp");
            }

        }
        rd.forward(request, response);

    }catch(SQLException e){
         RequestDispatcher rd = request.getRequestDispatcher("errorPage.jsp");
            if (e.getMessage().contains("no records")) {
                request.setAttribute("errormessage", e.getMessage());
            } else {
                request.setAttribute("errormessage", "if you see this page, that means an error has occurred, we're sorry.");
                request.setAttribute("devstacktrace", e);
            }
            rd.forward(request, response);

        }

    }

        
    
        private RequestDispatcher toonSpelOpNaam(String keuze, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("spel.jsp");
        ArrayList<Spel> spellen = spelDao.findSpellenByName(keuze);
            request.setAttribute("spellen", spellen);
            return rd;
        }
        
        private RequestDispatcher toonEersteSoort(HttpServletRequest request, HttpServletResponse response) throws SQLException{
        RequestDispatcher rd = request.getRequestDispatcher("soort.jsp");
        Soort soort = soortdao.getSoort();
        request.setAttribute("soort", soort);
        return rd;
        }
        
         private RequestDispatcher toonVijfdeSpel(HttpServletRequest request, HttpServletResponse response) throws SQLException{
        RequestDispatcher rd = request.getRequestDispatcher("spel.jsp");
        Spel spel = spelDao.findSpelByNR();
        request.setAttribute("spel", spel);
        return rd;
         }
            
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ManageServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ManageServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
