<%-- 
    Document   : error
    Created on : Feb 11, 2020, 12:42:55 PM
    Author     : TVCBL87
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
       <h1>A Error</h1>
<%
    String errormessage = (String) request.getAttribute("errormessage");
    Exception e = (Exception) request.getAttribute("devstacktrace");
%>
<p><%=errormessage%>
</p>
<p><a href="index.jsp">Hoofdmenu</a></p>
<hr>
<script type="text/javascript">
    <%if(e != null){
        StackTraceElement[] stacktrace = e.getStackTrace();
    for(StackTraceElement element : stacktrace){
    String s = element.toString();%>
    console.log("<%=s%>");
    <%}%>
    console.log("   ");
    console.log("<%=e.getMessage()%>")
    <%}%>
</script>

    </body>
</html>
