<%-- 
    Document   : soort
    Created on : Feb 5, 2020, 11:46:22 AM
    Author     : TVCBL87
--%>

<%@page import="beans.Soort"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Soort Page</title>
    </head>
    <body>
        <%Soort soort = (Soort) request.getAttribute("soort");%>
        <div id="headercontainer">
            <div id="header">
                <h1>Soort</h1>
            </div>
        </div>
        <div id="content">
            
            <p>
                <label for="soortnaam">Naam:</label>
                <input type="text" name="naam" value="<%=soort.getSoortnaam()%>" id="soortnaam"/>
            </p>
            <a href="index.jsp"> Terug naar homepage </a>
        
        </div>
    </body>
</html>
