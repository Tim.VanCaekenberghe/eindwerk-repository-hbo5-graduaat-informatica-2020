CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artikelNr` bigint(20) DEFAULT NULL,
  `eancode` varchar(255) DEFAULT NULL,
  `merk` varchar(255) DEFAULT NULL,
  `naam` varchar(255) DEFAULT NULL,
  `omschrijvingNL` varchar(255) DEFAULT NULL,
  `omschrijvingEN` varchar(255) DEFAULT NULL,
  `categorieNL` varchar(255) DEFAULT NULL,
  `categorieEn` varchar(255) DEFAULT NULL,
  `groep1` varchar(255) DEFAULT NULL,
  `groep2` varchar(255) DEFAULT NULL,
  `groep3` varchar(255) DEFAULT NULL,
  `prijs` double DEFAULT NULL,
  `aantal` varchar(255) DEFAULT NULL,
  `creatiedatum` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
