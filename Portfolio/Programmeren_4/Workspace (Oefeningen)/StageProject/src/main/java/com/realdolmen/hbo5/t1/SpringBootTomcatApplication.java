package com.realdolmen.hbo5.t1;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SpringBootTomcatApplication extends SpringBootServletInitializer {

}