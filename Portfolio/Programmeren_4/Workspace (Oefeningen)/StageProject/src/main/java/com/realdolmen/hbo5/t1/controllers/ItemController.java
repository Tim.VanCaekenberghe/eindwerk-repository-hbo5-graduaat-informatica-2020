package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.NoEntryFoundException;
import com.realdolmen.hbo5.t1.exceptions.NullPageException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ItemController {

    private ItemDao itemDao = new ItemDao();

    /**
     * gets a specific item by ID, please only use if you know if there's a item assosiated with a ID, not for end-users
     *
     * @param id (required) The ID of the product to find.
     * @return A JSON string containing the fetched result.
     */
    @GetMapping("/itemByID")
    public Item itemByID(@RequestParam(name = "id", required = true) int id) {
        return itemDao.findItemById(id);
    }

    /**
     * Get's ALL items from the database with a specific sorting applied to it
     *
     * @param page   the page you're on (default page 1)
     * @param amount amount of results you want (default 20)
     * @param sort   (the sort you want: 1 for name ASC, 2 for name DESC, 3 for price ASC, 4 for Price DESC)
     * @return A JSON String containing the fetched result.
     * @throws NoEntryFoundException
     */
    @GetMapping("/allItems")
    public List<Item> allItems(
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "amount", defaultValue = "20") int amount,
            @RequestParam(name = "sort", defaultValue = "1") int sort
    ) throws NoEntryFoundException {
        List<Item> items;

        switch (sort) {
            case 4:
                items = itemDao.allItemsOrderByPriceDesc(page, amount);
                break;
            case 3:
                items = itemDao.allItemsOrderByPriceAsc(page, amount);
                break;
            case 2:
                items = itemDao.allItemsOrderByNameDesc(page, amount);
                break;
            case 1:
            default:
                items = itemDao.allItemsOrderByNameAsc(page, amount);
                break;
        }
        return items;
    }

    /**
     *
     * This is the name or category search function, a bit wieldy, but it'll do for now.
     * @param name name to search on (default "")
     * @param category category to search on (default "")
     * @param page   the page you're on (default page 1)
     * @param amount amount of results you want (default 20)
     * @param sort   (the sort you want: 1 for name ASC, 2 for name DESC, 3 for price ASC, 4 for Price DESC)
     * @return A JSON String containing the fetched result.
     * @throws NoEntryFoundException
     * @throws NullPageException
     */
    @GetMapping("/itemSearchName")
    public List<Item> ItemsSearchName(
            @RequestParam(name = "name", defaultValue = "") String name,
            @RequestParam(name = "category", defaultValue = "") String category,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "amount", defaultValue = "20") int amount,
            @RequestParam(name = "sort", defaultValue = "1") int sort
    ) throws NoEntryFoundException, NullPageException {
        List<Item> items;
        boolean sortBoolean = false;
        if (sort % 2 == 1) {
            sortBoolean = true;
        }
        String sorting;
        switch (sort) {
            case 3:
            case 4:
                sorting = "prijs";
                break;
            case 1:
            case 2:
            default:
                sorting = "naam";
                break;
        }

        if (!name.isEmpty()) {
            items = itemDao.advancedSearch(page, amount, name, "naam", sortBoolean, sorting);
        } else if (!category.isEmpty()) {
            items = itemDao.advancedSearch(page, amount, category, "categorieNL", sortBoolean, sorting);
        } else throw new NullPageException("BAD REQUEST: needs Name OR Category to search on");

        return items;
    }

}
