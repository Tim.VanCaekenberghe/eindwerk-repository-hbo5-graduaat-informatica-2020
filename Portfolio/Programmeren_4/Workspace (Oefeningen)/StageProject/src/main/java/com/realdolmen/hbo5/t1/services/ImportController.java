package com.realdolmen.hbo5.t1.services;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.exceptions.FileToBigException;
import com.realdolmen.hbo5.t1.exceptions.FileValidationException;
import com.realdolmen.hbo5.t1.utilities.validation.ImportValidation;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * this is a import helping class, this uses itemDOA and CSVReader to read a csv file and push it to the database
 */
public class ImportController {

    private CSVReader importer;
    private String path;
    private ItemDao itemDao;

    public ImportController(String path) {
        this.path = path;
        itemDao = new ItemDao();
    }

    public ImportController() {
        itemDao = new ItemDao();
    }

    //somehow mockito didnt do this itself, so i had to place this here
    ImportController(ItemDao itemDao) {
        this.itemDao = itemDao;
    }


    private List<Item> read(File file) throws IOException, FileToBigException, ParseException, ColumnNameNotOkException, FileValidationException {
        if (importer == null) {
            importer = new CSVReader(file);
        }

        if (ImportValidation.endsOnCSV(file) && ImportValidation.contentCSV(file)) {
            if (ImportValidation.contentCSVToBig(file)) {
                return importer.read();
            } else {
                throw new FileToBigException("Het bestand dat u probeerde te importeren is te groot om zonder problemen te runnen");
            }

        } else {
            throw new FileValidationException("Het bestand is niet herkent als CSV bestand of bevat geen inhoud.");
        }

    }


    public int getAddedItems() {
        if (importer != null) {
            return importer.getAddedItems();
        } else {
            return 0;
        }
    }


    public List<String> getImportExceptions() {
        if (importer != null) {
            return importer.getImportExceptions();
        } else {
            return new ArrayList<>();
        }
    }


    public boolean importCSVToDB(String fullPath) throws IOException, FileValidationException, FileToBigException, ParseException, ColumnNameNotOkException {
        File file = new File(fullPath);
        return importCSVToDB(file);
    }

    public boolean importCSVToDB() throws ColumnNameNotOkException, FileValidationException, FileToBigException, ParseException, IOException {
        return importCSVToDB(this.path);
    }

    public boolean importCSVToDB(File file) throws ColumnNameNotOkException, FileValidationException, FileToBigException, ParseException, IOException {
        List<Item> items = read(file);
        return itemDao.saveToDB(items);
    }

    //TODO make this i guess place the picture in the correct place if we use pictures
    public String saveAFileToUserFolder(javax.servlet.http.Part part /*the response you get from request.getPart("name")*/) throws IOException {
        //make sure the folder images exist.
        String home = new JFileChooser().getFileSystemView().getDefaultDirectory().toString();
        File directory = new File(home + "/" + "StockProgram" + "/" + "Images");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        //create a new file in the location
        String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        File file = new File(directory, fileName);
        try (InputStream input = part.getInputStream()) {
            Files.copy(input, file.toPath());
        }
        return fileName; //filename to be send to the database
    }
}
