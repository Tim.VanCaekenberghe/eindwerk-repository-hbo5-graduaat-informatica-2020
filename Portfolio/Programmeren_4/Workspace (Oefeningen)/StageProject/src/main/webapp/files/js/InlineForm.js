function inlineFormSetup(elementQuerySelector){
    let elements = document.querySelectorAll(elementQuerySelector);
    console.log("elements: " + elements);
    for(let i = 0; i < elements.length; i++){
        let e = elements[i];
        e.addEventListener('click', function(){
            loadInputAndHideActual(e);
        });
    }
}

function loadInputAndHideActual(element){
    let parent = element.parentElement;
    let inp = document.createElement("input");
    inp.setAttribute("type", "text");
    inp.setAttribute("name", element.getAttribute('data-input-name'));
    inp.setAttribute("value", element.innerText);
    inp.setAttribute("autofocus", "true");
    element.style.display = "none";
    parent.appendChild(inp);

    inp.addEventListener("focusout", function(){
       saveContentsToDatabase(this, element.getAttribute("data-id"));
    });
}

async function saveContentsToDatabase(input, id){
    let formData = new FormData();
    formData.append("method", "editField");
    formData.append("field", input.getAttribute("name"));
    formData.append("value", input.value);
    formData.append("id", id);

    let config = {
        method: "POST",
        body: formData
    };

    let req = await fetch("async", config);
    let resp = await req.text();

    responseHandler(resp, input);
}

function responseHandler(resp, input){
    let response = resp.trim();
    if(response == "0"){
        closeInputAndShowNormalContent(input);
    } else if (response == "1"){
        console.error("An error happened on the server");
    } else {
        console.error(resp);
    }
}

function closeInputAndShowNormalContent(input){
    let parent = input.parentElement;
    let element = parent.getElementsByTagName('p')[0];
    element.innerText = input.value;
    element.setAttribute('title', input.value);
    input.remove();
    element.style.display = "inline-block";
}