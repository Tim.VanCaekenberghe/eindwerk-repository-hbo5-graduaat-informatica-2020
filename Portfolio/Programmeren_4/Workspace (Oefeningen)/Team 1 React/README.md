#Introductie

Deae applicatie is gemaakt geweest door de groep NullPointerNinjas met als groepsleden (Maxime Esnol, Jeroen Beulens, Stijn Hesters & Arne Wauters).
Met onze groep kregen we de opdracht om een applicatie te maken (voor data in te lezen en weg te schrijven van een csv bestand), dit gebruik makend van het AGILE framework.

#Aan de slag
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
######1.	Hoe installeer je de applicatie 
De applicatie kan geïnstalleerd worden door eerst een map te maken in waar de gebruiker de code wilt installeren.
Als dit gedaan is kan de gebruiker gebruik maken van Git Bash, dan schrijft men 
- $ git init
- $ git clone https://tfs.realdolmen.com/tfs/ManagedServices/HBO5/_git/Team%201
Als beide stappen gedaan zijn en er geen errormeldingen verschijnen heeft men de data locaal staan.

De andere manier voor het project te installeren is het zip bestand te downloaden van Teams in de groep Team - HBO5 Java 2019-2020 Team 1 onder de files.
Als men het zip bestand heeft gedownload extract men de data en plaats het in de gewenste locatie.
En als laatste stap opent men de data met een jdk omgeving.

######2.	Software dependencies

jdk java 8

######3.	Nieuwste releases
Voor de laatste versies van de code moet men pullen van de "origin/develop".

######4.	API referenties
Voor documentatie van de code kan er gekeken worden in de commentaar van de code.

#Build en Test
Voor de code te laten draaien gaat men in de code naar 

src => main => com.realdolmen.hbo5.t1 => applcation => App, in de klas App druk je op het groene pijltje run code.

Voor de tests te zien die geschreven zijn in de file onder 
src => test drukt men dan op de package java met de rechter muis en selecteer Run tests. 

#Bijdrage
Voor mee te werken aan de code / verbetering voor te stellen kan er gemaild worden naar jeroen.beulens@realdolmen.com