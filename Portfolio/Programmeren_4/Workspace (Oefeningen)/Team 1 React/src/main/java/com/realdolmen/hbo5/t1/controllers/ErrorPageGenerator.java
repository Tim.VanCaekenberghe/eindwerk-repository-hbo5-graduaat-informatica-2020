package com.realdolmen.hbo5.t1.controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ErrorPageGenerator {
    private static final Logger LOGGER = Logger.getLogger("ErrorLogger");

    public ErrorPageGenerator() throws IOException {
        String home = new JFileChooser().getFileSystemView().getDefaultDirectory().toString();
        File directory = new File(home + "/" + "StockProgram");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String filename = "errors_" + LocalDate.now() + ".log";
        File errorFile = new File(home + "/" + "StockProgram" + "/" + filename);
        LOGGER.addHandler(new FileHandler(errorFile.getPath()));
    }

    public void gotoErrorPage(HttpServletRequest request, HttpServletResponse response, Exception e) throws ServletException, IOException {
        LOGGER.log(Level.WARNING,"User encountered an exception: .",e);
        request.setAttribute("errormessage",e.getMessage());
        request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request,response);
    }
}
