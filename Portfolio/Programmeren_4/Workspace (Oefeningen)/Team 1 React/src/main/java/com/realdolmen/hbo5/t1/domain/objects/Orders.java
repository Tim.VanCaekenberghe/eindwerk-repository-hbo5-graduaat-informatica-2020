package com.realdolmen.hbo5.t1.domain.objects;

import com.realdolmen.hbo5.t1.domain.objects.database.aid.classes.AbstractDatabaseObject;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;


@Entity
@Table(name = "orders", schema = "stockprogram")
@NamedQueries({
        @NamedQuery(name = "User.findall", query = "SELECT  i  from User i"),
        @NamedQuery(name = "User.findbyid", query = "select i from  User i where i.pk = :id")
})
public class Orders extends AbstractDatabaseObject {

    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "orderdate")
    Date creatieDatum;

    @ManyToOne
    @Access(AccessType.FIELD)
    @JoinColumn(name = "user")
    User user;

    @OneToMany
    @Access(AccessType.FIELD)
    @JoinTable(name = "order_products",
            joinColumns = @JoinColumn(
                    name = "product_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "order_id", referencedColumnName = "id")
    )
    List<Item> itemList;

    public Orders() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public void setBirthDate(LocalDate creatieDatum) {
        this.creatieDatum = Date.valueOf(creatieDatum);
    }

    public void setBirthDate(String date) {
        setBirthDate(LocalDate.parse(date));
    }

    public LocalDate getBirthdate() {
        if (creatieDatum != null) {
            return creatieDatum.toLocalDate();
        } else {
            return null;
        }
    }
}
