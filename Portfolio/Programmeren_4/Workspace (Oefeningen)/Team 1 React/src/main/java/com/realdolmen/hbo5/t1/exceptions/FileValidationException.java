package com.realdolmen.hbo5.t1.exceptions;

public class FileValidationException extends Exception {
    public FileValidationException(String message) {
        super(message);
    }
}
