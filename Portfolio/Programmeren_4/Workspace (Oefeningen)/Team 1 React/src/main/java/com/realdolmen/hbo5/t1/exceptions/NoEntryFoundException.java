package com.realdolmen.hbo5.t1.exceptions;

public class NoEntryFoundException extends Exception {
    public NoEntryFoundException(){
        super("No entry for your query was found");
    }
    public NoEntryFoundException(String message){
        super(message);
    }
}
