<%--
  Created by IntelliJ IDEA.
  User: TVCBL87
  Date: 10/03/2020
  Time: 10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Error Page</title>
    <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="files/css/stockprogram.css"/>
</head>
<body>
<c:import url="/WEB-INF/imports/nav.jsp"/>
<c:out value="${errormessage}"/>
<p><a href="employee_index.jsp">Terug naar hoofdmenu</a></p>
</body>
</html>
