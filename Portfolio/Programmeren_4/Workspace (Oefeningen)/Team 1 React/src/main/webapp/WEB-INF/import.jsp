<%--
  Created by IntelliJ IDEA.
  User: TVCBL87
  Date: 10/03/2020
  Time: 11:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Importeer CSV</title>
    <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="files/css/stockprogram.css"/>
</head>
<body>
<c:import url="/WEB-INF/imports/nav.jsp"/>
<div class="container">
    <form action="import" method="post" enctype="multipart/form-data">
    <h1>
        <span class="lp" data-message="import_your_csv"></span>
    </h1>
        <input type="file" name="file" class="form-control-file" id="file" accept=".csv">
    <button type="submit" name="upload" class="btn btn-primary">
        <span class="lp" data-message="upload_csv"></span>
    </button>
    </form>
</div>


<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="files/js/bootstrap.min.js"></script>
<script src="files/js/LangParser.js"></script>
<script>
    $(document).ready(function () {
        setupLangParser("files/lang/");
    });
</script>
</body>
</html>
