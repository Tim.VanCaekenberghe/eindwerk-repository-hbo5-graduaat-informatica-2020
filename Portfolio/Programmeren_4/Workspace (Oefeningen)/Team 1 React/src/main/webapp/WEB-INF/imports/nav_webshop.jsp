<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="websiteName" value="http://localhost:8080/project1_war_exploded/"/>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<c:out value="${websiteName}"/>">
                    <span class="lp" data-message="portal"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<c:out value="${websiteName}"/>webshop">
                    <span class="lp" data-message="startpage_webshop"></span>
                </a>
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="/">StockProgram</a>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="<c:out value="${websiteName}"/>cart">
                    <span class="lp" data-message="shopping_cart"></span>
                </a>
            </li>
            <li class="nav-item">
                <select id="languageSelect" onchange="updateLanguage(this)">
                    <option value="nl">Nederlands</option>
                    <option value="fr">fran&#231;ais</option>
                    <option value="en">English</option>
                </select>
            </li>
        </ul>
    </div>
</nav>