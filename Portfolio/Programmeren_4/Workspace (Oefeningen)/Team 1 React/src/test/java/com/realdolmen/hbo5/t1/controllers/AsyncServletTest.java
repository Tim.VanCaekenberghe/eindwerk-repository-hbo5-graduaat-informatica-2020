package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.NoEntryFoundException;
import com.realdolmen.hbo5.t1.helper.ServletTestHelper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AsyncServletTest extends ServletTestHelper {

    @Mock
    ItemDao itemDao;
    @InjectMocks
    AsyncServlet servlet;


    @Test
    public void TestGetAllItemsAndForWardNoORDER() throws ServletException, IOException, NoEntryFoundException {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("method")).thenReturn("showAll");
        when(request.getParameter("page")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("20");
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.findAllItems(anyInt(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request, response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
    }

    @Test
    public void TestGetAllItemsAndForWardORDERPriceAsc() throws ServletException, IOException, NoEntryFoundException {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("method")).thenReturn("showAll");
        when(request.getParameter("page")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("20");
        when(request.getParameter("order")).thenReturn("orderPriceAsc");
        when(itemDao.getItemCount()).thenReturn(55L);
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.allItemsOrderByPriceAsc(anyInt(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request, response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
    }

    @Test
    public void TestGetAllItemsAndForWardORDERPriceDesc() throws ServletException, IOException, NoEntryFoundException {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("method")).thenReturn("showAll");
        when(request.getParameter("page")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("20");
        when(request.getParameter("order")).thenReturn("orderPriceDesc");
        when(itemDao.getItemCount()).thenReturn(55L);
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.allItemsOrderByPriceDesc(anyInt(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request, response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
    }

    @Test
    public void TestGetAllItemsAndForWardORDERAmountASC() throws ServletException, IOException, NoEntryFoundException {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("method")).thenReturn("showAll");
        when(request.getParameter("order")).thenReturn("orderAmountAsc");
        when(request.getParameter("page")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("20");
        when(itemDao.getItemCount()).thenReturn(55L);
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.allItemsOrderByAmountAsc(anyInt(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request, response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
    }

    @Test
    public void TestGetAllItemsAndForWardORDERAmountDESC() throws ServletException, IOException, NoEntryFoundException {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("method")).thenReturn("showAll");
        when(request.getParameter("page")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("20");
        when(request.getParameter("order")).thenReturn("orderAmountDesc");
        when(itemDao.getItemCount()).thenReturn(55L);
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.allItemsOrderByAmountDesc(anyInt(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request, response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
    }

    @Test
    public void TestGetAllItemsAndForWardORDERNameASC() throws ServletException, IOException, NoEntryFoundException {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("method")).thenReturn("showAll");
        when(request.getParameter("page")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("20");
        when(request.getParameter("order")).thenReturn("orderNameAsc");
        when(itemDao.getItemCount()).thenReturn(55L);
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.allItemsOrderByNameAsc(anyInt(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request, response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
    }

    @Test
    public void TestGetAllItemsAndForWardORDERNameDESC() throws ServletException, IOException, NoEntryFoundException {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("method")).thenReturn("showAll");
        when(request.getParameter("page")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("20");
        when(request.getParameter("order")).thenReturn("orderNameDesc");
        when(itemDao.getItemCount()).thenReturn(55L);
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.allItemsOrderByNameDesc(anyInt(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request, response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
    }

    @Test
    public void TestGetAllItemsAndForWardORDERDefault() throws ServletException, IOException, NoEntryFoundException {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("method")).thenReturn("showAll");
        when(request.getParameter("page")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("20");
        when(request.getParameter("order")).thenReturn("maximeschrijftgeentestenvoorzijncodeenikbenpissedhierdoor");
        when(itemDao.getItemCount()).thenReturn(55L);
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.findAllItems(anyInt(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request, response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
    }

    @Test
    public void testSearchItemsByName() throws NoEntryFoundException, ServletException, IOException {
        when(request.getParameter("method")).thenReturn("q");
        when(request.getParameter("q")).thenReturn("nullpointerninja");
        when(request.getParameter("page")).thenReturn("1");
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.findItemsByName(anyString(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request,response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
        assertEquals("search",(String) request.getAttribute("method"));
    }

    @Test
    public void testSearchItemsByNumber() throws NoEntryFoundException, ServletException, IOException {
        when(request.getParameter("method")).thenReturn("q");
        when(request.getParameter("q")).thenReturn("123");
        when(request.getParameter("page")).thenReturn("1");
        List<Item> itemsinput = new ArrayList<>();
        Item item = new Item();
        item.setNaam("nullpointerninja");
        for (int i = 0; i < 55; i++) {
            itemsinput.add(item);
        }
        when(itemDao.findItemByArtikelNR(anyLong(), anyInt())).thenReturn(itemsinput);
        servlet.doGet(request,response);
        List<Item> items = (List<Item>) request.getAttribute("items");
        assertArrayEquals(itemsinput.toArray(), items.toArray());
        assertEquals("search",(String) request.getAttribute("method"));
    }
}