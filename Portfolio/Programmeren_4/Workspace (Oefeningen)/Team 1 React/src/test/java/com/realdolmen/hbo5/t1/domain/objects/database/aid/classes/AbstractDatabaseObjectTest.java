package com.realdolmen.hbo5.t1.domain.objects.database.aid.classes;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.*;

import static com.realdolmen.hbo5.t1.helper.AssertAnnotations.assertField;
import static com.realdolmen.hbo5.t1.helper.AssertAnnotations.assertType;
import static com.realdolmen.hbo5.t1.helper.ReflectTool.getFieldAnnotation;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AbstractDatabaseObjectTest {
    @Test
    public void AbstractDBObjectIsMappedSuperClass() {
        assertType(AbstractDatabaseObject.class, MappedSuperclass.class);
    }

    @Test
    public void verifyPkIsAFieldWithCorrectAnnotations() {
        assertField(AbstractDatabaseObject.class, "pk", Id.class, Basic.class, Access.class, Column.class, GeneratedValue.class);
        GeneratedValue generatedValue = getFieldAnnotation(AbstractDatabaseObject.class, "pk", GeneratedValue.class);
        Access access = getFieldAnnotation(AbstractDatabaseObject.class, "pk", Access.class);
        Basic basic = getFieldAnnotation(AbstractDatabaseObject.class, "pk",Basic.class);
        Column column = getFieldAnnotation(AbstractDatabaseObject.class, "pk",Column.class);
        assertFalse(basic.optional());
        assertEquals("id",column.name());
        assertTrue(column.unique());
        assertEquals(AccessType.FIELD, access.value());
        assertEquals(GenerationType.IDENTITY, generatedValue.strategy());
    }
}