package com.realdolmen.hbo5.t1.services;

import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.services.CSVReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@ExtendWith(MockitoExtension.class)
public class CSVReaderTest {
    Path resourceDirectory = Paths.get("src", "test", "resources");
    String defaultDirectory = resourceDirectory.toFile().getAbsolutePath();

    @Test
    public void testRead() {
        Assertions.assertDoesNotThrow(() -> {
            CSVReader reader = new CSVReader(defaultDirectory + "\\expected.csv");
            reader.read();
        });
    }

    @Test
    public void testTrowAssertNoCSVReader() {
        Assertions.assertThrows(ColumnNameNotOkException.class, () -> {
            CSVReader reader = new CSVReader(defaultDirectory + "\\nohead.csv");
            reader.read();
        });
    }
}