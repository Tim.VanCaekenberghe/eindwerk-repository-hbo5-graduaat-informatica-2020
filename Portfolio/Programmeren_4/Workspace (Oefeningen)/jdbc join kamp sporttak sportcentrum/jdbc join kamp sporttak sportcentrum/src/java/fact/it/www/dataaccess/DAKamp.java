package fact.it.www.dataaccess;

import fact.it.www.beans.Kamp;
import fact.it.www.beans.Sportcentrum;
import fact.it.www.beans.Sporttak;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAKamp {

    private final String url, login, password;

    public DAKamp(String url, String login, String password, String driver) throws ClassNotFoundException{
        Class.forName(driver);
        this.url = url;
        this.login = login;
        this.password = password;
    }

       
    public Kamp getKamp() {
        Kamp kamp = null;
        try (Connection connection = DriverManager.getConnection(url, login, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select * from kamp inner join sporttak on kamp.sporttak_id = sporttak.id inner join sportcentrum on kamp.centrum_id = sportcentrum.id where kamp.id = 1");)
        {       if (resultSet.next()) {
                kamp = new Kamp();
                kamp.setId(resultSet.getInt("id"));
                kamp.setCentrum_id(resultSet.getInt("centrum_id"));
                kamp.setKampnaam(resultSet.getString("kampnaam"));
                kamp.setBegindatum(resultSet.getDate("begindatum"));
                kamp.setEinddatum(resultSet.getDate("einddatum"));
                kamp.setSporttak_id(resultSet.getInt("sporttak_id"));
                kamp.setMin_gebjaar(resultSet.getInt("min_gebjaar"));
                kamp.setMax_gebjaar(resultSet.getInt("max_gebjaar"));
                kamp.setPrijs(resultSet.getDouble("prijs"));
                kamp.setAantal_plaatsen(resultSet.getInt("aantal_plaatsen"));
                // gerelateerde sporttak aanmaken
                Sporttak sporttak = new Sporttak();
                sporttak.setId(resultSet.getInt("sporttak_id"));
                sporttak.setOmschrijving(resultSet.getString("omschrijving"));
                // sporttak toevoegen aan het kamp
                kamp.setSporttak(sporttak);
                // gerelateerd sportcentrum aanmaken
                Sportcentrum sportcentrum = new Sportcentrum();
                sportcentrum.setId(resultSet.getInt("centrum_id"));
                sportcentrum.setCentrumnaam(resultSet.getString("centrumnaam"));
                sportcentrum.setStraat(resultSet.getString("straat"));
                sportcentrum.setHuisnummer(resultSet.getString("huisnummer"));
                sportcentrum.setPostcode(resultSet.getInt("postcode"));
                sportcentrum.setWoonplaats(resultSet.getString("woonplaats"));
                sportcentrum.setTelefoon(resultSet.getString("telefoon"));
                // sportcentrum toevoegen aan het kamp
                kamp.setSportcentrum(sportcentrum);
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return kamp;
    }
}
