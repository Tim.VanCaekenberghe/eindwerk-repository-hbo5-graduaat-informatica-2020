
<%@page import="fact.it.www.beans.Kamp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Kamp details</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
        <%Kamp kamp = (Kamp) request.getAttribute("kamp");%>
        <div id="headercontainer">
            <div id="header">
                <h1>Sport je jong!</h1>
            </div>
        </div>
        <div id="content">
            <h1><%=kamp.getKampnaam() %></h1>                   
            <p> Het kamp "<%=kamp.getKampnaam()%>" loopt van <%=kamp.getBegindatum()%> tot <%=kamp.getEinddatum()%> </p>
            <p> Geschikt voor deelnemers geboren vanaf <%=kamp.getMin_gebjaar()%> tot <%=kamp.getMax_gebjaar()%> </p>
            <p> De prijs is <%=kamp.getPrijs()%> euro en er zijn <%=kamp.getAantal_plaatsen()%> plaatsen beschikbaar. </p>
            <p> Dit kamp behoort tot de sporttak "<%= kamp.getSporttak().getOmschrijving()%>" en wordt georganiseerd door volgend sportcentrum: </p>
            <p> <%= kamp.getSportcentrum().getCentrumnaam()%><br/>
                <%= kamp.getSportcentrum().getStraat()%> <%= kamp.getSportcentrum().getHuisnummer()%><br/>
                <%= kamp.getSportcentrum().getPostcode()%> <%= kamp.getSportcentrum().getWoonplaats()%><br/>
                <%= kamp.getSportcentrum().getTelefoon()%>            
            </p>
            
               
            <p>  <a href="index.jsp">Terug naar beginpagina</a></p>
        </div>
    </body>
</html>
