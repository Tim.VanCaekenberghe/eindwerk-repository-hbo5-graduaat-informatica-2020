
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
    <title>Sportvakanties startpagina</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <div id="headercontainer">
      <div id="header">
        <h1>Sport je jong!</h1>
      </div>
    </div>
    <div id="content">
        <form action="ManageServlet" >
            <p>Welk sportcentrum wil je bekijken? <input type="text" name="sportcentrumid" size="5" /></p>
          <p><input type="submit" value="Sportcentrum bekijken" name="sportcentrumKnop" /></p>
          <p>Klik <a href="ManageServlet?lijsttonen=aangeklikt" >hier</a> als je uit een combobox wil kiezen</p>
        </form>
      </div>

  </body>
</html>
