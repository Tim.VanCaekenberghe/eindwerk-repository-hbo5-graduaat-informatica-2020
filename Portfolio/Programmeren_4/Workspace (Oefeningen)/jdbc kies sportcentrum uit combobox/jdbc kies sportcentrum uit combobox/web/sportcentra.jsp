
<%@page import="java.util.ArrayList"%>
<%@page import="fact.it.www.beans.Sportcentrum"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sportcentra</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>

        <div id="headercontainer">
            <div id="header">
                <h1>Sport je jong!</h1>
            </div>
        </div>
        <div id="content">
            <%ArrayList<Sportcentrum> sportcentra = (ArrayList<Sportcentrum>) request.getAttribute("sportcentra");%>
            <h1>Lijst van sportcentra</h1> 
            <form action="ManageServlet" >
                <select name ="sportcentrumid">
                    <option value="0">---kies hier het sportcentrum---</option>
                    <% for (Sportcentrum sportcentrum : sportcentra) {%>
                    <option value ="<%=sportcentrum.getId()%>"><%=sportcentrum.getCentrumnaam()%> </option>
                    <%}%>
                </select>
                <input type="submit" value="Verder" name="sportcentrumKnop"/>
            </form>
            <p><a href="index.jsp">Terug naar beginpagina</a></p>
        </div>
    </body>
</html>
