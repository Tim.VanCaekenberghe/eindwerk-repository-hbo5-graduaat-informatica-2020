drop database sports;
create database sports;

use sports;
create table participant(
id            int         primary key auto_increment,
participantname varchar(30)  not null,
firstname      varchar(20)  not null,
birthdate date         not null,
sex      char(1)      not null,
street        varchar(30)  not null,
housenumber    varchar(6) not null,
bus           int,
postalcode      int not null,
city    varchar(30),
phone      varchar(10),
mobile           varchar(15),
email         varchar(40)
);



insert into participant values (1,'Faes','Annick', '1999/01/22' ,'V', 'Verloren hoek',159,null,3920,'Lommel','011/545855','0498/741147','Annick_Faes@hotmail.com');
insert into participant values (2,'Bareta','Wouter','2001/10/15' ,'M','Scheyerveld',26,null,3640,'Kinrooi','011/658414','0498/657198    ','Wouter_Bareta@belgacom.be');
insert into participant values (3,'De Ridder','Stijn','2003/06/03' ,'M','Veestreet',2,null,2360,'Oud-Turnhout','014/417352','0497/743970    ','stijn.de.ridder@pandora.be');
insert into participant values (4,'Neefs','Robby','2002/08/03' ,'M','Paalseweg',125,null,3980,'Tessenderlo','013/661471','0498/458970    ','Robby_Neefs@hotmail.com');
insert into participant values (5,'Stinckens','An','1999/04/14' ,'V','Dongenblok',10,null,2431,'Laakdal-Veerle','014/849503','0474/852258','An_Stinckens@hotmail.com');
insert into participant values (6,'Van Loon','Sara','1999/05/03' ,'V','Nieuwstreet',83,null,2430,'Laakdal','013/667641','0496/965478','Sara_Van Loon@hotmail.com');
insert into participant values (7,'Cortens','Bert','2002/10/03' ,'M','Gerkenbergstreet',68,null,3960,'Bree','089/484418','0486/632563','Bert_Cortens@hotmail.com');
insert into participant values (8,'Wellens','Kristof','2004/07/03' ,'M','Ernest Claesstreet',59,1,2200,'Herentals','014/231214','0486/545421','Kristof_Wellens@hotmail.com');
insert into participant values (9,'Waegenaere','Brenda','2000/02/05' ,'V','Nieuwland',21,null,2440,'Geel','014/582222','0498/657198    ','Brenda_Waegenaere@hotmail.com');
insert into participant values (10,'Smekens','Leo','1998/12/03' ,'M','Sint-Annastreet',34,null,3050,'Oud-Heverlee','016/447550','0499/434843    ','Leo_Smekens@belgacom.be');
insert into participant values (11,'Wuyts','Ellen','2005/03/21' ,'V','Eikenlei',46,null,2280,'Grobbendonk','014/516976','0484/963253','Ellen_Wuyts@telenet.be');
insert into participant values (12,'Van Deun','Steven','1998/07/20' ,'M','Vieille-Montagnestreet',5,null,2400,'Mol','014/811714','0473/520140','steven.van.deun@kke.be');
insert into participant values (13,'Geelen','Kevin','2002/05/13' ,'M','Hommelbeek',18,null,3980,'Tessenderlo','013/661250','0498/962014','Kevin_Geelen@belgacom.be');
insert into participant values (14,'Heiremans','Liesbeth','2000/10/03' ,'V','Molenweg',4,null,3294,'Diest-Molenstede','013/323325','0497/458700','Liesbeth_Heiremans@hotmail.com');
insert into participant values (15,'Wellens','Dennis','2004/05/03' ,'M','Tulpenstreet',32,null,2430,'Laakdal-Eindhout','014/869839','0496/560214','Dennis.Wellens@telenet.be');
insert into participant values (16,'Stes','Koen','1997/07/03' ,'M','Boerenkrijglaan',193,null,2260,'Westerlo','014/541122','0472/577563    ','Koen_Stes@hotmail.com');
insert into participant values (17,'De Weert','Dave','1998/07/03' ,'M','Brooseinde',42,1,2360,'Oud-Turnhout','014/450188','0473/289005    ','davedw@hotmail.com');
insert into participant values (18,'Vanlommel','Rik','1996/12/03' ,'M','Halensebaan',126,null,3290,'Diest','013/313874','0473/505072    ','rik_Vanlommel@telenet.be');
insert into participant values (19,'Bertels','Maarten','1998/10/07' ,'M','Diestsebaan',55,null,2230,'Herselt','014/546447','0473/599357    ','Maarten_Bertels@hotmail.com');
insert into participant values (20,'Brughmans','Sarah','2000/07/03' ,'V','Langstreet',9,null,2260,'Westerlo','014/540269','0473/850410','Sarah_Brughmans@hotmail.com');
insert into participant values (21,'Goetschalckx','Stefaan','1998/09/03' ,'M','Wielewaalstreet',34,null,2440,'Geel','014/587664','0474/630219','Stefaan_Goetschalckx@telenet.be');
insert into participant values (22,'Verellen','Lies','2004/07/16' ,'V','Bochtstreet',12,null,3920,'Lommel','011/551836','0477/600724    ','Lies_Verellen@hotmail.com');
insert into participant values (23,'Hofkens','Peter','2002/02/03' ,'M','Merellaan',9,null,2460,'Kasterlee','014/854522','0479/683063    ','Peter_Hofkens@hotmail.com');
insert into participant values (24,'Moerkens','Sophie','1999/11/08' ,'V','Brigandstreet',12,null,2490,'Balen','014/651425','0486/325920    ','Sophie_Moerkens@hotmail.com' );
insert into participant values (25,'Van Deuren','Jan','1990/07/03' ,'M','Pijnboomstreet',3,null,2480,'Dessel','014/377458','0493/505215    ','jvdeuren@hotmail.com');
insert into participant values (26,'Van Goethem','Kim','1997/09/03' ,'V','Hoebenschot',7,null,2460,'Kasterlee','014/552933','0487/852014','kim.van.goethem@telene.be' );
insert into participant values (27,'Smeyers','Wendy','1996/12/09' ,'V','St-Carolusstreet',21,null,2400,'Mol','011/542732','0496/234512','Wendy_Smeyers@belgacom.be' );
insert into participant values (28,'Hendrickx','Joris','1997/03/17' ,'M','Lammerdries',17,null,2250,'Olen','014/217531','0495/112442','Joris_Hendrickx@hotmail.com' );
insert into participant values (29,'Hendrix','Stijn','2004/05/10' ,'M','Koppeleershoeven',5,null,2440,'Geel','014/584908','0474/447870    ','Stijn_Hendrix@hotmail.com');
insert into participant values (30,'Geelen','Bart','2002/11/18' ,'M','Krakelaarsveld',31,null,2200,'Herentals','014/210922','0474/606101    ','Bart_Geelen@belgacom.be' );
insert into participant values (31,'Verlinden','Lucas','1999/09/03' ,'M','Balen-Neetlaan',14,null,2400,'Mol','014/813970','0474/897987    ','Lucas_Verlinden@hotmail.com' );
insert into participant values (32,'Verhulst','Matthias','2000/06/11' ,'M','Dwarsstreet',21,null,3945,'Ham','013/666147','0477/195962    ','Matthias_Verhulst@telenet.be' );
insert into participant values (33,'Adriaenssen','Kristof','1998/01/19' ,'M','Klaproosstreet',12,null,2340,'Beerse','014/714956','0473/874112','Kristof_Adriaenssen@belgacom.be' );
insert into participant values (34,'Borders','Mathias','2001/04/12' ,'M','Vlasbloemlaan',7,null,2070,'Zwijndrecht','013/672356','0474/875219','Mathias_Borders@hotmail.com' );
insert into participant values (35,'Bellen','Tom','2003/09/03' ,'M','Molsekiezel',171,null,3920,'Lommel','011/545589','0499/632630','Tom_Bellen@hotmail.com');
insert into participant values (36,'Meir','Carl','1998/03/03' ,'M','Vliegplein',87,null,2180,'Ekeren (Antwerpen)','011/545589','0498/542325','Carl_Meir@telenet.be' );
insert into participant values (37,'Van Opstal','Dieter','2004/11/03' ,'M','Kleine Puttingbaan',90,null,2560,'Nijlen-Kessel','03/4884531','0494/167920    ','dietertje@hotmail.com' );
insert into participant values (38,'Vlekken','Kevin','2001/08/13' ,'M','Rijsberg 2',2,null,2490,'Balen','014/719963','0495/405085    ','Kevin_Vlekken@telenet.be' );
insert into participant values (39,'Waegenaere','Dave','1996/06/03' ,'M','Zavelstreet',6,null,2290,'Vorselaar','014/214578','0495/999386    ','Dave_Waegenaere@hotmail.com');
insert into participant values (40,'Lowette','Katelijne','2005/12/27' ,'V','Oostmalsebaan',86,null,2960,'Brecht','03/3130644','0496/074004    ','Katelijne_Lowette@belgacom.be' );
insert into participant values (41,'Van Beirendonck','Tim','2003/02/25' ,'M','Glazeniersstreet 18',18,null,2300,'Turnhout','014/425289','0496/877867    ','timvanbeiren@hotmail.com' );
insert into participant values (42,'De Ridder','Timothy','2001/08/11' ,'M','Gildenstreet',56,null,2470,'Retie','014/370775','0497/166550    ','timothy54@hotmail.com' );
insert into participant values (43,'Gijbels','Kristof','1999/04/04' ,'M','Heiveld',30,null,3980,'Tessenderlo','013/337743','0497/335588    ','Kristof_Gijbels@hotmailcom' );
insert into participant values (44,'Jacobs','Kevin','2003/11/03' ,'M','Schansstreet',11,null,2431,'Laakdal-Veerle','014/840204','0473/412365','Kevin_Jacobs@hotmail.com');
insert into participant values (45,'Theunis','Nicky','1998/11/03' ,'V','Hermelijn',18,null,2221,'Booischot','015/225953','0474/652322','Nicky_Theunis@hotmail.com');
insert into participant values (46,'Versmissen','Ken','1998/06/03' ,'M','Keirlandse Zillen',21,null,2400,'Mol','014/512930','0493/542122','Ken_Versmissen@hotmail.com');
insert into participant values (47,'Goris','Marc','1999/10/05' ,'M','Graatakker',131,7,2300,'Turnhout','014/417788','0495/784785','Marc_Goris@belgacom.be');
insert into participant values (48,'Tombeur','Isabelle','2001/01/23' ,'V','Geelsebaan',29,2,2460,'Kasterlee','014/852718','0477/986585','Isabelle_Tombeur@hotmail.com');
insert into participant values (49,'De Backer','Inge','2005/08/24' ,'V','Turnhoutsebaan',18,3,2480,'Dessel','014/372045','0477/252514','ingetje@hotmail.com' );
insert into participant values (50,'Gysen','Britt','1997/02/26' ,'V','Klaterstreet',19,null,2310,'Rijkevorsel','03/3142399','0493/981245','Britt_Gysen@telenet.be');



create table sportscenter(
id                int       not null auto_increment,
centername       varchar(50)  not null,
street            varchar(30)  not null,
housenumber        varchar(6) not null,
postalcode          int not null,
city        varchar(30),
phone          varchar(10),
primary key (id)
);


insert into sportscenter
values (1,'Herentals Netepark','Vorselaarsebaan',60,2200,'Herentals','014 859512');
insert into sportscenter
values (2,'Tongeren','Hasseltsebaan',4,3600,'Tongeren','011 300810');
insert into sportscenter
values (3,'Geraardsbergen','Stw op Gent',52,9500,'Geraardsbergen','09 8559510');
insert into sportscenter
values (4,'Nieuwpoort','Burgse Steenweg 9',52,8620,'Nieuwpoort','058 235891');


create table sportsbranch(
id               int          not null auto_increment,
omschrijving varchar(50)  not null,
primary key (id)
);



insert into sportsbranch values (1,'Omnisport');
insert into sportsbranch values (2,'Tennis');
insert into sportsbranch values (3,'Athletics');
insert into sportsbranch values (4,'Gymnastics');
insert into sportsbranch values (5,'Sailing');
insert into sportsbranch values (6,'Basketball');
insert into sportsbranch values (7,'Volleyball');
insert into sportsbranch values (8,'Horseriding');
insert into sportsbranch values (9,'Badminton');
insert into sportsbranch values (10,'Football');
insert into sportsbranch values (11,'Kajaking');
insert into sportsbranch values (12,'Judo');
insert into sportsbranch values (13,'Ropeskipping');
insert into sportsbranch values (14,'Dancing');
insert into sportsbranch values (15,'Windsurfing');

create table camp(
id              int       not null auto_increment,
center_id      int not null,
campname            varchar(50) not null,
startdate      date not null,
enddate       date not null,
sportsbranch_id     int not null,
min_birthyear     int not null,
max_birthyear     int not null,
price           numeric(8, 2) not null,
available_spots numeric(4, 0) not null,
primary key (id),
foreign key (center_id) references sportscenter (id),
foreign key (sportsbranch_id) references sportsbranch (id)
);



insert into camp values (1,1,'Horse riding and grooming for beginners','2018-08-13','2018-08-17',8,1999,2003,287,10);
insert into camp values (2,1,'Horse riding and grooming for beginners','2018-07-30','2018-08-03',8,1999,2003,287,10);
insert into camp values (3,2,'Athletics: seven camp','2018-08-20','2018-08-24',3,2000,2004,207,12);
insert into camp values (4,4,'Kajak en jeugdzeilen','2018-07-02','2018-07-06',11,2000,2004,223,12);
insert into camp values (5,4,'Kajak: A-brevet','2018-08-20','2018-08-24',11,1998,2001,223,12);
insert into camp values (6,2,'Athletics: five-camp','2018-07-09','2018-07-13',3,1998,2002,207,15);
insert into camp values (7,2,'Athletics: five-camp','2018-08-06','2018-08-10',3,1998,2002,207,15);
insert into camp values (8,4,'Sailing with 4.20','2018-08-13','2018-08-17',5,1998,2002,223,15);
insert into camp values (9,4,'Sailing with 4.20','2018-07-09','2018-07-13',5,1998,2002,223,15);
insert into camp values (10,1,'All-in-one','2018-07-02','2018-07-06',1,1995,1998,203,20);
insert into camp values (11,1,'All-in-one','2018-08-13','2018-08-17',1,1995,1998,203,20);
insert into camp values (12,2,'Basket: teener','2018-07-23','2018-07-27',6,1998,2002,203,20);
insert into camp values (13,3,'Beachsport','2018-08-20','2018-08-24',7,2000,2004,207,20);
insert into camp values (14,1,'Gymnastics: acrogym','2018-07-02','2018-07-06',4,2000,2004,203,20);
insert into camp values (15,1,'Gymnastics: acrogym','2018-07-16','2018-07-20',4,1998,2002,203,20);
insert into camp values (16,1,'Gymnastics: acrogym','2018-08-06','2018-08-10',4,1995,2000,207,20);
insert into camp values (17,1,'Gymnastics: trampoline and tumbling','2018-07-23','2018-07-27',4,1998,2002,203,20);
insert into camp values (18,1,'Gymnastics: trampoline and tumbling','2018-08-13','2018-08-17',4,1995,2000,207,20);
insert into camp values (19,4,'Rowing and youth sailing','2018-07-02','2018-07-06',5,2000,2004,207,20);
insert into camp values (20,4,'Rowing and youth sailing','2018-07-30','2018-08-03',5,2000,2004,207,20);
insert into camp values (21,1,'Pentathlon and omnisport','2018-07-09','2018-07-13',1,2000,2004,203,20);
insert into camp values (22,3,'Football','2018-07-02','2018-07-06',10,2000,2004,203,20);
insert into camp values (23,3,'Football','2018-07-16','2018-07-20',10,2000,2004,203,20);
insert into camp values (24,3,'Football','2018-07-23','2018-07-27',10,1998,2001,203,20);
insert into camp values (25,1,'Football','2018-08-20','2018-08-24',10,1998,2001,203,20);
insert into camp values (26,3,'Football','2018-07-30','2018-08-03',10,1995,1998,203,20);
insert into camp values (27,1,'Football','2018-08-13','2018-08-17',10,1995,1998,203,20);
insert into camp values (28,3,'Volleyball and beachvolley','2018-07-09','2018-07-13',7,1998,2002,207,20);
insert into camp values (29,3,'Volleyball and beachvolley','2018-08-06','2018-08-10',7,1998,2002,207,20);
insert into camp values (30,4,'Windsurfing for beginners','2018-07-02','2018-07-06',15,1998,2002,227,20);
insert into camp values (31,4,'Windsurfing for beginners','2018-08-13','2018-08-17',15,1994,1998,227,20);
insert into camp values (32,4,'Windsurfen: evolutie 1','2018-07-30','2018-08-03',15,1998,2002,227,20);
insert into camp values (33,4,'Windsurfen: evolutie 1','2018-07-16','2018-07-20',15,1994,1998,227,20);
insert into camp values (34,4,'Windsurfen: evolutie 1','2018-07-23','2018-07-27',15,1994,1998,227,20);
insert into camp values (35,4,'Sailing (optimist)','2018-08-06','2018-08-10',5,1998,2002,223,20);
insert into camp values (36,4,'Sailing (optimist)','2018-08-20','2018-08-24',5,2000,2004,223,20);
insert into camp values (37,1,'Badminton en omnisport','2018-07-23','2018-07-27',9,2000,2004,203,25);
insert into camp values (38,1,'Badminton en omnisport','2018-08-06','2018-08-10',9,1998,2001,203,25);
insert into camp values (39,2,'Basket: junior pro','2018-07-16','2018-07-20',6,1998,2002,203,25);
insert into camp values (40,2,'Basketbal en omnisport','2018-07-09','2018-07-13',6,2000,2004,203,25);
insert into camp values (41,2,'Judo voor gevorderden','2018-07-23','2018-07-27',12,1998,2001,207,25);
insert into camp values (42,3,'Tennis ','2018-07-16','2018-07-20',2,2000,2004,223,25);
insert into camp values (43,3,'Tennis ','2018-07-30','2018-08-03',2,1998,2002,223,25);
insert into camp values (44,2,'Dans: funky dance','2018-08-20','2018-08-24',14,1995,1998,203,30);
insert into camp values (45,2,'Dans: hiphop','2018-07-16','2018-07-20',14,1998,2002,203,30);
insert into camp values (46,2,'Dans: hiphop','2018-07-30','2018-08-03',14,1995,1998,203,30);
insert into camp values (47,3,'Ropeskipping en dansatelier','2018-07-09','2018-07-13',13,1998,2002,203,30);
insert into camp values (48,3,'Ropeskipping en dansatelier','2018-08-06','2018-08-10',13,2000,2004,203,30);



create table registration(
participant_id int  not null,
camp_id  int  not null,
regisrationdate  date,
primary key (participant_id, camp_id),
foreign key (participant_id) references participant (id),
foreign key (camp_id) references camp (id)
);

insert into registration values (12, 1, '2018-04-07');
insert into registration values (1, 4, '2018-04-07');
insert into registration values (32, 31, '2018-04-15');
insert into registration values (13, 17, '2018-04-24');
insert into registration values (14, 26, '2018-04-17');
insert into registration values (5, 11, '2018-04-18');
insert into registration values (26, 24, '2018-03-07');
insert into registration values (50, 12, '2018-04-07');
insert into registration values (33, 19, '2018-04-07');
insert into registration values (19, 30, '2018-04-15');
insert into registration values (47, 35, '2018-03-24');
insert into registration values (14, 45, '2018-04-17');
insert into registration values (44, 19, '2018-04-18');
insert into registration values (39, 22, '2018-03-07');
