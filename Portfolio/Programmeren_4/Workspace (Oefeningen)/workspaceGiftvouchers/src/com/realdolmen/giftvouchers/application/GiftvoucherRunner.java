package com.realdolmen.giftvouchers.application;

import com.realdolmen.giftvouchers.dao.ClientDao;
import com.realdolmen.giftvouchers.dao.OrdersDao;
import com.realdolmen.giftvouchers.dao.ThemeDao;
import com.realdolmen.giftvouchers.dao.VoucherDao;
import com.realdolmen.giftvouchers.domain.Client;
import com.realdolmen.giftvouchers.domain.Order;
import com.realdolmen.giftvouchers.domain.Theme;
import com.realdolmen.giftvouchers.domain.Voucher;

import java.sql.SQLException;
import java.util.ArrayList;

public class GiftvoucherRunner {
    public static void main(String[] args) {
        ClientDao clientDao = new ClientDao();
            ArrayList<Client> clients = clientDao.getAllClients();
            for(Client client: clients){
                System.out.println(client.toString());
            }
        OrdersDao ordersDao = new OrdersDao();
            ArrayList<Order> orders = ordersDao.getAllOrders();
            for(Order order: orders){
                System.out.println(order.toString());
            }

        ThemeDao themeDao = new ThemeDao();
            ArrayList<Theme> themes = themeDao.getAllThemes();
            for(Theme theme : themes) {
                System.out.println(theme.toString());
            }

        VoucherDao voucherDao = new VoucherDao();
            ArrayList<Voucher> vouchers = voucherDao.getAllVouchers();
            for(Voucher voucher: vouchers){
                System.out.println(voucher.toString());
            }
    }




}
