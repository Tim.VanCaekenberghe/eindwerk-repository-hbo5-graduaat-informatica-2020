package com.realdolmen.giftvouchers.dao;

import com.realdolmen.giftvouchers.domain.Client;

import java.sql.*;
import java.util.ArrayList;

public class ClientDao {
    String URL = "jdbc:mysql://localhost:3306/giftvouchers";
    String login = "root";
    String password = "";


    public ArrayList<Client> getAllClients() {
        ArrayList<Client> clients = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, login, password);
             PreparedStatement preparedStatement = connection.prepareStatement("select * from client");) {
            ResultSet set = preparedStatement.executeQuery();
            while (set.next()) {
                Client client;
                client = setToData(set);
                clients.add(client);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clients;
    }
    private Client setToData(ResultSet set) throws SQLException {
        Client client = new Client();
        client.setClientId(set.getInt("id"));
        client.setName(set.getString("name"));
        client.setFirstname(set.getString("firstname"));
        client.setPhonenumber(set.getString("phonenumber"));
        client.setEmail(set.getString("email"));
        return client;
    }
}
