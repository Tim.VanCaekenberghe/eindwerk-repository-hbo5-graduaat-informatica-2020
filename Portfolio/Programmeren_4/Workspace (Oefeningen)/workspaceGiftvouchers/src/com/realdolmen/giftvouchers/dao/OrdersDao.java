package com.realdolmen.giftvouchers.dao;

import com.realdolmen.giftvouchers.domain.Client;
import com.realdolmen.giftvouchers.domain.Order;
import com.realdolmen.giftvouchers.domain.Theme;
import com.realdolmen.giftvouchers.domain.Voucher;

import java.sql.*;
import java.util.ArrayList;

public class OrdersDao {
    String URL = "jdbc:mysql://localhost:3306/giftvouchers";
    String login = "root";
    String password = "";

    public ArrayList<Order> getAllOrders() {
        ArrayList<Order> orders = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, login, password);
             PreparedStatement preparedStatement = connection.prepareStatement("select orders.id, voucherID, vouchernr, theme.theme, theme.id, clientID, quantity, date, client.name, firstname, phonenumber, email, client.id, themeID, voucher.name, information, supplier, price, valid_until from orders, client, voucher, theme where voucherID = voucher.id and clientID = client.id order by orders.id");)
        {
            ResultSet set = preparedStatement.executeQuery();
            while (set.next()) {

                Order order = new Order();
                order.setOrderId(set.getInt("orders.id"));
                Voucher voucher = new Voucher();
                voucher.setVoucherId(set.getInt("voucherID"));
                voucher.setInformation(set.getString("information"));
                voucher.setPrice(set.getDouble("price"));
                voucher.setSupplier(set.getString("supplier"));
                voucher.setValid_until(set.getDate("valid_until"));
                voucher.setVouchername(set.getString("voucher.name"));
                voucher.setVoucherId(set.getInt("voucherID"));
                voucher.setVouchernr(set.getString("vouchernr"));
                order.setVoucher(voucher);
                order.setDate(set.getDate("date"));
                order.setQuantity(set.getInt("quantity"));
                Client client = new Client();
                client.setEmail(set.getString("email"));
                client.setPhonenumber(set.getString("phonenumber"));
                client.setFirstname(set.getString("firstname"));
                client.setClientId(set.getInt("clientID"));
                client.setName(set.getString("client.name"));
                order.setClient(client);
                Theme theme = new Theme();
                theme.setThemeId(set.getInt("theme.id"));
                theme.setTheme(set.getString("theme.theme"));
                voucher.setTheme(theme);
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;

    }
}
