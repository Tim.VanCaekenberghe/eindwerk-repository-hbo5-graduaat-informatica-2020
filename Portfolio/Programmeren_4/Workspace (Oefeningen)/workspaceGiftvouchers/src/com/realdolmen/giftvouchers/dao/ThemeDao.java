package com.realdolmen.giftvouchers.dao;

import com.realdolmen.giftvouchers.domain.Client;
import com.realdolmen.giftvouchers.domain.Theme;

import java.sql.*;
import java.util.ArrayList;

public class ThemeDao {
    String URL = "jdbc:mysql://localhost:3306/giftvouchers";
    String login = "root";
    String password = "";


    public ArrayList<Theme> getAllThemes() {
        ArrayList<Theme> themes = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, login, password);
             PreparedStatement preparedStatement = connection.prepareStatement("select * from theme");) {
            ResultSet set = preparedStatement.executeQuery();
            while (set.next()) {
                Theme theme = new Theme();
                theme.setThemeId(set.getInt("id"));
                theme.setTheme(set.getString("theme"));
                themes.add(theme);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return themes;
    }
}
