package com.realdolmen.giftvouchers.dao;

import com.realdolmen.giftvouchers.domain.Order;
import com.realdolmen.giftvouchers.domain.Theme;
import com.realdolmen.giftvouchers.domain.Voucher;

import java.sql.*;
import java.util.ArrayList;

public class VoucherDao {
    String URL = "jdbc:mysql://localhost:3306/giftvouchers";
    String login = "root";
    String password = "";

    public ArrayList<Voucher> getAllVouchers() {
        ArrayList<Voucher> vouchers = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, login, password);
             PreparedStatement preparedStatement = connection.prepareStatement("select voucher.id, vouchernr, themeID, voucher.name, voucher.information, supplier, price, valid_until, theme.id, theme.theme  from voucher voucher, theme theme where voucher.themeID = theme.id order by voucher.id;");) {
            ResultSet set = preparedStatement.executeQuery();
            while (set.next()) {
                Voucher voucher = new Voucher();
                voucher.setVoucherId(set.getInt("voucher.id"));
                voucher.setVouchernr(set.getString("vouchernr"));
                voucher.setInformation(set.getString("information"));
                voucher.setPrice(set.getDouble("price"));
                voucher.setSupplier(set.getString("supplier"));
                voucher.setValid_until(set.getDate("valid_until"));
                voucher.setVouchername(set.getString("voucher.name"));
                Theme theme = new Theme();
                theme.setTheme(set.getString("theme.theme"));
                theme.setThemeId(set.getInt("theme.id"));
                voucher.setTheme(theme);
                vouchers.add(voucher);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return vouchers;
    }
}
