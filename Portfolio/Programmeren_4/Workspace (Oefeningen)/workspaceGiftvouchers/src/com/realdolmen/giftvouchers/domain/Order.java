package com.realdolmen.giftvouchers.domain;

import java.sql.Date;

public class Order {

    private int orderId;
    private Voucher voucher;
    private Client client;
    private int quantity;
    private Date date;

    public Order() {
    }

    public Order(int orderId, Voucher voucher, Client client, int quantity, Date date) {
        this.orderId = orderId;
        this.voucher = voucher;
        this.client = client;
        this.quantity = quantity;
        this.date = date;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orderId=" + orderId +
                ", voucher=" + voucher +
                ", client=" + client +
                ", quantity=" + quantity +
                ", date=" + date +
                '}';
    }
}
