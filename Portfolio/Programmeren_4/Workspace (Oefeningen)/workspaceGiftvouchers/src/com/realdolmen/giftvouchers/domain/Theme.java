package com.realdolmen.giftvouchers.domain;

public class Theme {
    private int themeId;
    private String theme;

    public Theme(){

    }
    public Theme(int themeId, String theme) {
        this.themeId = themeId;
        this.theme = theme;
    }

    public int getThemeId() {
        return themeId;
    }

    public void setThemeId(int themeId) {
        this.themeId = themeId;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
    @Override
    public String toString() {
        return "Theme{" +
                "themeId=" + themeId +
                ", theme='" + theme + '\'' +
                '}';
    }
}
