package com.realdolmen.giftvouchers.domain;

import java.sql.Date;

public class Voucher {
    private int voucherId;
    private String vouchernr;
    private Theme theme;
    private String vouchername;
    private String information;
    private String supplier;
    private double price;
    private Date valid_until;

    public Voucher() {
    }

    public Voucher(int voucherId, String vouchernr, Theme theme, String vouchername, String information, String supplier, double price, Date valid_until) {
        this.voucherId = voucherId;
        this.vouchernr = vouchernr;
        this.theme = theme;
        this.vouchername = vouchername;
        this.information = information;
        this.supplier = supplier;
        this.price = price;
        this.valid_until = valid_until;
    }

    public int getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(int voucherId) {
        this.voucherId = voucherId;
    }

    public String getVouchernr() {
        return vouchernr;
    }

    public void setVouchernr(String vouchernr) {
        this.vouchernr = vouchernr;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public String getVouchername() {
        return vouchername;
    }

    public void setVouchername(String vouchername) {
        this.vouchername = vouchername;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getValid_until() {
        return valid_until;
    }

    public void setValid_until(Date valid_until) {
        this.valid_until = valid_until;
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "voucherId=" + voucherId +
                ", vouchernr='" + vouchernr + '\'' +
                ", theme=" + theme +
                ", vouchername='" + vouchername + '\'' +
                ", information='" + information + '\'' +
                ", supplier='" + supplier + '\'' +
                ", price=" + price +
                ", valid_until=" + valid_until +
                '}';
    }
}
