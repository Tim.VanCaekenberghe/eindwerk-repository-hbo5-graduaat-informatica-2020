package com.realdolmen.giftvouchers.utilities;

import java.sql.*;

public class MySQLConnection{
        private static final String URL = "jdbc:mysql://localhost:3306/giftvouchers";
        private static final String USERNAME = "root";
        private static final String PASSWORD = "";

        private MySQLConnection() {

        }

        public static Connection createConnection(Connection connection) throws SQLException {
            if(connection == null || connection.isClosed()) {
                return DriverManager.getConnection(URL, USERNAME, PASSWORD);
            }else {
                return connection;
            }
        }


        public static void closeConnection(ResultSet resultset, Connection connection, PreparedStatement preparedStatement) {
            try {
                if(resultset != null) {
                    resultset.close();
                }
                preparedStatement.close();
                connection.close();
            }catch(SQLException e) {
            }
        }
    }


