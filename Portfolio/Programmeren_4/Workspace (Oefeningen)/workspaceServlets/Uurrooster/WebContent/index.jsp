<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Uurrooster</title>
</head>
<body>
<form action ="ManageServlet">
<p>
<label for="nummer"> Mijn studentennummer:</label>
<input type="text" id="nummer" name="gebruikersnummer"/>
</p>
<p>
<label for="klas">Mijn klasgroep:</label>
<select id="klas" name="klas">
<%for (int i = 1; i<=6; i++) { %>
<option value ="<%=i %>">1ITF<%=i %></option>
<%} %>
</select>
</p>
<p>Mijn voorkeur gaat op dit ogenblik uit naar:</p>
<%String [] keuzeArray = {"APP", "BIT", "EMDEV", "INFRA", "nog geen voorkeur"};
for(int j = 0; j<keuzeArray.length; j++) {%>
<p><input type="radio" name="keuze" value="<%=keuzeArray[j] %>" id="<%=j %>"/>
<label for="<%=j%>"><%=keuzeArray[j] %></label></p>
<%} %>
<p><input type="submit" value="OK"/></p>
</form>
</body>
</html>