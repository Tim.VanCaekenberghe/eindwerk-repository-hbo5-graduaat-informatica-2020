<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User List</title>
</head>
<body>
	<div>
		<h1>Super app!</h1>
	</div>

	<div>
		<div>
			<div>
				<h2>Users</h2>
			</div>

			<%
				List<String> names = (List<String>) request.getAttribute("userNames");
				if (names != null && !names.isEmpty()) {
					out.println("<ui>");
					for (String s : names) {
						out.println("<li>" + s + "</li>");
					}
					out.println("</ui>");
				} else
					out.println("<p>There are no users yet!</p>");
			%>
			<div>
				<a href="index.html"><button>Back to main</button></a>
			</div>
</body>
</html>