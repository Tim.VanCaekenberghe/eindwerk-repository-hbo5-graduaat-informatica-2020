package com.realdolmen.Domain;

public class Toetsenbord {

	private int serienummer;
	private String merk;
	private Boolean draadloos;
	private double prijs;
	
	public Toetsenbord() {
		this.serienummer = 987450;
		this.merk = "?";
		this.draadloos = true;
		
	}
	public Toetsenbord(String merk, double prijs) {
		this.merk = merk;
		this.prijs = prijs;
	}
	public Toetsenbord(int serienummer, String merk, Boolean draadloos, double prijs) {
		
		this.serienummer = serienummer;
		this.merk = merk;
		this.draadloos = draadloos;
		this.prijs = prijs;
	}
	public double getPrijsDollar(double omrekenen) {
		return prijs*omrekenen;
	}
	public String toString() {
		return "Het toetsenbord met serienummer " + serienummer + " kost � " + prijs;
	}
	public int getSerienummer() {
		return serienummer;
	}
	public void setSerienummer(int serienummer) {
		this.serienummer = serienummer;
	}
	public String getMerk() {
		return merk;
	}
	public void setMerk(String merk) {
		this.merk = merk;
	}
	public Boolean isDraadloos() {
		return draadloos;
	}
	public void setDraadloos(Boolean draadloos) {
		this.draadloos = draadloos;
	}
	public double getPrijs() {
		return prijs;
	}
	public void setPrijs(double prijs) {
		this.prijs = prijs;
	}
	
	
	
}
