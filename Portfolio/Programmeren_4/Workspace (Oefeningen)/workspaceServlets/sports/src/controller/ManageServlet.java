package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.SportsCenterDao;
import domain.Sportscenter;
import utitilities.MySQLConnection;


@WebServlet(name = "ManageServlet", urlPatterns = {"/ManageServlet"}, initParams = {
		@WebInitParam(name= "url", value ="jdbc:mysql://localhost:3306/sports"),
		@WebInitParam(name= "login", value ="root"),
		@WebInitParam(name= "password", value ="")})
		@WebInitParam(name="driver", value="com.mysql.jdbc.Driver")
		

public class ManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SportsCenterDao sportscenterdao;
	private Connection con;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManageServlet() {
        super();
    }
    public void init() throws ServletException{
    	String url = getInitParameter("url");
		String login = getInitParameter("login");
		String password = getInitParameter("password");
		String driver = getInitParameter("driver");
		if(sportscenterdao == null) {
			sportscenterdao = new SportsCenterDao(url, login, password);
		}
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	RequestDispatcher rd = request.getRequestDispatcher("sportcentrum.jsp");
    	Sportscenter sportscenter = sportscenterdao.getSportsCenter();
    	request.setAttribute("sportcentrum", sportscenter);
    	rd.forward(request, response);
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
