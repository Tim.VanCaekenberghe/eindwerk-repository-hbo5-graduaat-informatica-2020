package utitilities;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Driver;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class MySQLConnection {
		  private static final String URL = "jdbc:mysql://localhost:3306/sports";
		  private static final String USERNAME = "root";
		  private static final String PASSWORD = "";
		  private static final String DRIVER ="com.mysql.jdbc.Driver";
		  
		  private static Connection connection;
		  
		  private MySQLConnection() {
			  
		  }
		  
		  public static  Connection createConnection() throws SQLException {
			 
			  if(connection == null || connection.isClosed()) {
				  try {
				  Class.forName(DRIVER);
				  connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			  }catch(Exception ex) {
				  ex.printStackTrace();}
			  
			  }
				  return connection;
			  
		  }

		  
		 public static void closeConnection(ResultSet resultset, PreparedStatement preparedStatement) {
			 try {
				 if(resultset != null) {
					 resultset.close();
				 }
				 preparedStatement.close();
				 connection.close();
			 }catch(SQLException e) {
			 }
		 }
	}


