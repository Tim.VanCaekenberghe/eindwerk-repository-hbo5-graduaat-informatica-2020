<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Uurrooster</title>
</head>
<body>
	<h1>Uurrooster</h1>
	<form action="ManageServlet">
		<p>
			<label for="studentNummer">Mijn studentennummer: </label> 
			<input type="text" name="studentNummer" name="studentNummer" />
		</p>
		<p>
			<label for="klas">Mijn klasgroep: </label> <select name="klas"
				name="klas">
				<%
					for (int i = 0; i < 6; i++) {
				%>
				<option value="<%=i%>">1ITF<%=i%></option>
				<%
					}
				%>
			</select>
		</p>

		<p>
			<label for="voorkeur">Mijn voorkeur gaat op dit ogenblik uit
				naar: </label>
			<%
				String[] keuzes = { "APP", "BIT", "EMDEV", "INFRA", "Nog geen voorkeur" };

				for (int i = 0; i < keuzes.length; i++) {
			%>

		<p>
			<input type="radio" name="keuze" value="<%=keuzes[i]%>" id="<%=i%>" />
			<label for="<%=i%>"><%=keuzes[i]%></label>
		</p>
		<%
			}
		%>
		</p>

		<button type="submit">Bevestigen</button>

	</form>
</body>
</html>
