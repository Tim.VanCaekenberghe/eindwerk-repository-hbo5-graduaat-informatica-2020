package controller;



	import java.io.IOException;

	import javax.servlet.RequestDispatcher;
	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;


	@WebServlet(name = "ManageServlet", urlPatterns = {"/ManageServlet"})
	public class ManageServlet extends HttpServlet {

	    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	    	String docent = "?";
	    	String klas = request.getParameter("klas");
	    	int klasnummer = Integer.parseInt(klas);
	    	switch(klasnummer) {
	    	case 1: docent = "Kristine Mengelschoots"; break;
	    	case 2: case 4: docent = "Christel Maes"; break;
	    	case 3: case 5: docent = "Christine Smeets"; break;
	    	case 6: docent = "Els Peetermans"; break;
	    	}
	    
	        RequestDispatcher rd = request.getRequestDispatcher("antwoord.jsp");
	        request.setAttribute("docent", docent);
	        rd.forward(request, response);
	    }

	     
	    @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
		processRequest(request, response); 
	    }


	    @Override 
	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    	doGet(request, response);
	    }
	  
	}


