package com.realdolmen.sports.application;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import com.realdolmen.sports.dao.CampDao;
import com.realdolmen.sports.dao.SportsCenterDAO;
import com.realdolmen.sports.domain.Camp;
import com.realdolmen.sports.domain.Sportscenter;

public class SportsRunner {

	public static void main(String[] args) throws SQLException {
		/*
		 * SportsCenterDAO sportscenterdao = new SportsCenterDAO(); String userinput =
		 * null; Scanner sc = new Scanner(System.in);
		 * System.out.println("Please enter a name of a sportscenter"); userinput =
		 * sc.next(); Sportscenter sportscenter =
		 * sportscenterdao.getSportscenterOnName(userinput);
		 * 
		 * System.out.println(sportscenter.toString());
		 */
		CampDao campDao = new CampDao();
		ArrayList<Camp> camps = campDao.getAllCamps();
		for(Camp camp: camps) {
			System.out.println(camp.toString());
		}
		
	}
}
