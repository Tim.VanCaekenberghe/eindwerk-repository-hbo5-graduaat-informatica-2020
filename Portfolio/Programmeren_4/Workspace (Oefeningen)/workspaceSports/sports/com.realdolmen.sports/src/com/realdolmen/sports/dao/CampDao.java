package com.realdolmen.sports.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.realdolmen.sports.domain.Camp;
import com.realdolmen.sports.domain.Sportsbranch;
import com.realdolmen.sports.domain.Sportscenter;
import com.realdolmen.sports.utilities.MySQLConnection;

public class CampDao {
	String URL = "jdbc:mysql://localhost:3306/sports";
	String login = "root";
	String password = "";
	

	public ArrayList<Camp> getAllCamps() throws SQLException {
		ArrayList<Camp> camps = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(URL, login, password);
				PreparedStatement preparedStatement = connection.prepareStatement(
						"select camp.id, center_id, sportscenter.id, centername, campname, sportsbranch_id, sportsbranch.id, startdate, enddate, omschrijving, min_birthyear, max_birthyear, price, available_spots from camp, sportscenter, sportsbranch where center_id = sportscenter.id and sportsbranch_id = sportsbranch.id order by camp.id");) 
		{
			ResultSet set = preparedStatement.executeQuery();
			while (set.next()) {
				
				Camp camp = new Camp();
				Sportscenter sc = new Sportscenter();
				sc.setCentername(set.getString("centername"));
				sc.setcenter_id(set.getInt("center_id"));
				camp.setCamp_id(set.getInt("camp.id"));
				camp.setCenter(sc);
				camp.setStartdate(set.getDate("startdate"));
				camp.setEnddate(set.getDate("enddate"));
				Sportsbranch sb = new Sportsbranch();
				sb.setOmschrijving(set.getString("omschrijving"));
				camp.setSportsbranch(sb);
				camp.setMin_birthyear(set.getInt("min_birthyear"));
				camp.setMax_birthyear(set.getInt("max_birthyear"));
				camp.setAvailable_spots(set.getDouble("available_spots"));
				camp.setPrice(set.getDouble("price"));
				camp.setCampname(set.getString("campname"));

				
				camps.add(camp);
			}
		}return camps;

	}

	/*
	 * public ArrayList<Camp> findAllCamps(){ ArrayList<Camp> camps = new
	 * ArrayList<>(); try { connection =
	 * MySQLConnection.createConnection(connection); preparedStatement = connection.
	 * prepareStatement("SELECT camp.id, center_id, sportscenter.id, centername, campname, sportsbranch_id, sportsbranch.id, startdate, enddate, omschrijving, min_birthyear, max_birthyear, price, available_spots \r\n"
	 * + "from camp, sportscenter, sportsbranch\r\n" +
	 * "where center_id = sportscenter.id and sportsbranch_id = sportsbranch.id\r\n"
	 * + "order by camp.id"); resultset = preparedStatement.executeQuery(); while
	 * (resultset.next()) { Camp camp = new Camp(); Sportscenter sportscenter = new
	 * Sportscenter(); camp.setCamp_id(resultset.getInt("id"));
	 * camp.setCampname(resultset.getString("campname"));
	 * camp.setCenter(resultset.("centername")); } } catch (SQLException e) {
	 * e.printStackTrace(); } return sportscenters;
	 * 
	 * }
	 */

}
