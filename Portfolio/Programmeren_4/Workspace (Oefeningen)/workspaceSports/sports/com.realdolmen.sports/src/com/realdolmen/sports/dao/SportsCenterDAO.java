package com.realdolmen.sports.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.realdolmen.sports.domain.Sportscenter;
import com.realdolmen.sports.utilities.MySQLConnection;

public class SportsCenterDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet resultset;

	private Sportscenter setToData(ResultSet resultset) throws SQLException {
		Sportscenter sportscenter = new Sportscenter();
		sportscenter.setcenter_id(resultset.getInt("id"));
		sportscenter.setCentername(resultset.getString("centername"));
		sportscenter.setStreet(resultset.getString("street"));
		sportscenter.setHousenumber(resultset.getString("housenumber"));
		sportscenter.setPostalcode(resultset.getInt("postalcode"));
		sportscenter.setCity(resultset.getString("city"));
		sportscenter.setPhone(resultset.getString("phone"));
		return sportscenter;
	}

	public Sportscenter findById(int id) {
		Sportscenter sportscenter = new Sportscenter();
		try {
			connection = MySQLConnection.createConnection(connection);
			preparedStatement = connection.prepareStatement("select * from sportscenter where id = ?;");
			preparedStatement.setInt(1, id);
			resultset = preparedStatement.executeQuery();
			if (resultset.next()) {
				sportscenter = setToData(resultset);
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return sportscenter;
	}

	public ArrayList<Sportscenter> getSportscenters() {
		ArrayList<Sportscenter> sportscenters = new ArrayList<>();
		try {
			connection = MySQLConnection.createConnection(connection);
			preparedStatement = connection.prepareStatement("select * from sportscenter");
			resultset = preparedStatement.executeQuery();
			while (resultset.next()) {
				Sportscenter sportscenter = new Sportscenter();
				sportscenter = setToData(resultset);
				sportscenters.add(sportscenter);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sportscenters;

	}

	public Sportscenter getSportscenterOnName(String sportscentername) {
		Sportscenter sportscenter = new Sportscenter();
		try {
				connection = MySQLConnection.createConnection(connection);
				preparedStatement = connection.prepareStatement("select * from sportscenter where centername like ?");
				preparedStatement.setString(1, "%"+sportscentername+"%");
				resultset = preparedStatement.executeQuery();	
			if (resultset.next()) {
				sportscenter = setToData(resultset);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sportscenter;
	}
}
