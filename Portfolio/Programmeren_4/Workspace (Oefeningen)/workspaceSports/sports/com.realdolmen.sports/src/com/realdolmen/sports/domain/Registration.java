package com.realdolmen.sports.domain;

import java.sql.Date;

public class Registration {

	private Participant participant_id;
	private Camp camp;
	private Date regisrationdate;

	public Registration() {

	}

	public Registration(Participant participant_id, Camp camp, Date regisrationdate) {
		super();
		this.participant_id = participant_id;
		this.camp = camp;
		this.regisrationdate = regisrationdate;
	}

	public Participant getParticipant_id() {
		return participant_id;
	}

	public void setParticipant_id(Participant participant_id) {
		this.participant_id = participant_id;
	}

	public Camp getCamp() {
		return camp;
	}

	public void setCamp(Camp camp) {
		this.camp = camp;
	}

	public Date getRegisrationdate() {
		return regisrationdate;
	}

	public void setRegisrationdate(Date regisrationdate) {
		this.regisrationdate = regisrationdate;
	}

	@Override
	public String toString() {
		return "Registration [participant_id=" + participant_id + ", camp=" + camp + ", regisrationdate="
				+ regisrationdate + "]";
	}
	

}
