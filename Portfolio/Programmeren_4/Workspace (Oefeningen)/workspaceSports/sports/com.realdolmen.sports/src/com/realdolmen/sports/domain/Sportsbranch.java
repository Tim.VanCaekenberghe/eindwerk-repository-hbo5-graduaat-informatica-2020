package com.realdolmen.sports.domain;

public class Sportsbranch {

	private int sportsbranch_id;
	private String omschrijving;
	
	public Sportsbranch() {
		
	}
	public Sportsbranch(int sportsbranch_id, String omschrijving) {
		super();
		this.sportsbranch_id = sportsbranch_id;
		this.omschrijving = omschrijving;
	}
	public int getsportsbranch_id() {
		return sportsbranch_id;
	}
	public void setsportsbranch_id(int sportsbranch_id) {
		this.sportsbranch_id = sportsbranch_id;
	}
	public String getOmschrijving() {
		return omschrijving;
	}
	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}
	@Override
	public String toString() {
		return "Sportsbranch [sportsbranch_id=" + sportsbranch_id + ", omschrijving=" + omschrijving + "]";
	}
	
	
	
}
