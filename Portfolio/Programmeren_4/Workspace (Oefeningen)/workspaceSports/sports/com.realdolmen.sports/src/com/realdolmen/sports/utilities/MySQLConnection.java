package com.realdolmen.sports.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

public class MySQLConnection{
	  private static final String URL = "jdbc:mysql://localhost:3306/sports";
	  private static final String USERNAME = "root";
	  private static final String PASSWORD = "";
	  
	  private MySQLConnection() {
		  
	  }
	  
	  public static Connection createConnection(Connection connection) throws SQLException {
		  if(connection == null || connection.isClosed()) {
			  return DriverManager.getConnection(URL, USERNAME, PASSWORD);
		  }else {
			  return connection;
		  }
	  }

	  
	 public static void closeConnection(ResultSet resultset, Connection connection, PreparedStatement preparedStatement) {
		 try {
			 if(resultset != null) {
				 resultset.close();
			 }
			 preparedStatement.close();
			 connection.close();
		 }catch(SQLException e) {
		 }
	 }
}
