import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Home from './screens/Home';
import Colors from "./constants/Colors";

export default function App() {
  return (
    <View style={styles.container}>
      <Home/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.secondary
  }
});
