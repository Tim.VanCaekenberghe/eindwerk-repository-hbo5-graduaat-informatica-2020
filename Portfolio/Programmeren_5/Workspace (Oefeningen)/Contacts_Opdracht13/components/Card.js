import React from 'react'
import {StyleSheet, View} from 'react-native'
import Colors from "../constants/Colors";

const Card = props =>{
    return(
    <View style={{...styles.card, ...props.style}}>
        {props.children}
    </View>
    )
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: Colors.primary,
        borderRadius: 20,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        marginVertical: 30
    }
})
export default Card;