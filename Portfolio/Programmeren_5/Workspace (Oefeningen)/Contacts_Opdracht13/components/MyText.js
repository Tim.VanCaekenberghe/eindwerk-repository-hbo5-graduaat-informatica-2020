import React from 'react'
import {StyleSheet, View, Text} from 'react-native'
import Colors from "../constants/Colors";

const MyText = props =>{
    return(
        <Text style={styles.text}>
            {props.children}
        </Text>
    )
}

const styles = StyleSheet.create({
    text: {
        marginTop: 15,
        color: Colors.secondary,
        fontSize: 15,
        marginRight: 10,
        fontFamily: 'spartan'
    }
})
export default MyText;