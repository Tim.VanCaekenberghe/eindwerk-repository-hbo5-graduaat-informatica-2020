export default {
    primary: "#FF5964",
    secondary: "#ffe6e7",
    accent: "white",
    header: "#ff1a29"
};