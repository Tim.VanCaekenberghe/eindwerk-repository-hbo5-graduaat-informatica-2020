import React, {useState} from 'react';
import {View, Image, StyleSheet, ScrollView, Picker} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyText from "../components/MyText";
import MyInput from "../components/MyInput";
import Header from "../components/Header";
import MyDatePicker from "../components/MyDatePicker";
import MyPicker from "../components/MyPicker";
import { CheckBox } from 'react-native-elements'

let socialMedia = require('../assets/socialMedia.png')



const Home = props =>{
    const [enteredValue, setEnteredValue] = useState('');
    const[selectedNumber, setSelectedNumber] = useState('');

    const [enteredText, setEnteredText] = useState('');

    const [enteredEmail, setEnteredEmail] = useState('');

    const phoneInputHandler = number =>{
        setSelectedNumber(number.replace(/[^0-9]/g, ''));
    };
    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^a-zA-Z ]/g, ''));
    };
    const textInputHandler = text => {
        setEnteredText(text.replace(/[^a-zA-Z ]/g, ''));
    };
    const mailInputHandler = mail => {
        setEnteredEmail(mail.replace(/[^a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]/g, ''));
    };
    let date = useState("1980-01-01");

    return(
    <ScrollView>
            <Header
            text='Contact App'
            />
            <View style={styles.container}>
            <Card style={styles.inputContainer}>
                <MyTitle style={styles.title}>Identification</MyTitle>
                <MyInput text='First Name :'
                         placeholder='First Name'
                         autoCompleteType='off'
                         autoCorrect={false}
                    onChangeText={numberInputHandler}
                         value={enteredValue}/>
                <MyInput text='Last Name :'
                         placeholder='Last Name'
                         onChangeText={textInputHandler}
                         value={enteredText}/>
                <MyDatePicker text='Birthdate :'
                              date={date}
                              mode="date"
                              placeholder="Select Birthdate"
                              format="YYYY-MM-DD"
                              onDateChange={(date) => {this.setState({date: date})}}
                />
                <MyPicker text='Bloodtype :'
                />
            </Card>
        <Card style={styles.inputContainer}>
            <MyTitle style={styles.title}>Contact information</MyTitle>
            <MyInput text='Address :'
                     placeholder='Full address'
                     autoCompleteType='off'
                     autoCorrect={false}
                     />
            <MyInput text='E-mail :'
                     placeholder='E-mail'
                     onChangeText={mailInputHandler}
                     value={enteredEmail}
                     />
            <MyInput text='Mobile phone :'
                     placeholder='Mobile phone'
                     keyboardType='phone-pad'
                     onChangeText={phoneInputHandler}
                     value={selectedNumber}
            />
        </Card>
                <Card style={styles.imageContainer}>
                    <MyTitle style={styles.title}>Social Media</MyTitle>
                    <Image source={socialMedia}
                    style={{width:200, height: 150, marginTop: 15}}
                    />
                </Card>
                <Card style={styles.inputContainer}>
                    <MyTitle style={styles.title}>Allerges & references</MyTitle>
                    <CheckBox
                        center
                        title='Gluten'
                    />
                    <CheckBox
                        center
                        title='House Dust Mite'
                    />
                    <CheckBox
                        center
                        title='Pollen'
                    />
                    <CheckBox
                        center
                        title='Vegan'
                    />
                    <MyInput
                    text='Other: '/>
                </Card>
                <Card style={styles.inputContainer}>
                    <MyTitle style={styles.title}>{enteredValue + ' ' + enteredText}</MyTitle>
                    <MyText>Birhtdate: {date}</MyText>
                </Card>
            </View>
            </ScrollView>
    )
}
const styles = StyleSheet.create({
container: {
    alignItems: 'center'
},
input:{
    width: '80%'
},
    title:{
    textDecorationLine: 'underline',
        marginBottom: 10,
        textAlign: 'center',
        fontSize: 24
    },
inputContainer: {
    backgroundColor: Colors.primary,
        width: '85%',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        paddingVertical: 20
},
    imageContainer:{
    justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary,
        width: '85%',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        paddingVertical: 20
    }
})
export default Home;