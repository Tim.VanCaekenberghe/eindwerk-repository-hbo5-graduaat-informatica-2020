import React, {useState} from 'react';
import {View, Image, StyleSheet, ScrollView, Picker} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyText from "../components/MyText";
import MyInput from "../components/MyInput";
import Header from "../components/Header";
import MyDatePicker from "../components/MyDatePicker";
import MyPicker from "../components/MyPicker";
import { CheckBox } from 'react-native-elements'
import DatePicker from "react-native-datepicker";

let socialMedia = require('../assets/socialMedia.png')



const Results = props =>{
    return(
       <ScrollView>
           <View>
                <Card style={styles.inputContainer}>
                    <MyTitle style={styles.title}>{enteredValue + ' ' + enteredText}</MyTitle>
                    <MyText>Birhtdate: {date}</MyText>
                </Card>
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    input:{
        width: '80%'
    },
    title:{
        textDecorationLine: 'underline',
        marginBottom: 10,
        textAlign: 'center',
        fontSize: 24
    },
    inputContainer: {
        backgroundColor: Colors.primary,
        width: '85%',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        paddingVertical: 20
    },
    imageContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary,
        width: '85%',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        paddingVertical: 20
    }
})
export default Results;