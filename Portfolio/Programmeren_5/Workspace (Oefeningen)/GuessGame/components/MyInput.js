import React from 'react'
import {StyleSheet, TextInput, Text} from 'react-native'
import Colors from "../constants/Colors";

const MyInput = props =>{
    return(
        <TextInput {...props} style={{ ...styles.input, ...props.style}}
        />
    )
}

const styles = StyleSheet.create({
    input: {
        borderWidth: 2,
        borderColor: Colors.secondary,
        width:50,
        textAlign: 'center',
        marginVertical: 10
    }
})
export default MyInput;