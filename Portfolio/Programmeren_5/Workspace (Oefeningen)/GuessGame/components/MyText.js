import React from 'react'
import {StyleSheet, View, Text} from 'react-native'
import Colors from "../constants/Colors";

const MyText = props =>{
    return(
        <Text style={{...styles.text, ...props.style}}>
            {props.children}
        </Text>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 15,
        color: Colors.normalText,
        fontFamily: 'nunito-black'
    }
})
export default MyText;