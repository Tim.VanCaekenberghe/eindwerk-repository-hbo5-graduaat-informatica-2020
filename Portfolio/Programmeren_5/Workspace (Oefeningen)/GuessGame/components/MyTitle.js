import React from 'react'
import {StyleSheet, View, Text} from 'react-native'
import Colors from "../constants/Colors";

const MyTitle = props =>{
    return(
        <Text style={{...styles.title, ...props.style}}>
            {props.children}
        </Text>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        color: Colors.normalText,
        padding: 10,
        fontFamily: 'nunito-black'
    }
})
export default MyTitle;