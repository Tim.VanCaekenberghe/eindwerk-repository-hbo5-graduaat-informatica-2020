import React from 'react'
import {StyleSheet, View, Text} from 'react-native'

const Splash = props =>{
        return (
            <View style={styles.container}>
                <Text style={styles.text}>
                    Splash Screen
                </Text>
            </View>
        )
    }
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10,
        backgroundColor: 'grey',
        justifyContent:'center'
    },
    text: {
        color: 'black',
        textAlign: 'center'
    }
})
export default Splash