import React, {useState} from 'react';
import {View, StyleSheet, Text, Alert, TouchableOpacity} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyButton from "../components/MyButton";
import MyInput from "../components/MyInput";
import MyText from "../components/MyText";
import GuessingScreen from "./GuessingScreen";
import {MaterialIcons} from "@expo/vector-icons";


const GameOverScreen = props => {
    return (
        <View style={styles.container}>
            <MyTitle> The number was guessed!</MyTitle>
            <Card style={styles.inputContainer}>
                <MyText>Number of rounds: {props.roundNumber}</MyText>
                <MyText>The number was: {props.userNumber}</MyText>
            </Card>
            <Card style={styles.iconContainer}>
                <TouchableOpacity
                >
                    <MaterialIcons name="fiber-new" size={60} />
                </TouchableOpacity>
            </Card>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10
    },
    iconContainer: {
        backgroundColor: Colors.primary,
        width: 100,
        borderRadius: 20,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        alignItems: 'center',
    },
    inputContainer: {
        backgroundColor: Colors.primary,
        width: 300,
        maxWidth: '80%',
        borderRadius: 20,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        alignItems: 'center',
        paddingVertical: 20,
        marginBottom: 20
    },
    buttonContainer: {
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    myText:{
        padding: 10
    }
})
export default GameOverScreen;