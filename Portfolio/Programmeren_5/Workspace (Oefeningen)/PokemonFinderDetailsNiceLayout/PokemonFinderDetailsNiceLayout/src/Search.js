import React from 'react';
import {Text, View, ImageBackground, Platform} from 'react-native';
import {Header, Item, Icon, Input, Button} from 'native-base';
import Loading from './Loading';
import SearchBody from './SearchBody';



class Search extends React.Component {
    render() {
        return (
            <View>
                <Header
                    searchBar
                    rounded
                >
                    <Item>
                        <Icon
                            name="ios-search"

                        />
                        <Input
                            placeholder="Find"
                        />
                    </Item>

                </Header>
                <SearchBody/>

            </View>

        )
    }
}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}


export default Search;
