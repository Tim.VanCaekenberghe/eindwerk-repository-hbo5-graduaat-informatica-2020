import React from 'react';
import {Text, View, ImageBackground, Platform} from 'react-native';
import {Header, Item, Icon, Input, Button} from 'native-base';
import Loading from './Loading';
import SearchBody from './SearchBody';
import axios from 'axios';



class Search extends React.Component {
    searchDisabled = false;

    state = {
        input: "",
        data: {},
        onCall: true
    }
    findPokemon = () => {
        let input = this.state.input;
        let self = this;
        this.setState({onCall: true});

        if (!this.searchDisabled) {
            axios.get("https://pokeapi.co/api/v2/pokemon/" + input)
                .then((response) => {
                    self.setState({data: response.data, onCall:false});
                    this.searchDisabled = true;
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    };

    updateInput = (text) => {
        this.setState({input: text.toLowerCase()});
        this.searchDisabled = false;
    };

    renderBody = () => {
        if (this.state.onCall) {
            return (<Loading/>)
        } else {
            return (
                <SearchBody data={this.state.data}/>
            )
        }
    }

    render() {
        return (
            <View>
                <Header
                    searchBar
                    rounded
                >
                    <Item>
                        <Icon
                            name="ios-search"
                            onPress={this.findPokemon}
                        />
                        <Input
                            placeholder="Find a pokémon!"
                            onChangeText={(text) => this.updateInput(text)}>
                        </Input>
                    </Item>

                </Header>
                {this.renderBody()}

            </View>

        )
    }
}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}


export default Search;
