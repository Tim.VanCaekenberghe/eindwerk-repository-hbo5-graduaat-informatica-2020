import {StyleSheet} from 'react-native';
import Colors from '../constants/colors';

export default StyleSheet.create({
    gridItem:{
        flexDirection: 'row',
        flex: 1,
        padding: 10,
        marginBottom: 70

    }
   
})
