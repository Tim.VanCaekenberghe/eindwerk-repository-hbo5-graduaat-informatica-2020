import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button, FlatList } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';

import {CATEGORIES} from "../data/dummy-data";
import CategoryGridTile from "../components/CategoryGridTile";


const CategoriesScreen = props =>{
    const renderGridItem = (itemData) =>{
        return(
        <CategoryGridTile onPress={() => {
            props.navigation.navigate('CategoryMeal', {category: itemData.item});
        }} title={itemData.item.title} color={itemData.item.color}/>
        )
    };
    return(
        <FlatList data={CATEGORIES} renderItem={renderGridItem} numColumns={2}/>
    );

};

const styles = StyleSheet.create({

});

export default CategoriesScreen;
