import React from 'react';
import { StyleSheet, View } from 'react-native';
import Homepage from "./src/Homepage";
import MeatRecipes from "./src/MeatRecipes"
import Rec1 from "./src/Rec1";
import VegetarianRecipes from "./src/VegetarianRecipes";
import FishRecipes from "./src/FishRecipes";
import Rec4 from "./src/Rec4";
import Rec3 from "./src/Rec3"
import Rec2 from "./src/Rec2";
import Rec5 from "./src/Rec5";
import Rec6 from "./src/Rec6";


export default class App extends React.Component {
  state = {
    currentScreen: "Homepage"
  }
  switchScreen = (currentScreen) => {
    this.setState({currentScreen});
  }
  renderScreen = () => {
    switch (this.state.currentScreen) {
      case "Homepage":
        return (
            <Homepage switchScreen={this.switchScreen}/>
        );
        break;
      case "MeatRecipes":
        return (
            <MeatRecipes switchScreen={this.switchScreen}/>
        );
        break;
      case "Rec1":
        return (
            <Rec1 switchScreen={this.switchScreen}/>
        );
        break;
      case "Rec2":
        return (
            <Rec2 switchScreen={this.switchScreen}/>
        );
        break;
      case "FishRecipes":
        return (
            <FishRecipes switchScreen={this.switchScreen}/>
        );
        break;
      case "Rec3":
        return (
            <Rec3 switchScreen={this.switchScreen}/>
        );
        break;
      case "Rec4":
        return (
            <Rec4 switchScreen={this.switchScreen}/>
        );
        break;
      case "VegetarianRecipes":
        return (
            <VegetarianRecipes switchScreen={this.switchScreen}/>
        );
        break;
      case "Rec5":
        return(
            <Rec5 switchScreen={this.switchScreen}/>
        );
        break;
      case "Rec6":
        return(
            <Rec6 switchScreen={this.switchScreen}/>
        );
        break;
    }

  }

  render() {
    return (
        <View style={styles.container}>
          {this.renderScreen()}
        </View>
    );

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

