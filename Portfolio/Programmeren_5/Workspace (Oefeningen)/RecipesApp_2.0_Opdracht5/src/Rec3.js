import React from 'react';
import {StyleSheet,Image, Button, View, ScrollView} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';


class Rec1 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                <Content contentContainerStyle={styles.container}>
                    <Card style={styles.card}>
                        <CardItem style={styles.card} header>
                            <Text style={styles.title}>Gebakken tongreepjes met rucolasalade</Text>
                        </CardItem>
                        <CardItem style={styles.card}>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 300, height: 250}}
                                    source={require('../assets/tongrolletjes.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem style={styles.card} footer>
                            <Text>
                                Bereiding
                                Spoel de tongfilets af onder koud water en dep ze goed droog. Snijd de filets in lange repen. Bestrooi ze met zout, een beetje peper en bloem.
                                Maak de sla schoon, scheur de grote bladeren iets kleiner en meng deze met de rucola. Pel de rode ui, verdeel deze in ringen en leg ze op de sla.
                                Rooster de pijnboompitten in een droge koekenpan goudbruin.
                                Meng de ingrediënten voor de dressing en schep deze door de salade.
                                Bak de tongreepjes 3-4 minuten in de hete olie. Schep ze uit de pan en verdeel ze als ze lauwwarm zijn over de salade. Strooi de pijnboompitten erover.
                                Lekker met ciabatta of stokbrood.
                                </Text>
                        </CardItem>
                    </Card>
                    <View style={styles.alternativeLayoutButtonContainer}>
                        <Button style={styles.button}
                                title={"Back"}
                                onPress={() => this.props.switchScreen("FishRecipes")}
                        />
                        <View style={styles.buttonSpace}>
                            <Button style={styles.button}
                                    title={"Home"}
                                    onPress={() => this.props.switchScreen("Homepage")}
                            />
                        </View>
                    </View>
                </Content>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    buttonSpace:{
        marginLeft: 25
    },
    alternativeLayoutButtonContainer: {
        margin: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        flex: 1
    },
    card: {
        width: '100%',
        justifyContent: 'center',
        marginTop: 25
    },
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        textDecorationLine: 'underline'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99',
        padding: 10
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec1;