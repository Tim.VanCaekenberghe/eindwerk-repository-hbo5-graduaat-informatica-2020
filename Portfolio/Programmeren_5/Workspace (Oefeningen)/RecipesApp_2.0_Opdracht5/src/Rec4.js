import React from 'react';
import {StyleSheet,Image, Button, View, ScrollView} from 'react-native';
import { Content, CardItem, Card, Text, Body} from 'native-base';


class Rec4 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                        <CardItem style={styles.card} header>
                            <Text style={styles.title}>Pasta met Hollandse garnalen en courgette</Text>
                        </CardItem>
                        <CardItem style={styles.card}>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 300, height: 250}}
                                    source={require('../assets/pasta.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem style={styles.card} footer>
                            <Text>
                                Bereiding
                                Zet een grote pan met water en wat zout op en breng aan de kook. Kook de spaghetti al dente. Als je de spaghetti afgiet vang dan een kopje van het kookwater op, dit heb je daarna nog nodig!
                                Smelt de boter in een grote koekenpan, bak op hoog vuur de courgette 2-3 minuten aan.
                                Voeg de knoflook toe, zet het vuur iets lager en bak dit 2 minuten mee, terwijl je regelmatig alles omschept.
                                Voeg nu de rasp van de citroen toe en sap naar smaak (minimaal 2 eetlepels).
                                Voeg tot slot de Hollandse garnalen toe en warm alles even mee.
                                De afgegoten spaghetti én een half kopje van het kookvocht gaan daar nog bij.
                                Schep alles goed door en breng op smaak met zout en versgemalen zwarte peper.
                                Proef of er eventueel nog wat extra citroensap bij moet.
                                Schep de pasta in diepe borden en garneer met wat fijngesneden bieslook. Serveer meteen.
                                ....</Text>
                        </CardItem>
                    </Card>
                    <View style={styles.alternativeLayoutButtonContainer}>
                        <Button style={styles.button}
                                title={"Back"}
                                onPress={() => this.props.switchScreen("FishRecipes")}
                        />
                        <View style={styles.buttonSpace}>
                            <Button style={styles.button}
                                    title={"Home"}
                                    onPress={() => this.props.switchScreen("Homepage")}
                            />
                        </View>
                    </View>
                </Content>
                    </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    buttonSpace:{
        marginLeft: 25
    },
    alternativeLayoutButtonContainer: {
        margin: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        flex: 1
    },
    card: {
        width: '100%',
        justifyContent: 'center',
        marginTop: 25
    },
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        textDecorationLine: 'underline'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99',
        padding: 10
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec4;