import React from 'react';
import {StyleSheet, ImageBackground, View} from 'react-native';
import {Content, CardItem, Card, Text} from 'native-base';

let background = require('../assets/background.png')
class Homepage extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
                    <Content contentContainerStyle={styles.container}>
                        <Text style={styles.title}>Soorten Recepten</Text>
                        <Card style={styles.card}>
                            <CardItem header bordered>
                                <Text onPress={() => this.props.switchScreen("MeatRecipes")}>Vleesgerechten</Text>
                            </CardItem>
                        </Card>
                        <Card style={styles.card} >
                            <CardItem header bordered>
                                <Text onPress={() => this.props.switchScreen("FishRecipes")}>Visgerechten</Text>
                            </CardItem>
                        </Card>
                        <Card style={styles.card}>
                            <CardItem header bordered
                                      style={styles.card}>
                                <Text onPress={() => this.props.switchScreen("VegetarianRecipes")}>Vegetarische gerechten</Text>
                            </CardItem>
                            </Card>
                    </Content>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    }
});


export default Homepage;

