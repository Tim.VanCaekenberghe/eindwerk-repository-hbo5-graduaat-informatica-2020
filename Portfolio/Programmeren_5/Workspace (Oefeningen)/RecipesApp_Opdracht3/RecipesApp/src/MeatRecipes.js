import React from 'react';
import {StyleSheet,Image, Button, View} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';

class MeatRecipes extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Vleesgerechten</Text>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                    <CardItem header>
                        <Text style={styles.title1}>Balletjes in Tomatensaus</Text>
                    </CardItem>
                    <CardItem>
                        <Body style={styles.body}>
                            <Image
                                style={{width: 150, height: 150}}
                                source={require('../assets/balletjesInTomatenSaus.png')}
                            />
                        </Body>
                    </CardItem>
                    <CardItem footer>
                        <Content>
                            <Button
                                title="Recept"
                                style={styles.text}
                                onPress={() => this.props.switchScreen("Rec1")}/>
                        </Content>
                    </CardItem>
                </Card>
                    <Card>
                        <CardItem header>
                            <Text style={styles.title1}>Varkenshaasje met aardappelgratin</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/varkenshaasje.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Content>
                                <Button
                                    title="Recept"
                                    style={styles.text}
                                    onPress={() => this.props.switchScreen("Rec2")}/>
                            </Content>
                        </CardItem>
                    </Card>
                    <Button title={"Go back"}
                            onPress={() => this.props.switchScreen("Homepage")}
                    />
                </Content>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    title1: {
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default MeatRecipes;