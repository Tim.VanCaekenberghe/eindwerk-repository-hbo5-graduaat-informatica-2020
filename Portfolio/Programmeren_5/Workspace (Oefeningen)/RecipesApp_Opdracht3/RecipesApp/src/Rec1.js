import React from 'react';
import {StyleSheet,Image, Button, View} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';


class Rec1 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                        <CardItem header>
                            <Text style={styles.title}>Balletjes in tomatensaus</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/balletjesInTomatenSaus.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>Vleesballetjes maken:
                                1 klont boter
                                ½ kilogram gehakt: half varken, half kalf of rund
                                wat tarwebloem in een bord om de gerolde balletjes te bebloemen
                                Saus ingrediënten:
                                1 ajuin
                                1 knoflookteentje
                                4 verse tomaten
                            ....</Text>
                        </CardItem>
                    </Card>
                    <Button title={"Go back"}
                            onPress={() => this.props.switchScreen("MeatRecipes")}
                    />
                </Content>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',

    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec1;