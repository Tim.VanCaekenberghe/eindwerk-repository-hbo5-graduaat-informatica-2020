import React from 'react';
import {StyleSheet,Image, Button,View} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';

class VegetarianRecipes extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Vegetarische gerechten</Text>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                        <CardItem header>
                            <Text style={styles.title1}>Veggiesatés met champignons en kruidenkaas</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/vBrochetten.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Content>
                                <Button
                                    title="Recept"
                                    style={styles.text}
                                    onPress={() => this.props.switchScreen("Rec5")}/>
                            </Content>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem header>
                            <Text style={styles.title1}>Pastasalade met gegrilde groenten</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/vPasta.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Content>
                                <Button
                                    title="Recept"
                                    style={styles.text}
                                    onPress={() => this.props.switchScreen("Rec6")}/>
                            </Content>
                        </CardItem>
                    </Card>
                    <Button title={"Go back"}
                            onPress={() => this.props.switchScreen("Homepage")}
                    />
                </Content>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    title1: {
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default VegetarianRecipes;