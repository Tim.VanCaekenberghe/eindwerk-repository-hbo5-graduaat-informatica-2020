import React from 'react';
import { StyleSheet, Text, View, ImageBackground, TextInput, Button} from 'react-native';

var background = require('./assets/Supermarket.png')

class App extends React.Component {
    state = {
        text: "You dont have anything on your list yet",
        list: ["Appelen", "Peren", "test"]
    }
    deleteItem = (item) => {
        var array = this.state.list;
        var itemToDelete = array.indexOf(item);
        array.splice(itemToDelete, 1);
        this.setState({list:array})
    }
    addItem = () => {
        var newItem = this.state.text;
        var shopArray = this.state.list;
        shopArray.push(newItem);
        this.setState({list: shopArray, text:""})
    }
    createShoppingList = () => {
        return this.state.list.map(
            item => {
                return (
                    <Text key={item}
                          onPress={() => {this.deleteItem(item)}}
                          style={styles.listItem}
                    >{item}</Text>
                )
            }
        )
    }
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
                    <Text style={styles.title}>ShoppingList</Text>
                    <Text style={styles.paragraph}>Enter the products you need below to add it to the shopping list</Text>
                    <TextInput style={styles.inputStyle}
                               onChangeText={(text) => this.setState({text})}
                               value={this.state.text}
                    />
                    <Button
                        title="Add to shopping list"
                        onPress={this.addItem}
                    />
                    {this.createShoppingList()}
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
      color: 'white',
      fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center'
        

    },
    listItem: {
        fontSize: 30,
        color: 'white',
        textAlign: 'center'
    },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
    paragraph: {
        color: 'white',
        textAlign: 'center',
        fontSize: 30
    },
    inputStyle: {
        height: 40,
        borderColor: "white",
        borderWidth: 2,
        color: "white",


    }

})

    export default App;
