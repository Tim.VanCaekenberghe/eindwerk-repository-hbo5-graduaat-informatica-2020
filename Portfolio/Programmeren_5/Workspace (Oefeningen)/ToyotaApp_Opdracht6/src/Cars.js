import React from 'react';
import {Text, View, ImageBackground, ScrollView, Platform,  Image} from 'react-native';
import data from '../toyota';
import {Header, Item, CardItem, Card, Button, ListItem, List, Body} from 'native-base';


class Cars extends React.Component {
    render() {
        let cars = data.cars
        return (
            <View style={styles.container}>
                <ScrollView>
                <List dataArray= {cars}
                      renderRow={(car) =>
                          <ListItem>
                              <Card style={styles.card}>
                                  <CardItem header style={styles.card}>
                              <Text style={styles.title}>{car.name}</Text>
                                  </CardItem>
                                  <CardItem style={styles.card} >
                                      <Body style={styles.body}>
                                          <Image
                                              style={{width: '100%', height: 200}}
                                              source={{uri: car.img}}
                                          />
                                      </Body>
                                  </CardItem>
                                  <CardItem>
                                      <List>
                                          <ListItem itemDivider>
                                              <Text style={{fontWeight: 'bold', width: '100%'}}>Specificaties</Text>
                                          </ListItem>
                                          <ListItem>
                                              <Text> Prijs: {car.prijs}</Text>
                                          </ListItem>
                                          <ListItem>
                                              <Text> Verbruik: {car.verbruik}</Text>
                                          </ListItem>
                                          <ListItem>
                                              <Text> Motor: {car.motor}</Text>
                                          </ListItem>
                                      </List>
                                  </CardItem>
                              </Card>
                          </ListItem>
                      }>

                </List>
                </ScrollView>
            </View>

        )
    }
}

const styles = {
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 5

    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#f5f5f0'
    },
    header: {
        fontSize: 30,
        color: 'red',
        textAlign: 'center'
    },
    viewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    img: {
        height: 200,
        width: 200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    info: {
        flex: 1,
        opacity: 0.6
    },
    card: {
        width: '100%',
        justifyContent: 'center',
        backgroundColor: '#ebebe0'
    },
    body: {
        alignItems: 'center'
    }
}

export default Cars;
